//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TransactionalInputStreamTest {

    @Test
    public void testSimple() throws Exception {
    
        final InputStream source = new ByteArrayInputStream("hello world!".getBytes());
        final TransactionalInputStream inputStream = TransactionalInputStream.create(source);
        assertEquals(12, inputStream.available());

        final byte[] buffer = new byte[6];
        assertEquals(6, inputStream.read(buffer));
        assertArrayEquals("hello ".getBytes(), buffer);
        assertEquals(6, inputStream.available());

        inputStream.rollback();
        assertEquals(12, inputStream.available());
        assertEquals(0x68, inputStream.read());
        assertEquals(11, inputStream.available());

        inputStream.commit();
        assertEquals(6, inputStream.read(buffer));
        assertArrayEquals("ello w".getBytes(), buffer);
        assertEquals(5, inputStream.available());

    }
}
