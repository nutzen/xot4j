//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.InputStream;
import java.io.OutputStream;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class PipeTest {

    @Test
    public void testSimple() throws Exception {

        final Pipe byteStream = Pipe.create();
        final InputStream inputStream = byteStream.getInputStream();
        final OutputStream outputStream = byteStream.getOutputStream();

        assertEquals(0, inputStream.available());
        assertEquals(-1, inputStream.read());

        {
            final byte[] buffer = new byte[12];
            final byte[] helloWorld = "hello world!".getBytes();

            outputStream.write(helloWorld);
            assertEquals(12, inputStream.available());

            inputStream.read(buffer);
            assertArrayEquals(helloWorld, buffer);

            assertEquals(0, inputStream.available());
            assertEquals(-1, inputStream.read());
        }

        {
            final byte[] buffer = new byte[4];

            outputStream.write("hello world!".getBytes());
            assertEquals(12, inputStream.available());

            inputStream.read(buffer);
            assertArrayEquals("hell".getBytes(), buffer);

            assertEquals(8, inputStream.available());

            inputStream.read(buffer);
            assertArrayEquals("o wo".getBytes(), buffer);

            assertEquals(4, inputStream.available());

            inputStream.read(buffer);
            assertArrayEquals("rld!".getBytes(), buffer);

            assertEquals(0, inputStream.available());
            assertEquals(-1, inputStream.read());
        }

        {
            final byte[] buffer = new byte[12];

            outputStream.write("he".getBytes());
            assertEquals(2, inputStream.available());

            outputStream.write("llo ".getBytes());
            assertEquals(6, inputStream.available());

            outputStream.write("world!".getBytes());
            assertEquals(12, inputStream.available());

            inputStream.read(buffer);
            assertArrayEquals("hello world!".getBytes(), buffer);
        }
    }
}
