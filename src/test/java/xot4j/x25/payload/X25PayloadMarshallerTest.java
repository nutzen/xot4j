//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.Test;
import xot4j.x121.X121Address;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.codes.ResetRequestEnumeration;
import xot4j.x25.codes.RestartRequestEnumeration;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.PacketSizeSelection;
import xot4j.x25.facilities.PacketSizeSelectionEnumeration;
import xot4j.x25.facilities.WindowSizeSelection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class X25PayloadMarshallerTest {

    @Test
    public void testCallPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{(byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, (byte) 0xc4};

        final X121Address calledAddress = X121Address.create("1634567");
        final X121Address callingAddress = X121Address.create("11009000");
        final Collection<Facility> facilities = new ArrayList<Facility>();
        facilities.add(WindowSizeSelection.create(1, 1));
        facilities.add(PacketSizeSelection.create(PacketSizeSelectionEnumeration.Size128octets, PacketSizeSelectionEnumeration.Size128octets));
        final byte[] userData = new byte[]{(byte) 0xc4};

        final CallPacketPayload source = CallPacketPayload.create(calledAddress, callingAddress, facilities, userData);

        assertEquals(calledAddress, source.getCalledAddress());
        assertEquals(callingAddress, source.getCallingAddress());
        assertEquals(facilities.size(), source.getFacilities().size());
        assertArrayEquals(userData, source.getUserData());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalCallPacketPayload(outputStream, source, false);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final CallPacketPayload result = (CallPacketPayload) X25PayloadMarshaller.unmarshalCallPacketPayload(new ByteArrayInputStream(intermediate), false);

        assertEquals(source.getCalledAddress(), result.getCalledAddress());
        assertEquals(source.getCallingAddress(), result.getCallingAddress());
        assertEquals(source.getFacilities().size(), result.getFacilities().size());
        assertArrayEquals(source.getUserData(), result.getUserData());
    }

    @Test
    public void testClearPacketPayloadWithOptionalFields() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x39, 0x30, (byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, (byte) 0xc4};

        final ClearRequestEnumeration cause = ClearRequestEnumeration.ClearRequestShipAbsent;
        final DiagnosticCodeEnumeration diagnostic = DiagnosticCodeEnumeration.TimerExpired;
        final X121Address calledAddress = X121Address.create("1634567");
        final X121Address callingAddress = X121Address.create("11009000");
        final Collection<Facility> facilities = new ArrayList<Facility>();
        facilities.add(WindowSizeSelection.create(1, 1));
        facilities.add(PacketSizeSelection.create(PacketSizeSelectionEnumeration.Size128octets, PacketSizeSelectionEnumeration.Size128octets));
        final byte[] userData = new byte[]{(byte) 0xc4};

        final ClearPacketPayload source = ClearPacketPayload.create(cause, diagnostic, calledAddress, callingAddress, facilities, userData);

        assertEquals(cause, source.getCause());
        assertEquals(diagnostic, source.getDiagnostic());
        assertEquals(calledAddress, source.getCalledAddress());
        assertEquals(callingAddress, source.getCallingAddress());
        assertEquals(facilities.size(), source.getFacilities().size());
        assertArrayEquals(userData, source.getUserData());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalClearPacketPayload(outputStream, source, false);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final ClearPacketPayload result = (ClearPacketPayload) X25PayloadMarshaller.unmarshalClearPacketPayload(new ByteArrayInputStream(intermediate), false);

        assertEquals(source.getCause(), result.getCause());
        assertEquals(source.getDiagnostic(), result.getDiagnostic());
        assertEquals(source.getCalledAddress(), result.getCalledAddress());
        assertEquals(source.getCallingAddress(), result.getCallingAddress());
        assertEquals(source.getFacilities().size(), result.getFacilities().size());
        assertArrayEquals(source.getUserData(), result.getUserData());
    }

    @Test
    public void testClearPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x00, 0x00};

        final ClearRequestEnumeration cause = ClearRequestEnumeration.ClearRequestDteOriginated;
        final DiagnosticCodeEnumeration diagnostic = DiagnosticCodeEnumeration.NoAdditionalInformation;

        final ClearPacketPayload source = ClearPacketPayload.create(cause, diagnostic);

        assertEquals(cause, source.getCause());
        assertEquals(diagnostic, source.getDiagnostic());
        assertEquals(null, source.getCalledAddress());
        assertEquals(null, source.getCallingAddress());
        assertEquals(null, source.getFacilities());
        assertEquals(null, source.getUserData());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalClearPacketPayload(outputStream, source, false);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final ClearPacketPayload result = (ClearPacketPayload) X25PayloadMarshaller.unmarshalClearPacketPayload(new ByteArrayInputStream(intermediate), false);

        assertEquals(source.getCause(), result.getCause());
        assertEquals(source.getDiagnostic(), result.getDiagnostic());
        assertEquals(source.getCalledAddress(), result.getCalledAddress());
        assertEquals(source.getCallingAddress(), result.getCallingAddress());
        assertEquals(source.getFacilities(), result.getFacilities());
        assertArrayEquals(source.getUserData(), result.getUserData());
    }

    @Test
    public void testClearConfirmationPayloadWithOptionalFields() throws Exception {

        final byte[] expectedIntermediate = new byte[]{(byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07};

        final X121Address calledAddress = X121Address.create("1634567");
        final X121Address callingAddress = X121Address.create("11009000");
        final Collection<Facility> facilities = new ArrayList<Facility>();
        facilities.add(WindowSizeSelection.create(1, 1));
        facilities.add(PacketSizeSelection.create(PacketSizeSelectionEnumeration.Size128octets, PacketSizeSelectionEnumeration.Size128octets));

        final ClearConfirmationPayload source = ClearConfirmationPayload.create(calledAddress, callingAddress, facilities);

        assertEquals(calledAddress, source.getCalledAddress());
        assertEquals(callingAddress, source.getCallingAddress());
        assertEquals(facilities.size(), source.getFacilities().size());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalClearConfirmationPayload(outputStream, source, false);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final ClearConfirmationPayload result = (ClearConfirmationPayload) X25PayloadMarshaller.unmarshalClearConfirmationPayload(new ByteArrayInputStream(intermediate), false);

        assertEquals(source.getCalledAddress(), result.getCalledAddress());
        assertEquals(source.getCallingAddress(), result.getCallingAddress());
        assertEquals(source.getFacilities().size(), result.getFacilities().size());
    }

    @Test
    public void testClearConfirmationPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{};

        final ClearConfirmationPayload source = ClearConfirmationPayload.create();

        assertEquals(null, source.getCalledAddress());
        assertEquals(null, source.getCallingAddress());
        assertEquals(null, source.getFacilities());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalClearConfirmationPayload(outputStream, source, false);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final ClearConfirmationPayload result = (ClearConfirmationPayload) X25PayloadMarshaller.unmarshalClearConfirmationPayload(new ByteArrayInputStream(intermediate), false);

        assertEquals(source.getCalledAddress(), result.getCalledAddress());
        assertEquals(source.getCallingAddress(), result.getCallingAddress());
        assertEquals(source.getFacilities(), result.getFacilities());
    }

    @Test
    public void testResetPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x03, 0x00};

        final ResetRequestEnumeration cause = ResetRequestEnumeration.ResetRequestRemoteProcedureError;
        final DiagnosticCodeEnumeration diagnostic = DiagnosticCodeEnumeration.NoAdditionalInformation;

        final ResetPacketPayload source = ResetPacketPayload.create(cause, diagnostic);

        assertEquals(cause, source.getCause());
        assertEquals(diagnostic, source.getDiagnostic());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalResetPacketPayload(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final ResetPacketPayload result = (ResetPacketPayload) X25PayloadMarshaller.unmarshalResetPacketPayload(new ByteArrayInputStream(intermediate));

        assertEquals(source.getCause(), result.getCause());
        assertEquals(source.getDiagnostic(), result.getDiagnostic());
    }

    @Test
    public void testRestartPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x00, 0x50};

        final RestartRequestEnumeration cause = RestartRequestEnumeration.RestartRequestDteRestarting;
        final DiagnosticCodeEnumeration diagnostic = DiagnosticCodeEnumeration.Miscellaneous;

        final RestartPacketPayload source = RestartPacketPayload.create(cause, diagnostic);

        assertEquals(cause, source.getCause());
        assertEquals(diagnostic, source.getDiagnostic());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalRestartPacketPayload(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final RestartPacketPayload result = (RestartPacketPayload) X25PayloadMarshaller.unmarshalRestartPacketPayload(new ByteArrayInputStream(intermediate));

        assertEquals(source.getCause(), result.getCause());
        assertEquals(source.getDiagnostic(), result.getDiagnostic());
    }

    @Test
    public void testDataPacketPayload() throws Exception {

        final String data = "Hello world";
        final byte[] expectedIntermediate = new byte[]{0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64};


        final DataPacketPayload source = DataPacketPayload.create(data.getBytes());
        assertArrayEquals(data.getBytes(), source.getUserData());

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PayloadMarshaller.marshalDataPacketPayload(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);
        final DataPacketPayload result = (DataPacketPayload) X25PayloadMarshaller.unmarshalDataPacketPayload(new ByteArrayInputStream(intermediate));

        assertArrayEquals(source.getUserData(), result.getUserData());
    }
}
