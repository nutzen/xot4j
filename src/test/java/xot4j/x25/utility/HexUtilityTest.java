//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HexUtilityTest {

    @Test
    public void testSimple() {

        final byte[] source = new byte[]{0x10, 0x01, 0x0b, (byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, (byte) 0xc4};
        final String expected = "10 01 0b 87 16 34 56 71 10 09 00 00 06 43 01 01 42 07 07 c4";

        final String target = HexUtility.toHex(source);
        assertEquals(expected, target);
    }
}
