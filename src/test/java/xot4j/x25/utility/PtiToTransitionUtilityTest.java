//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

import org.junit.Test;
import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;
import xot4j.x25.transitions.TransitionEnumeration;

import static org.junit.Assert.assertEquals;

public class PtiToTransitionUtilityTest {

    @Test
    public void testSimple() {

        // Call set-up and clearing DESCRIPTION
        assertEquals(TransitionEnumeration.CallRequest,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.CallRequest_IncomingCall));
        assertEquals(TransitionEnumeration.CallIncoming,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.CallRequest_IncomingCall));
        assertEquals(TransitionEnumeration.CallAccepted,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.CallAccepted_CallConnected));
        assertEquals(TransitionEnumeration.CallConnected,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.CallAccepted_CallConnected));
        assertEquals(TransitionEnumeration.ClearRequest,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication));
        assertEquals(TransitionEnumeration.ClearIndication,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication));
        assertEquals(TransitionEnumeration.ClearConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.ClearConfirmation));
        assertEquals(TransitionEnumeration.ClearConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.ClearConfirmation));
        // Data and interrupt DESCRIPTION
        assertEquals(TransitionEnumeration.Data,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.Data));
        assertEquals(TransitionEnumeration.Data,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.Data));
        assertEquals(TransitionEnumeration.Interrupt,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.Interrupt));
        assertEquals(TransitionEnumeration.Interrupt,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.Interrupt));
        assertEquals(TransitionEnumeration.InterruptConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.InterruptConfirmation));
        assertEquals(TransitionEnumeration.InterruptConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.InterruptConfirmation));
        // Flow control and reset DESCRIPTION
        assertEquals(TransitionEnumeration.ReceiverReady,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.ReceiverReady));
        assertEquals(TransitionEnumeration.ReceiverReady,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.ReceiverReady));
        assertEquals(TransitionEnumeration.ReceiverNotReady,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.ReceiverNotReady));
        assertEquals(TransitionEnumeration.ReceiverNotReady,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.ReceiverNotReady));
        assertEquals(TransitionEnumeration.Reject,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.Reject));
        assertEquals(TransitionEnumeration.Reject,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.Reject));
        assertEquals(TransitionEnumeration.ResetRequest,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.ResetRequest_ResetIndication));
        assertEquals(TransitionEnumeration.ResetIndication,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.ResetRequest_ResetIndication));
        assertEquals(TransitionEnumeration.ResetConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.ResetConfirmation));
        assertEquals(TransitionEnumeration.ResetConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.ResetConfirmation));
        // Restart DESCRIPTION
        assertEquals(TransitionEnumeration.RestartRequest,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.RestartRequest_RestartIndication));
        assertEquals(TransitionEnumeration.RestartIndication,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.RestartRequest_RestartIndication));
        assertEquals(TransitionEnumeration.RestartConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.RestartConfirmation));
        assertEquals(TransitionEnumeration.RestartConfirmation,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.RestartConfirmation));
        // Diagnostic DESCRIPTION
        assertEquals(TransitionEnumeration.Diagnostic,
                PtiToTransitionUtility.transition(RoleEnumeration.DTE, PacketTypeIdentifierEnumeration.Diagnostic));
        assertEquals(TransitionEnumeration.Diagnostic,
                PtiToTransitionUtility.transition(RoleEnumeration.DCE, PacketTypeIdentifierEnumeration.Diagnostic));
    }
}
