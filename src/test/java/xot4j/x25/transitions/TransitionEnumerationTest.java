//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.transitions.TransitionEnumeration;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TransitionEnumerationTest {

    static void testSimple(TransitionEnumeration source) {

        final String intermediate = source.toDescription();
        final TransitionEnumeration result = TransitionEnumeration.fromDescription(intermediate);
        assertEquals(intermediate, source, result);
    }

    @Test
    public void testSimple() {

        // Call set-up and clearing DESCRIPTION
        testSimple(TransitionEnumeration.CallRequest);
        testSimple(TransitionEnumeration.CallIncoming);
        testSimple(TransitionEnumeration.CallAccepted);
        testSimple(TransitionEnumeration.CallConnected);
        testSimple(TransitionEnumeration.ClearRequest);
        testSimple(TransitionEnumeration.ClearIndication);
        testSimple(TransitionEnumeration.ClearConfirmation);
        // Data and interrupt DESCRIPTION
        testSimple(TransitionEnumeration.Data);
        testSimple(TransitionEnumeration.Interrupt);
        testSimple(TransitionEnumeration.InterruptConfirmation);
        // Flow control and reset DESCRIPTION
        testSimple(TransitionEnumeration.ReceiverReady);
        testSimple(TransitionEnumeration.ReceiverNotReady);
        testSimple(TransitionEnumeration.Reject);
        testSimple(TransitionEnumeration.ResetRequest);
        testSimple(TransitionEnumeration.ResetIndication);
        testSimple(TransitionEnumeration.ResetConfirmation);
        // Restart DESCRIPTION
        testSimple(TransitionEnumeration.RestartRequest);
        testSimple(TransitionEnumeration.RestartIndication);
        testSimple(TransitionEnumeration.RestartConfirmation);
        // Diagnostic DESCRIPTION
        testSimple(TransitionEnumeration.Diagnostic);
    }
}
