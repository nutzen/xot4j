//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PacketTypeIdentifierTest {

    @Test
    public void testSimple() {

        testSimple(PacketTypeIdentifierEnumeration.CallRequest_IncomingCall);
        testSimple(PacketTypeIdentifierEnumeration.CallAccepted_CallConnected);
        testSimple(PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication);
        testSimple(PacketTypeIdentifierEnumeration.ClearConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.Data, false, 1, 2);
        testSimple(PacketTypeIdentifierEnumeration.Interrupt);
        testSimple(PacketTypeIdentifierEnumeration.InterruptConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.ReceiverReady, 1);
        testSimple(PacketTypeIdentifierEnumeration.ReceiverNotReady, 2);
        testSimple(PacketTypeIdentifierEnumeration.Reject, 3);
        testSimple(PacketTypeIdentifierEnumeration.ResetRequest_ResetIndication);
        testSimple(PacketTypeIdentifierEnumeration.ResetConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.RestartRequest_RestartIndication);
        testSimple(PacketTypeIdentifierEnumeration.RestartConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.Diagnostic);
    }

    static void testSimple(PacketTypeIdentifierEnumeration source) {

        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(source);
        assertEquals(false, packetTypeIdentifier.isDataPacketTypeIdentifier());
        assertEquals(false, packetTypeIdentifier.isReceiverPacketTypeIdentifier());

        assertEquals(source, packetTypeIdentifier.getEnumeration());
    }

    static void testSimple(PacketTypeIdentifierEnumeration source, boolean moreData, int packetReceiveSequenceNumber, int packetSendSequenceNumber) {

        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(source, moreData, packetReceiveSequenceNumber, packetSendSequenceNumber);
        assertEquals(true, packetTypeIdentifier.isDataPacketTypeIdentifier());
        assertEquals(false, packetTypeIdentifier.isReceiverPacketTypeIdentifier());

        final DataPacketTypeIdentifier identifier = (DataPacketTypeIdentifier) packetTypeIdentifier;
        assertEquals(source, identifier.getEnumeration());

        assertEquals(moreData, identifier.isMoreData());
        assertEquals(packetReceiveSequenceNumber, identifier.getPacketReceiveSequenceNumber());
        assertEquals(packetSendSequenceNumber, identifier.getPacketSendSequenceNumber());
    }

    static void testSimple(PacketTypeIdentifierEnumeration source, int packetReceiveSequenceNumber) {

        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(source, packetReceiveSequenceNumber);
        assertEquals(false, packetTypeIdentifier.isDataPacketTypeIdentifier());
        assertEquals(true, packetTypeIdentifier.isReceiverPacketTypeIdentifier());

        final ReceiverPacketTypeIdentifier identifier = (ReceiverPacketTypeIdentifier) packetTypeIdentifier;
        assertEquals(source, identifier.getEnumeration());

        assertEquals(packetReceiveSequenceNumber, identifier.getPacketReceiveSequenceNumber());
    }
}
