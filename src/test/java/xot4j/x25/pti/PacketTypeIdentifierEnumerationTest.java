//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PacketTypeIdentifierEnumerationTest {

    @Test
    public void testSimple() {

        testSimple(PacketTypeIdentifierEnumeration.CallRequest_IncomingCall);
        testSimple(PacketTypeIdentifierEnumeration.CallAccepted_CallConnected);
        testSimple(PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication);
        testSimple(PacketTypeIdentifierEnumeration.ClearConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.Data);
        testSimple(PacketTypeIdentifierEnumeration.Interrupt);
        testSimple(PacketTypeIdentifierEnumeration.InterruptConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.ReceiverReady);
        testSimple(PacketTypeIdentifierEnumeration.ReceiverNotReady);
        testSimple(PacketTypeIdentifierEnumeration.Reject);
        testSimple(PacketTypeIdentifierEnumeration.ResetRequest_ResetIndication);
        testSimple(PacketTypeIdentifierEnumeration.ResetConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.RestartRequest_RestartIndication);
        testSimple(PacketTypeIdentifierEnumeration.RestartConfirmation);
        testSimple(PacketTypeIdentifierEnumeration.Diagnostic);
    }

    static void testSimple(PacketTypeIdentifierEnumeration source) {

        {
            final String intermediate = source.toDescription();
            final PacketTypeIdentifierEnumeration pti = PacketTypeIdentifierEnumeration.fromDescription(intermediate);

            assertEquals("intermediate=" + intermediate, source, pti);
        }

        {
            final int intermediate = source.toInt();
            final PacketTypeIdentifierEnumeration pti = PacketTypeIdentifierEnumeration.fromInt(intermediate);

            assertEquals("intermediate=" + intermediate, source, pti);
        }
    }
}
