//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PacketTypeIdentifierMarshallerTest {

    @Test
    public void testSimpleModulo8() {

        testSimpleModulo8(PacketTypeIdentifierEnumeration.CallRequest_IncomingCall);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.CallAccepted_CallConnected);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.ClearConfirmation);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.Data, true, 2, 1);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.Interrupt);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.InterruptConfirmation);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.ReceiverReady, 7);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.ReceiverNotReady, 6);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.Reject, 3);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.ResetRequest_ResetIndication);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.ResetConfirmation);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.RestartRequest_RestartIndication);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.RestartConfirmation);
        testSimpleModulo8(PacketTypeIdentifierEnumeration.Diagnostic);
    }

    static void testSimpleModulo8(PacketTypeIdentifierEnumeration source) {

        final PacketTypeIdentifier identifier = PacketTypeIdentifierFactory.create(source);

        final byte intermediate = PacketTypeIdentifierMarshaller.marshalModule8(identifier);
        assertEquals(source.toInt(), (intermediate & 0x00ff));

        final PacketTypeIdentifier target = PacketTypeIdentifierMarshaller.unmarshalModule8(intermediate);
        assertEquals(false, target.isDataPacketTypeIdentifier());
        assertEquals(false, target.isReceiverPacketTypeIdentifier());

        assertEquals(source, target.getEnumeration());
    }

    static void testSimpleModulo8(PacketTypeIdentifierEnumeration source, boolean moreData, int packetReceiveSequenceNumber, int packetSendSequenceNumber) {

        final PacketTypeIdentifier identifier = PacketTypeIdentifierFactory.create(source, moreData, packetReceiveSequenceNumber, packetSendSequenceNumber);

        final byte intermediate = PacketTypeIdentifierMarshaller.marshalModule8(identifier);
        final PacketTypeIdentifier target = PacketTypeIdentifierMarshaller.unmarshalModule8(intermediate);

        assertEquals(true, target.isDataPacketTypeIdentifier());
        assertEquals(false, target.isReceiverPacketTypeIdentifier());

        assertEquals(source, target.getEnumeration());

        final DataPacketTypeIdentifier dataPacketTypeIdentifier = (DataPacketTypeIdentifier) target;
        assertEquals(moreData, dataPacketTypeIdentifier.isMoreData());
        assertEquals(packetReceiveSequenceNumber, dataPacketTypeIdentifier.getPacketReceiveSequenceNumber());
        assertEquals(packetSendSequenceNumber, dataPacketTypeIdentifier.getPacketSendSequenceNumber());
    }

    static void testSimpleModulo8(PacketTypeIdentifierEnumeration source, int packetReceiveSequenceNumber) {

        final PacketTypeIdentifier identifier = PacketTypeIdentifierFactory.create(source, packetReceiveSequenceNumber);

        final byte intermediate = PacketTypeIdentifierMarshaller.marshalModule8(identifier);
        final PacketTypeIdentifier target = PacketTypeIdentifierMarshaller.unmarshalModule8(intermediate);

        assertEquals(false, target.isDataPacketTypeIdentifier());
        assertEquals(true, target.isReceiverPacketTypeIdentifier());

        assertEquals(source, target.getEnumeration());

        final ReceiverPacketTypeIdentifier dataPacketTypeIdentifier = (ReceiverPacketTypeIdentifier) target;
        assertEquals(packetReceiveSequenceNumber, dataPacketTypeIdentifier.getPacketReceiveSequenceNumber());
    }
}
