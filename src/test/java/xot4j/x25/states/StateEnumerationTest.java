//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StateEnumerationTest {

    static void testSimple(StateEnumeration source) {

        final String intermediate = source.toDescription();
        final StateEnumeration result = StateEnumeration.fromDescription(intermediate);
        assertEquals(intermediate, source, result);
    }

    @Test
    public void testSimple() {

        // P-States
        testSimple(StateEnumeration.P1_Ready);
        testSimple(StateEnumeration.P2_DTEWaiting);
        testSimple(StateEnumeration.P3_DCEWaiting);
        testSimple(StateEnumeration.P4_DataTransfer);
        testSimple(StateEnumeration.P5_CallCollision);
        testSimple(StateEnumeration.P6_DTEClearing);
        testSimple(StateEnumeration.P7_DCEClearing);
        // D-States
        testSimple(StateEnumeration.D1_FlowControlReady);
        testSimple(StateEnumeration.D2_DTEReseting);
        testSimple(StateEnumeration.D3_DCEReseting);
        // R-States
        testSimple(StateEnumeration.R1_PacketLayerReady);
        testSimple(StateEnumeration.R2_DTERestarting);
        testSimple(StateEnumeration.R3_DCERestarting);
    }
}
