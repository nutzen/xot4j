//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.x25.transitions.TransitionEnumeration;
import org.junit.Test;
import xot4j.exceptions.X25StateTransitionFailure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class P_StateTest {

    @Test
    public void testP1_ReadyState() throws Exception {

        // Data
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Data;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // Interrupt
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Interrupt;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // InterruptConfirmation
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.InterruptConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ReceiverReady
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ReceiverReady;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ReceiverNotReady
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ReceiverNotReady;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // Reject
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Reject;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // Diagnostic
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Diagnostic;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }

        // ResetRequest
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetRequest;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ResetIndication
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetIndication;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ResetConfirmation
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }

        // CallRequest
        {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P2_DTEWaiting;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // CallIncoming
        {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            final StateEnumeration stateEnumeration = StateEnumeration.P3_DCEWaiting;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // CallAccepted
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallConnected
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ClearRequest
        {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P6_DTEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearIndication
        {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P7_DCEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearConfirmation
        try {
            final State state = P1_ReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testP2_DTEWaitingState() throws Exception {

        // CallRequest
        try {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallIncoming
        {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            final StateEnumeration stateEnumeration = StateEnumeration.P5_CallCollision;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // CallAccepted
        try {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallConnected
        {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearRequest
        {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P6_DTEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearIndication
        {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P7_DCEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearConfirmation
        try {
            final State state = P2_DTEWaitingState.create();
            assertEquals(StateEnumeration.P2_DTEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testP3_DCEWaitingState() throws Exception {

        // CallRequest
        {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P5_CallCollision;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // CallIncoming
        try {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallAccepted
        {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // CallConnected
        try {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ClearRequest
        {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P6_DTEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearIndication
        {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P7_DCEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearConfirmation
        try {
            final State state = P3_DCEWaitingState.create();
            assertEquals(StateEnumeration.P3_DCEWaiting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testP4_DataTransferState() throws Exception {

        // ResetRequest
        {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.D2_DTEReseting;

            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
            assertEquals(stateEnumeration, state.getEnumeration());
        }
        // ResetIndication
        {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.D3_DCEReseting;

            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
            assertEquals(stateEnumeration, state.getEnumeration());
        }
        // ResetConfirmation
        try {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }

        // CallRequest
        try {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallIncoming
        try {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallAccepted
        try {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallConnected
        try {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ClearRequest
        {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P6_DTEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearIndication
        {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P7_DCEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearConfirmation
        try {
            final State state = P4_DataTransferState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testP5_CallCollisionState() throws Exception {

        // CallRequest
        try {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallIncoming
        try {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallAccepted
        try {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallConnected
        {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearRequest
        {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P6_DTEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearIndication
        {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P7_DCEClearing;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearConfirmation
        try {
            final State state = P5_CallCollisionState.create();
            assertEquals(StateEnumeration.P5_CallCollision, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testP6_DTEClearingState() throws Exception {

        // CallRequest
        try {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallIncoming
        {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // CallAccepted
        try {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallConnected
        {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ClearRequest
        {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ClearIndication
        {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearConfirmation
        {
            final State state = P6_DTEClearingState.create();
            assertEquals(StateEnumeration.P6_DTEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
    }

    @Test
    public void testP7_DCEClearingState() throws Exception {

        // CallRequest
        {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // CallIncoming
        try {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallAccepted
        {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // CallConnected
        try {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ClearRequest
        {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ClearIndication
        {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ClearConfirmation
        {
            final State state = P7_DCEClearingState.create();
            assertEquals(StateEnumeration.P7_DCEClearing, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
    }
}
