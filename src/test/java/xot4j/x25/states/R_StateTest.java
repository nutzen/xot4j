//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.x25.transitions.TransitionEnumeration;
import org.junit.Test;
import xot4j.exceptions.X25StateTransitionFailure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class R_StateTest {

    @Test
    public void testR1_PacketLayerReadyState() throws Exception {

        // Data
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Data;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // Interrupt
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Interrupt;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // InterruptConfirmation
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.InterruptConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ReceiverReady
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ReceiverReady;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ReceiverNotReady
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ReceiverNotReady;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // Reject
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Reject;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // Diagnostic
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Diagnostic;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }

        // ResetRequest
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetRequest;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ResetIndication
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetIndication;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ResetConfirmation
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallRequest
        {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallRequest;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // CallIncoming
        {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallIncoming;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // CallAccepted
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallAccepted;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // CallConnected
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.CallConnected;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
        // ClearRequest
        {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearRequest;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ClearIndication
        {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearIndication;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ClearConfirmation
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ClearConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }

        // RestartRequest
        {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.R2_DTERestarting;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // RestartIndication
        {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.R3_DCERestarting;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // RestartConfirmation
        try {
            final State state = R1_PacketLayerReadyState.create();
            assertEquals(StateEnumeration.P1_Ready, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartConfirmation;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testR2_DTERestarting() throws Exception {

        // RestartRequest
        {
            final State state = R2_DTERestartingState.create();
            assertEquals(StateEnumeration.R2_DTERestarting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartRequest;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // RestartIndication
        {
            final State state = R2_DTERestartingState.create();
            assertEquals(StateEnumeration.R2_DTERestarting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // RestartConfirmation
        {
            final State state = R2_DTERestartingState.create();
            assertEquals(StateEnumeration.R2_DTERestarting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartConfirmation;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
    }

    @Test
    public void testR3_DCERestartingState() throws Exception {

        // RestartRequest
        {
            final State state = R3_DCERestartingState.create();
            assertEquals(StateEnumeration.R3_DCERestarting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // RestartIndication
        {
            final State state = R3_DCERestartingState.create();
            assertEquals(StateEnumeration.R3_DCERestarting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartIndication;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // RestartConfirmation
        {
            final State state = R3_DCERestartingState.create();
            assertEquals(StateEnumeration.R3_DCERestarting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.RestartConfirmation;
            final StateEnumeration stateEnumeration = StateEnumeration.P1_Ready;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
    }
}
