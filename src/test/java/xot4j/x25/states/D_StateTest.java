//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.x25.transitions.TransitionEnumeration;
import org.junit.Test;
import xot4j.exceptions.X25StateTransitionFailure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class D_StateTest {

    @Test
    public void testD1_FlowControlReadyState() throws Exception {

        // Data
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Data;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // Interrupt
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Interrupt;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // InterruptConfirmation
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.InterruptConfirmation;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ReceiverReady
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ReceiverReady;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ReceiverNotReady
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ReceiverNotReady;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // Reject
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Reject;
            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // Diagnostic
        try {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.Diagnostic;
            state.handle(stateContext, transition);
            fail();

        } catch (X25StateTransitionFailure e) {
            // ignore
        }

        // ResetRequest
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.D2_DTEReseting;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ResetIndication
        {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.D3_DCEReseting;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ResetConfirmation
        try {
            final State state = D1_FlowControlReadyState.create();
            assertEquals(StateEnumeration.D1_FlowControlReady, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetConfirmation;
            state.handle(stateContext, transition);
            fail("This should not happen");

        } catch (X25StateTransitionFailure e) {
            // ignore
        }
    }

    @Test
    public void testD2_DTEResetingState() throws Exception {

        // ResetRequest
        {
            final State state = D2_DTEResetingState.create();
            assertEquals(StateEnumeration.D2_DTEReseting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetRequest;

            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ResetIndication
        {
            final State state = D2_DTEResetingState.create();
            assertEquals(StateEnumeration.D2_DTEReseting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetIndication;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ResetConfirmation
        {
            final State state = D2_DTEResetingState.create();
            assertEquals(StateEnumeration.D2_DTEReseting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetConfirmation;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
    }

    @Test
    public void testD3_DCEResetingState() throws Exception {

        // ResetRequest
        {
            final State state = D3_DCEResetingState.create();
            assertEquals(StateEnumeration.D3_DCEReseting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetRequest;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
        // ResetIndication
        {
            final State state = D3_DCEResetingState.create();
            assertEquals(StateEnumeration.D3_DCEReseting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetIndication;

            state.handle(stateContext, transition);
            assertNull(stateContext.getState());
        }
        // ResetConfirmation
        {
            final State state = D3_DCEResetingState.create();
            assertEquals(StateEnumeration.D3_DCEReseting, state.getEnumeration());

            final StateContext stateContext = BasicStateContext.create(null);
            assertNull(stateContext.getState());

            final TransitionEnumeration transition = TransitionEnumeration.ResetConfirmation;
            final StateEnumeration stateEnumeration = StateEnumeration.D1_FlowControlReady;

            state.handle(stateContext, transition);
            assertNotNull(stateContext.getState());
            assertEquals(stateEnumeration, stateContext.getState().getEnumeration());
        }
    }
}
