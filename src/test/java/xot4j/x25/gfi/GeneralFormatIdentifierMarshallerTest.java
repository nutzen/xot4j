//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.gfi;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GeneralFormatIdentifierMarshallerTest {

    @Test
    public void testSimple() {

        final byte gfi = 0x01;

        final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifierMarshaller.unmarshal(gfi);
        assertEquals(GeneralFormatIdentifier.QualifiedData.DataForUser, generalFormatIdentifier.getQualifiedData());
        assertEquals(GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment, generalFormatIdentifier.getDeliveryConfirmation());
        assertEquals(GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing, generalFormatIdentifier.getProtocolIdentification());

        final byte target = GeneralFormatIdentifierMarshaller.marshal(generalFormatIdentifier);
        assertEquals(gfi, target);
    }
}
