//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.pti.PacketTypeIdentifier;
import xot4j.x25.gfi.GeneralFormatIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;
import xot4j.x25.pti.PacketTypeIdentifierFactory;

import org.junit.Test;
import xot4j.x121.X121Address;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.codes.ResetRequestEnumeration;
import xot4j.x25.codes.RestartRequestEnumeration;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.PacketSizeSelection;
import xot4j.x25.facilities.PacketSizeSelectionEnumeration;
import xot4j.x25.facilities.WindowSizeSelection;
import xot4j.x25.payload.CallPacketPayload;
import xot4j.x25.payload.ClearConfirmationPayload;
import xot4j.x25.payload.ClearPacketPayload;
import xot4j.x25.payload.DataPacketPayload;
import xot4j.x25.payload.ResetPacketPayload;
import xot4j.x25.payload.RestartPacketPayload;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

public class X25PacketMarshallerTest {

    @Test
    public void testCallPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x10, 0x01, 0x0b, (byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, (byte) 0xc4};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.CallRequest_IncomingCall);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final CallPacketPayload payload;
        {
            final X121Address calledAddress = X121Address.create("1634567");
            final X121Address callingAddress = X121Address.create("11009000");
            final Collection<Facility> facilities = new ArrayList<Facility>();
            facilities.add(WindowSizeSelection.create(1, 1));
            facilities.add(PacketSizeSelection.create(PacketSizeSelectionEnumeration.Size128octets, PacketSizeSelectionEnumeration.Size128octets));
            byte[] userData = new byte[]{(byte) 0xc4};
            payload = CallPacketPayload.create(calledAddress, callingAddress, facilities, userData);
        }

        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }

    @Test
    public void testCallPacketPayloadForCallConnect() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x18, 0x00, 0x0f};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 8, (byte) 0);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.CallAccepted_CallConnected);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final CallPacketPayload payload = CallPacketPayload.create();
        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }

    @Test
    public void testClearPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x10, 0x01, 0x13, 0x39, 0x30};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final ClearPacketPayload payload;
        {
            payload = ClearPacketPayload.create(ClearRequestEnumeration.ClearRequestShipAbsent, DiagnosticCodeEnumeration.TimerExpired);
        }

        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }

    @Test
    public void testClearConfirmationPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x10, 0x01, 0x17};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.ClearConfirmation);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final ClearConfirmationPayload payload;
        {
            payload = ClearConfirmationPayload.create();
        }

        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }

    @Test
    public void testResetPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x10, 0x01, 0x1b, 0x07, 0x30};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.ResetRequest_ResetIndication);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final ResetPacketPayload payload;
        {
            payload = ResetPacketPayload.create(ResetRequestEnumeration.ResetRequestNetworkCongestion, DiagnosticCodeEnumeration.TimerExpired);
        }

        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }

    @Test
    public void testRestartPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x10, 0x01, (byte) 0xfb, 0x03, 0x00};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.RestartRequest_RestartIndication);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final RestartPacketPayload payload;
        {
            payload = RestartPacketPayload.create(RestartRequestEnumeration.RestartRequestNetworkCongestion, DiagnosticCodeEnumeration.NoAdditionalInformation);
        }

        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }

    @Test
    public void testDataPacketPayload() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x10, 0x01, 0x22, 0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.Data, false, 1, 1);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final DataPacketPayload payload;
        {
            payload = DataPacketPayload.create("Hello world".getBytes());
        }

        final X25Packet source = X25Packet.create(header, payload);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final X25Packet result = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }
}
