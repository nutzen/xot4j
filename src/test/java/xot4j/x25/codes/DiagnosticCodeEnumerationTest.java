//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DiagnosticCodeEnumerationTest {

    private static void testSimple(final DiagnosticCodeEnumeration source) throws Exception {

        // description
        {
            final String intermediate = source.toDescription();
            final DiagnosticCodeEnumeration target = DiagnosticCodeEnumeration.fromDescription(intermediate);
            assertEquals(source, target);
        }
        // mask
        {
            final int intermediate = source.toInt();
            final DiagnosticCodeEnumeration target = DiagnosticCodeEnumeration.fromInt(intermediate);
            assertEquals(source, target);
        }
    }

    @Test
    public void testSimple() throws Exception {

        testSimple(DiagnosticCodeEnumeration.NoAdditionalInformation);
        testSimple(DiagnosticCodeEnumeration.InvalidPacketSendSequenceNumber);
        testSimple(DiagnosticCodeEnumeration.InvalidPacketReceiveSequenceNumber);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalid);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateR1PacketLevelReady);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateR2DteRestartRequest);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateR3DceRestartIndication);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP1Ready);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP2DteWaiting);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP3DceWaiting);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP4DataTransfer);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP5CallCollision);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP6DteClearRequest);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateP7DceClearIndication);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateD1FlowControlReady);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateD2DteResetReady);
        testSimple(DiagnosticCodeEnumeration.PacketTypeInvalidForStateD3DceResetIndication);
        testSimple(DiagnosticCodeEnumeration.PacketNotAllowed);
        testSimple(DiagnosticCodeEnumeration.UnidentifiablePacket);
        testSimple(DiagnosticCodeEnumeration.CallOnOneWayLogicalChannel);
        testSimple(DiagnosticCodeEnumeration.InvalidPacketTypeOnAPermanentVirtualCircuit);
        testSimple(DiagnosticCodeEnumeration.PacketOnUnassignedLogicalChannelNumber);
        testSimple(DiagnosticCodeEnumeration.RejectNotSubscribedTo);
        testSimple(DiagnosticCodeEnumeration.PacketTooShort);
        testSimple(DiagnosticCodeEnumeration.PacketTooLong);
        testSimple(DiagnosticCodeEnumeration.InvalidGeneralFormatIdentifier);
        testSimple(DiagnosticCodeEnumeration.RestartOrRegistrationPacketWithNonzeroLci);
        testSimple(DiagnosticCodeEnumeration.PacketTypeNotCompatibleWithFacility);
        testSimple(DiagnosticCodeEnumeration.UnauthorizedInterruptConfirmation);
        testSimple(DiagnosticCodeEnumeration.UnauthorizedInterrupt);
        testSimple(DiagnosticCodeEnumeration.UnauthorizedReject);
        testSimple(DiagnosticCodeEnumeration.TimerExpired);
        testSimple(DiagnosticCodeEnumeration.TimerExpiredForIncomingCall);
        testSimple(DiagnosticCodeEnumeration.TimerExpiredForClearIndication);
        testSimple(DiagnosticCodeEnumeration.TimerExpiredForResetIndication);
        testSimple(DiagnosticCodeEnumeration.TimerExpiredForRestartIndication);
        testSimple(DiagnosticCodeEnumeration.TimerExpiredForCallDeflection);
        testSimple(DiagnosticCodeEnumeration.CallSetupClearingOrRegistrationProblem);
        testSimple(DiagnosticCodeEnumeration.FacilityCodeNotAllowed);
        testSimple(DiagnosticCodeEnumeration.FacilityParameterNotAllowed);
        testSimple(DiagnosticCodeEnumeration.InvalidCalledAddress);
        testSimple(DiagnosticCodeEnumeration.InvalidCallingAddress);
        testSimple(DiagnosticCodeEnumeration.InvalidFacilityLength);
        testSimple(DiagnosticCodeEnumeration.IncomingCallBarred);
        testSimple(DiagnosticCodeEnumeration.NoLogicalChannelAvailable);
        testSimple(DiagnosticCodeEnumeration.CallCollision);
        testSimple(DiagnosticCodeEnumeration.DuplicateFacilityRequested);
        testSimple(DiagnosticCodeEnumeration.NonzeroAddressLength);
        testSimple(DiagnosticCodeEnumeration.NonzeroFacilityLength);
        testSimple(DiagnosticCodeEnumeration.FacilityNotProvidedWhenExpected);
        testSimple(DiagnosticCodeEnumeration.InvalidItuTSpecifiedDteFacility);
        testSimple(DiagnosticCodeEnumeration.MaximumNumberOfCallRedirectionsOrDeflectionsExceeded);
        testSimple(DiagnosticCodeEnumeration.Miscellaneous);
        testSimple(DiagnosticCodeEnumeration.ImproperCauseCodeForDte);
        testSimple(DiagnosticCodeEnumeration.OctetNotAaligned);
        testSimple(DiagnosticCodeEnumeration.InconsistentQBitSetting);
        testSimple(DiagnosticCodeEnumeration.NetworkUserIdentificationProblem);
        testSimple(DiagnosticCodeEnumeration.InternationalProblem);
        testSimple(DiagnosticCodeEnumeration.RemoteNetworkProblem);
        testSimple(DiagnosticCodeEnumeration.InternationalProtocolProblem);
        testSimple(DiagnosticCodeEnumeration.InternationalLinkOutOfOrder);
        testSimple(DiagnosticCodeEnumeration.InternationalLinkBusy);
        testSimple(DiagnosticCodeEnumeration.TransitNetworkFacilityProblem);
        testSimple(DiagnosticCodeEnumeration.RemoteNetworkFacilityProblem);
        testSimple(DiagnosticCodeEnumeration.InternationalRoutingProblem);
        testSimple(DiagnosticCodeEnumeration.TemporaryRoutingProblem);
        testSimple(DiagnosticCodeEnumeration.UnknownCalledDataNetworkIdentificationCode);
        testSimple(DiagnosticCodeEnumeration.MaintenanceActionClearX25VcCommandIssued);
        // 0xf..
        testSimple(DiagnosticCodeEnumeration.NormalTermination);
        testSimple(DiagnosticCodeEnumeration.OutOfResources);
        testSimple(DiagnosticCodeEnumeration.AuthenticationFailure);
        testSimple(DiagnosticCodeEnumeration.InboundUserDataTooLarge);
        testSimple(DiagnosticCodeEnumeration.IdleTimerExpired);
    }
}
