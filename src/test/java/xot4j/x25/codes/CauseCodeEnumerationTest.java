//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CauseCodeEnumerationTest {

    private static void testSimple(final ClearRequestEnumeration source) throws Exception {

        // description
        {
            final String intermediate = source.toDescription();
            final ClearRequestEnumeration target = ClearRequestEnumeration.fromDescription(intermediate);
            assertEquals(source, target);
        }
        // mask
        {
            final int intermediate = source.toInt();
            final ClearRequestEnumeration target = ClearRequestEnumeration.fromInt(intermediate);
            assertEquals(source, target);
        }
    }

    private static void testSimple(final ResetRequestEnumeration source) throws Exception {

        // description
        {
            final String intermediate = source.toDescription();
            final ResetRequestEnumeration target = ResetRequestEnumeration.fromDescription(intermediate);
            assertEquals(source, target);
        }
        // mask
        {
            final int intermediate = source.toInt();
            final ResetRequestEnumeration target = ResetRequestEnumeration.fromInt(intermediate);
            assertEquals(source, target);
        }
    }

    private static void testSimple(final RestartRequestEnumeration source) throws Exception {

        // description
        {
            final String intermediate = source.toDescription();
            final RestartRequestEnumeration target = RestartRequestEnumeration.fromDescription(intermediate);
            assertEquals(source, target);
        }
        // mask
        {
            final int intermediate = source.toInt();
            final RestartRequestEnumeration target = RestartRequestEnumeration.fromInt(intermediate);
            assertEquals(source, target);
        }
    }

    @Test
    public void testSimple() throws Exception {

        // ClearRequestEnumeration
        testSimple(ClearRequestEnumeration.ClearRequestDteOriginated);
        testSimple(ClearRequestEnumeration.ClearRequestNumberBusy);
        testSimple(ClearRequestEnumeration.ClearRequestInvalidFacilityRequest);
        testSimple(ClearRequestEnumeration.ClearRequestNetworkCongestion);
        testSimple(ClearRequestEnumeration.ClearRequestOutOfOrder);
        testSimple(ClearRequestEnumeration.ClearRequestAccessBarred);
        testSimple(ClearRequestEnumeration.ClearRequestNotObtainable);
        testSimple(ClearRequestEnumeration.ClearRequestRemoteProcedureError);
        testSimple(ClearRequestEnumeration.ClearRequestLocalProcedureError);
        testSimple(ClearRequestEnumeration.ClearRequestRpoaOutOfOrder);
        testSimple(ClearRequestEnumeration.ClearRequestReverseChargingNotAccepted);
        testSimple(ClearRequestEnumeration.ClearRequestIncompatibleDestination);
        testSimple(ClearRequestEnumeration.ClearRequestFastSelectNotAccepted);
        testSimple(ClearRequestEnumeration.ClearRequestShipAbsent);

        // ResetRequestEnumeration
        testSimple(ResetRequestEnumeration.ResetRequestDteOriginated);
        testSimple(ResetRequestEnumeration.ResetRequestOutOfOrder);
        testSimple(ResetRequestEnumeration.ResetRequestRemoteProcedureError);
        testSimple(ResetRequestEnumeration.ResetRequestLocalProcedureError);
        testSimple(ResetRequestEnumeration.ResetRequestNetworkCongestion);
        testSimple(ResetRequestEnumeration.ResetRequestRemoteDteOperational);
        testSimple(ResetRequestEnumeration.ResetRequestNetworkOperational);
        testSimple(ResetRequestEnumeration.ResetRequestIncompatibleDestination);
        testSimple(ResetRequestEnumeration.ResetRequestNetworkOutOfOrder);

        // RestartRequestEnumeration
        testSimple(RestartRequestEnumeration.RestartRequestDteRestarting);
        testSimple(RestartRequestEnumeration.RestartRequestLocalProcedureError);
        testSimple(RestartRequestEnumeration.RestartRequestNetworkCongestion);
        testSimple(RestartRequestEnumeration.RestartRequestNetworkOperational);
        testSimple(RestartRequestEnumeration.RestartRequestRegistrationCancellationConfirmed);
    }
}
