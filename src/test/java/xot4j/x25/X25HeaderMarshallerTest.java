//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.pti.PacketTypeIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierMarshaller;
import xot4j.x25.gfi.GeneralFormatIdentifierMarshaller;
import xot4j.x25.gfi.GeneralFormatIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;
import xot4j.x25.pti.PacketTypeIdentifierFactory;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class X25HeaderMarshallerTest {

    @Test
    public void testSimple() throws Exception {

        final byte[] expected = new byte[]{
            0x11,
            0x05,
            0x0f
        };
        final byte gfi = 0x01;
        final byte lcgn = 0x01;
        final byte lcn = 0x05;
        final byte pti = 0x0f;

        final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifierMarshaller.unmarshal(gfi);
        {
            assertEquals(GeneralFormatIdentifier.QualifiedData.DataForUser, generalFormatIdentifier.getQualifiedData());
            assertEquals(GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment, generalFormatIdentifier.getDeliveryConfirmation());
            assertEquals(GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing, generalFormatIdentifier.getProtocolIdentification());
        }

        final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create(lcgn, lcn);
        {
            assertEquals(lcgn, logicalChannelIdentifier.getLogicalChannelGroupNumber());
            assertEquals(lcn, logicalChannelIdentifier.getLogicalChannelNumber());
        }

        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.fromInt(pti & 0x00ff));

        final X25Header source = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        {
            assertEquals(generalFormatIdentifier, source.getGeneralFormatIdentifier());
            assertEquals(logicalChannelIdentifier, source.getLogicalChannelIdentifier());
            assertEquals(pti, PacketTypeIdentifierMarshaller.marshalModule8(source.getPacketTypeIdentifier()));
        }

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            X25HeaderMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();

            assertArrayEquals(expected, intermediate);
        }

        final X25Header target = X25HeaderMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        {
            assertEquals(GeneralFormatIdentifier.QualifiedData.DataForUser, target.getGeneralFormatIdentifier().getQualifiedData());
            assertEquals(GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment, target.getGeneralFormatIdentifier().getDeliveryConfirmation());
            assertEquals(GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing, target.getGeneralFormatIdentifier().getProtocolIdentification());

            assertEquals(lcgn, target.getLogicalChannelIdentifier().getLogicalChannelGroupNumber());
            assertEquals(lcn, target.getLogicalChannelIdentifier().getLogicalChannelNumber());

            assertEquals(pti, PacketTypeIdentifierMarshaller.marshalModule8(target.getPacketTypeIdentifier()));
        }
    }
}
