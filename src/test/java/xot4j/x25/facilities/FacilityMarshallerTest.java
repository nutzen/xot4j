//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import java.util.ArrayList;
import java.util.Collection;
import org.junit.Test;
import xot4j.x25.utility.HexUtility;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class FacilityMarshallerTest {

    static Facility remarshal(Facility source) {

        final Facility target;
        final byte[] intermediate;
        {
            final Collection<Facility> collection = new ArrayList<Facility>();
            collection.add(source);

            intermediate = FacilityMarshaller.marshal(collection);
        }
        {
            final Collection<Facility> collection = FacilityMarshaller.unmarshal(intermediate);

            assertEquals(HexUtility.toHex(intermediate), 1, collection.size());
            target = collection.iterator().next();
        }
        return target;
    }

    // one parameter
    @Test
    public void testReverseChargingAndFastSelect() {

        final byte parameter = (byte) 0x01;
        final ReverseChargingAndFastSelect source = ReverseChargingAndFastSelect.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ReverseChargingAndFastSelect result = (ReverseChargingAndFastSelect) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testThroughputClass() {

        final byte parameter = (byte) 0x02;
        final ThroughputClass source = ThroughputClass.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ThroughputClass result = (ThroughputClass) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testClosedUserGroupSelection() {

        final byte parameter = (byte) 0x04;
        final ClosedUserGroupSelection source = ClosedUserGroupSelection.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ClosedUserGroupSelection result = (ClosedUserGroupSelection) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testChargingInformationRequest() {

        final byte parameter = (byte) 0x08;
        final ChargingInformationRequest source = ChargingInformationRequest.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ChargingInformationRequest result = (ChargingInformationRequest) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testCalledLineAddressModifiedNotification() {

        final byte parameter = (byte) 0x10;
        final CalledLineAddressModifiedNotification source = CalledLineAddressModifiedNotification.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final CalledLineAddressModifiedNotification result = (CalledLineAddressModifiedNotification) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testClosedUserGroupWithOutgoingAccess() {

        final byte parameter = (byte) 0x20;
        final ClosedUserGroupWithOutgoingAccess source = ClosedUserGroupWithOutgoingAccess.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ClosedUserGroupWithOutgoingAccess result = (ClosedUserGroupWithOutgoingAccess) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testQualityOfServiceNegotiationMinimumThroughputClass() {

        final byte parameter = (byte) 0x40;
        final QualityOfServiceNegotiationMinimumThroughputClass source = QualityOfServiceNegotiationMinimumThroughputClass.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final QualityOfServiceNegotiationMinimumThroughputClass result = (QualityOfServiceNegotiationMinimumThroughputClass) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testExpeditedDataNegotiation() {

        final byte parameter = (byte) 0x80;
        final ExpeditedDataNegotiation source = ExpeditedDataNegotiation.create(parameter);
        assertEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ExpeditedDataNegotiation result = (ExpeditedDataNegotiation) intermediate;
        assertEquals(source.getParameter(), result.getParameter());
    }

    // two parameters
    @Test
    public void testBilateralClosedUserGroupSelection() {

        final byte parameter0 = (byte) 0x01;
        final byte parameter1 = (byte) 0x02;
        final BilateralClosedUserGroupSelection source = BilateralClosedUserGroupSelection.create(parameter0, parameter1);
        assertEquals(parameter0, source.getParameter0());
        assertEquals(parameter1, source.getParameter1());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final BilateralClosedUserGroupSelection result = (BilateralClosedUserGroupSelection) intermediate;
        assertEquals(source.getParameter0(), result.getParameter0());
        assertEquals(source.getParameter1(), result.getParameter1());
    }

    @Test
    public void testPacketSizeSelection() {

        final PacketSizeSelectionEnumeration transmitPacketSize = PacketSizeSelectionEnumeration.Size1024octets;
        final PacketSizeSelectionEnumeration receivePacketSize = PacketSizeSelectionEnumeration.Size2048octets;
        final PacketSizeSelection source = PacketSizeSelection.create(transmitPacketSize, receivePacketSize);
        assertEquals(transmitPacketSize, source.getTransmitPacketSize());
        assertEquals(receivePacketSize, source.getReceivePacketSize());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final PacketSizeSelection result = (PacketSizeSelection) intermediate;
        assertEquals(source.getTransmitPacketSize(), result.getTransmitPacketSize());
        assertEquals(source.getReceivePacketSize(), result.getReceivePacketSize());
    }

    @Test
    public void testWindowSizeSelection() {

        final int transmissionWindowSize = 0x02;
        final int receiveWindowSize = 0x04;
        final WindowSizeSelection source = WindowSizeSelection.create(transmissionWindowSize, receiveWindowSize);
        assertEquals(transmissionWindowSize, source.getTransmissionWindowSize());
        assertEquals(receiveWindowSize, source.getReceiveWindowSize());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final WindowSizeSelection result = (WindowSizeSelection) intermediate;
        assertEquals(source.getTransmissionWindowSize(), result.getTransmissionWindowSize());
        assertEquals(source.getReceiveWindowSize(), result.getReceiveWindowSize());
    }

    @Test
    public void testRecognizedPrivateOperatingAgencySelectionBasicFormat() {

        final byte parameter0 = (byte) 0x40;
        final byte parameter1 = (byte) 0x80;
        final RecognizedPrivateOperatingAgencySelectionBasicFormat source = RecognizedPrivateOperatingAgencySelectionBasicFormat.create(parameter0, parameter1);
        assertEquals(parameter0, source.getParameter0());
        assertEquals(parameter1, source.getParameter1());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final RecognizedPrivateOperatingAgencySelectionBasicFormat result = (RecognizedPrivateOperatingAgencySelectionBasicFormat) intermediate;
        assertEquals(source.getParameter0(), result.getParameter0());
        assertEquals(source.getParameter1(), result.getParameter1());
    }

    @Test
    public void testTransitDelaySelectionAndIndication() {

        final byte parameter0 = (byte) 0x11;
        final byte parameter1 = (byte) 0x12;
        final TransitDelaySelectionAndIndication source = TransitDelaySelectionAndIndication.create(parameter0, parameter1);
        assertEquals(parameter0, source.getParameter0());
        assertEquals(parameter1, source.getParameter1());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final TransitDelaySelectionAndIndication result = (TransitDelaySelectionAndIndication) intermediate;
        assertEquals(source.getParameter0(), result.getParameter0());
        assertEquals(source.getParameter1(), result.getParameter1());
    }

    // variable parameters
    @Test
    public void testChargingCallDuration() {

        final byte[] parameter = new byte[]{(byte) 0x01};
        final ChargingCallDuration source = ChargingCallDuration.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ChargingCallDuration result = (ChargingCallDuration) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testChargingSegmentCount() {

        final byte[] parameter = new byte[]{(byte) 0x02, (byte) 0x03};
        final ChargingSegmentCount source = ChargingSegmentCount.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ChargingSegmentCount result = (ChargingSegmentCount) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testCallRedirectionNotification() {

        final byte[] parameter = new byte[]{(byte) 0x04, (byte) 0x05, (byte) 0x06};
        final CallRedirectionNotification source = CallRedirectionNotification.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final CallRedirectionNotification result = (CallRedirectionNotification) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testRecognizedPrivateOperatingAgencySelectionExtendedFormat() {

        final byte[] parameter = new byte[]{(byte) 0x07, (byte) 0x08, (byte) 0x09, (byte) 0x10};
        final RecognizedPrivateOperatingAgencySelectionExtendedFormat source = RecognizedPrivateOperatingAgencySelectionExtendedFormat.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final RecognizedPrivateOperatingAgencySelectionExtendedFormat result = (RecognizedPrivateOperatingAgencySelectionExtendedFormat) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testChargingMonetaryUnit() {

        final byte[] parameter = new byte[]{(byte) 0x11, (byte) 0x12, (byte) 0x13, (byte) 0x14, (byte) 0x15};
        final ChargingMonetaryUnit source = ChargingMonetaryUnit.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final ChargingMonetaryUnit result = (ChargingMonetaryUnit) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testNetworkUserIdentification() {

        final byte[] parameter = new byte[]{(byte) 0x16, (byte) 0x17, (byte) 0x18, (byte) 0x19, (byte) 0x20, (byte) 0x21};
        final NetworkUserIdentification source = NetworkUserIdentification.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final NetworkUserIdentification result = (NetworkUserIdentification) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testCalledAddressExtensionOsi() {

        final byte[] parameter = new byte[]{(byte) 0x02, (byte) 0x03};
        final CalledAddressExtensionOsi source = CalledAddressExtensionOsi.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final CalledAddressExtensionOsi result = (CalledAddressExtensionOsi) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testQualityOfServiceNegotiationEndToEndTransitDelay() {

        final byte[] parameter = new byte[]{(byte) 0x02, (byte) 0x03};
        final QualityOfServiceNegotiationEndToEndTransitDelay source = QualityOfServiceNegotiationEndToEndTransitDelay.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final QualityOfServiceNegotiationEndToEndTransitDelay result = (QualityOfServiceNegotiationEndToEndTransitDelay) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }

    @Test
    public void testCallingAddressExtensionOsi() {

        final byte[] parameter = new byte[]{(byte) 0x02, (byte) 0x03};
        final CallingAddressExtensionOsi source = CallingAddressExtensionOsi.create(parameter);
        assertArrayEquals(parameter, source.getParameter());

        final Facility intermediate = remarshal(source);
        assertEquals(source.getEnumeration(), intermediate.getEnumeration());

        final CallingAddressExtensionOsi result = (CallingAddressExtensionOsi) intermediate;
        assertArrayEquals(source.getParameter(), result.getParameter());
    }
}
