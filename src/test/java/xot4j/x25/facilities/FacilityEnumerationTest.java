//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FacilityEnumerationTest {

    static void testSimple(FacilityEnumeration source) {

        {
            final String intermediate = source.toDescription();
            final FacilityEnumeration enumeration = FacilityEnumeration.fromDescription(intermediate);

            assertEquals("intermediate=" + intermediate, source, enumeration);
        }

        {
            final int intermediate = source.toInt();
            final FacilityEnumeration enumeration = FacilityEnumeration.fromInt(intermediate);

            assertEquals("intermediate=" + intermediate, source, enumeration);
        }
    }

    @Test
    public void testSimple() {

        testSimple(FacilityEnumeration.ReverseChargingAndFastSelect);
        testSimple(FacilityEnumeration.ThroughputClass);
        testSimple(FacilityEnumeration.ClosedUserGroupSelection);
        testSimple(FacilityEnumeration.ChargingInformationRequest);
        testSimple(FacilityEnumeration.CalledLineAddressModifiedNotification);
        testSimple(FacilityEnumeration.ClosedUserGroupWithOutgoingAccess);
        testSimple(FacilityEnumeration.QualityOfServiceNegotiationMinimumThroughputClass);
        testSimple(FacilityEnumeration.ExpeditedDataNegotiation);
        testSimple(FacilityEnumeration.BilateralClosedUserGroupSelection);
        testSimple(FacilityEnumeration.PacketSizeSelection);
        testSimple(FacilityEnumeration.WindowSizeSelection);
        testSimple(FacilityEnumeration.RecognizedPrivateOperatingAgencySelectionBasicFormat);
        testSimple(FacilityEnumeration.TransitDelaySelectionAndIndication);
        testSimple(FacilityEnumeration.ChargingCallDuration);
        testSimple(FacilityEnumeration.ChargingSegmentCount);
        testSimple(FacilityEnumeration.CallRedirectionNotification);
        testSimple(FacilityEnumeration.RecognizedPrivateOperatingAgencySelectionExtendedFormat);
        testSimple(FacilityEnumeration.ChargingMonetaryUnit);
        testSimple(FacilityEnumeration.NetworkUserIdentification);
        testSimple(FacilityEnumeration.CalledAddressExtensionOsi);
        testSimple(FacilityEnumeration.QualityOfServiceNegotiationEndToEndTransitDelay);
        testSimple(FacilityEnumeration.CallingAddressExtensionOsi);
    }
}
