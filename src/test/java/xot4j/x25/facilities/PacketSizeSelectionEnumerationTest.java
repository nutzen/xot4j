//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PacketSizeSelectionEnumerationTest {

    static void testSimple(PacketSizeSelectionEnumeration source) {

        {
            final String intermediate = source.toDescription();
            final PacketSizeSelectionEnumeration enumeration = PacketSizeSelectionEnumeration.fromDescription(intermediate);

            assertEquals("intermediate=" + intermediate, source, enumeration);
        }

        {
            final int intermediate = source.toInt();
            final PacketSizeSelectionEnumeration enumeration = PacketSizeSelectionEnumeration.fromInt(intermediate);

            assertEquals("intermediate=" + intermediate, source, enumeration);
        }

        {
            final int intermediate = source.toPacketSize();
            final PacketSizeSelectionEnumeration enumeration = PacketSizeSelectionEnumeration.fromPacketSize(intermediate);

            assertEquals("intermediate=" + intermediate, source, enumeration);
        }
    }

    @Test
    public void testSimple() {

        testSimple(PacketSizeSelectionEnumeration.Size16octets);
        testSimple(PacketSizeSelectionEnumeration.Size32octets);
        testSimple(PacketSizeSelectionEnumeration.Size64octets);
        testSimple(PacketSizeSelectionEnumeration.Size128octets);
        testSimple(PacketSizeSelectionEnumeration.Size256octets);
        testSimple(PacketSizeSelectionEnumeration.Size512octets);
        testSimple(PacketSizeSelectionEnumeration.Size1024octets);
        testSimple(PacketSizeSelectionEnumeration.Size2048octets);
        testSimple(PacketSizeSelectionEnumeration.Size4096octets);
    }
}
