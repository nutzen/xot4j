//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.utility;

import java.util.Arrays;
import org.junit.Test;
import xot4j.x121.X121Address;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class X25MessageUtilityTest {

    @Test
    public void testGetCallRequest() throws Exception {

        final int DEFAULT_BUFFER_SIZE = 20;
        final byte lcgn = 0;
        final byte lcn = 1; // 8;
        final X121Address calledAddress = X121Address.create("1634567");
        final X121Address callingAddress = X121Address.create("11009000");

        final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int length = X25MessageUtility.getCallRequest(lcgn, lcn, calledAddress, callingAddress, buffer);

        final byte[] expectedMessageBlock = new byte[]{0x10, 0x01, 0x0b, (byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, (byte) 0xc4};
        assertEquals(expectedMessageBlock.length, length);
        assertArrayEquals(expectedMessageBlock, Arrays.copyOf(buffer, length));
    }
}
