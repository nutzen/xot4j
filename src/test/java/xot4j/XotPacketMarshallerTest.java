//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.pti.PacketTypeIdentifier;
import xot4j.x25.gfi.GeneralFormatIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;
import xot4j.x25.pti.PacketTypeIdentifierFactory;

import org.junit.Test;
import xot4j.x121.X121Address;
import xot4j.x25.X25Header;
import xot4j.x25.X25Packet;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.PacketSizeSelection;
import xot4j.x25.facilities.PacketSizeSelectionEnumeration;
import xot4j.x25.facilities.WindowSizeSelection;
import xot4j.x25.payload.CallPacketPayload;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;

public class XotPacketMarshallerTest {

    @Test
    public void testCallPacket() throws Exception {

        final byte[] expectedIntermediate = new byte[]{0x00, 0x00, 0x00, 0x14, 0x10, 0x01, 0x0b, (byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00, 0x06, 0x43, 0x01, 0x01, 0x42, 0x07, 0x07, (byte) 0xc4};

        final X25Header header;
        {
            final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                    GeneralFormatIdentifier.QualifiedData.DataForUser,
                    GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                    GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
            final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create((byte) 0, (byte) 1);
            final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(PacketTypeIdentifierEnumeration.CallRequest_IncomingCall);
            header = X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
        }

        final CallPacketPayload payload;
        {
            final X121Address calledAddress = X121Address.create("1634567");
            final X121Address callingAddress = X121Address.create("11009000");
            final Collection<Facility> facilities = new ArrayList<Facility>();
            facilities.add(WindowSizeSelection.create(1, 1));
            facilities.add(PacketSizeSelection.create(PacketSizeSelectionEnumeration.Size128octets, PacketSizeSelectionEnumeration.Size128octets));
            byte[] userData = new byte[]{(byte) 0xc4};
            payload = CallPacketPayload.create(calledAddress, callingAddress, facilities, userData);
        }

        final X25Packet packet = X25Packet.create(header, payload);
        final XotPacket source = XotPacket.create(packet);

        final byte[] intermediate;
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            XotPacketMarshaller.marshal(outputStream, source);
            intermediate = outputStream.toByteArray();
        }
        assertArrayEquals(expectedIntermediate, intermediate);

        final XotPacket result = XotPacketMarshaller.unmarshal(new ByteArrayInputStream(intermediate));
        assertNotNull(result);
    }
}
