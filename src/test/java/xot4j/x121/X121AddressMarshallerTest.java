//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x121;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class X121AddressMarshallerTest {

    static byte[] marshalAddressBlock(final X121Address calledAddress, final X121Address callingAddress, boolean aBit) throws IOException {

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        X121AddressMarshaller.marshal(outputStream, calledAddress, callingAddress, aBit);
        return outputStream.toByteArray();
    }

    @Test
    public void testMarshalAddressBlock0() throws Exception {

        final X121Address calledAddress = X121Address.create("123456");
        final X121Address callingAddress = X121Address.create("123456");

        {
            final boolean aBit = false;
            final byte[] expected = new byte[]{0x66, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }

        {
            final boolean aBit = true;
            final byte[] expected = new byte[]{0x66, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }
    }

    @Test
    public void testMarshalAddressBlock1() throws Exception {

        final X121Address calledAddress = X121Address.create("123456");
        final X121Address callingAddress = X121Address.create("1234567");

        {
            final boolean aBit = false;
            final byte[] expected = new byte[]{0x76, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x70};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }

        {
            final boolean aBit = true;
            final byte[] expected = new byte[]{0x67, 0x12, 0x34, 0x56, 0x12, 0x34, 0x56, 0x70};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }
    }

    @Test
    public void testMarshalAddressBlock2() throws Exception {

        final X121Address calledAddress = X121Address.create("1234567");
        final X121Address callingAddress = X121Address.create("123456");

        {
            final boolean aBit = false;
            final byte[] expected = new byte[]{0x67, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45, 0x60};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }

        {
            final boolean aBit = true;
            final byte[] expected = new byte[]{0x76, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45, 0x60};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }
    }

    @Test
    public void testMarshalAddressBlock3() throws Exception {

        final X121Address calledAddress = X121Address.create("1234567");
        final X121Address callingAddress = X121Address.create("12345");

        {
            final boolean aBit = false;
            final byte[] expected = new byte[]{0x57, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }

        {
            final boolean aBit = true;
            final byte[] expected = new byte[]{0x75, 0x12, 0x34, 0x56, 0x71, 0x23, 0x45};
            final byte[] actual = marshalAddressBlock(calledAddress, callingAddress, aBit);

            assertEquals(expected.length, actual.length);
            assertArrayEquals(expected, actual);
        }
    }

    @Test
    public void testCallerId() throws Exception {

        final byte[] addressBlock = new byte[]{(byte) 0x87, 0x16, 0x34, 0x56, 0x71, 0x10, 0x09, 0x00, 0x00};

        // a-bit set
        {
            final boolean aBit = true;
            final X121AddressPair addressPair = X121AddressMarshaller.unmarshal(new ByteArrayInputStream(addressBlock), aBit);
            assertEquals(X121Address.create("16345671"), addressPair.calledAddress);
            assertEquals(X121Address.create("1009000"), addressPair.callingAddress);
        }
        // a-bit unset
        {
            final boolean aBit = false;
            final X121AddressPair addressPair = X121AddressMarshaller.unmarshal(new ByteArrayInputStream(addressBlock), aBit);
            assertEquals(X121Address.create("1634567"), addressPair.calledAddress);
            assertEquals(X121Address.create("11009000"), addressPair.callingAddress);
        }
    }
}
