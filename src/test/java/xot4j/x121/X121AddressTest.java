//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x121;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class X121AddressTest {

    @Test
    public void test6Digits0() throws Exception {

        final String source = "666666";
        final byte[] expected = new byte[]{0x66, 0x66, 0x66};

        final X121Address address = X121Address.create(source);
        assertEquals(3, address.getByteLength());
        assertArrayEquals(expected, address.getBytes());
    }

    @Test
    public void test7Digits() throws Exception {

        final String source = "1634567";
        // final byte[] expected = new byte[]{0x01, 0x63, 0x45, 0x67};
        final byte[] expected = new byte[]{0x16, 0x34, 0x56, 0x70};

        final X121Address address = X121Address.create(source);
        assertEquals(4, address.getByteLength());
        assertArrayEquals(expected, address.getBytes());
    }

    @Test
    public void test8Digits() throws Exception {

        final String source = "01634567";
        final byte[] expected = new byte[]{0x01, 0x63, 0x45, 0x67};

        final X121Address address = X121Address.create(source);
        assertEquals(4, address.getByteLength());
        assertArrayEquals(expected, address.getBytes());
    }
}
