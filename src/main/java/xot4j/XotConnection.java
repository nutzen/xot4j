//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j;

import java.io.InputStream;
import java.io.OutputStream;
import xot4j.x121.X121Address;

public interface XotConnection {

    public X121Address getRemoteAddress();

    public void disconnect();

    public InputStream getInputStream();

    public OutputStream getOutputStream();
}
