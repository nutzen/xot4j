//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public final class SocketFactory {

    private final String hostname;
    private final int port;
    private final int connectTimeout;

    public SocketFactory(final String hostname, final int port) {

        this(hostname, port, 3000);
    }

    public SocketFactory(final String hostname, final int port, final int connectTimeout) {

        this.hostname = hostname;
        this.port = port;
        this.connectTimeout = connectTimeout;
    }

    private void sleep() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // ignore
        }
    }

    public Socket create() throws IOException {

        return connect(3);
    }

    @Override
    public String toString() {

        return "[" + hostname + ":" + port + "]";
    }

    private Socket connect(final int retries) throws IOException {

        try {
            final Socket socket = new Socket();
            if (connectTimeout > 0) {
                socket.connect(new InetSocketAddress(hostname, port), connectTimeout);
            } else {
                socket.connect(new InetSocketAddress(hostname, port));
            }
            return socket;

        } catch (IOException e) {
            if (retries <= 0) {
                throw e;
            }
        }

        sleep();
        return connect(retries - 1);
    }
}
