//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.transport;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import xot4j.stream.TransactionalInputStream;
import xot4j.x25.X25Packet;

public final class XotSocketTransportWrapper implements TransportWrapper {

    private static final int SO_TIMEOUT = 500;
    private final TransportWrapper wrapper;
    private final TransactionalInputStream inputStream;
    private final Socket socket;

    private XotSocketTransportWrapper(TransportWrapper wrapper,
            TransactionalInputStream inputStream,
            Socket socket) {

        this.wrapper = wrapper;
        this.inputStream = inputStream;
        this.socket = socket;
    }

    public static XotSocketTransportWrapper create(Socket socket) throws IOException {

        socket.setSoTimeout(SO_TIMEOUT) /* throws SocketTimeoutException on poll() */;

        final TransactionalInputStream inputStream = TransactionalInputStream.create(socket.getInputStream());
        final OutputStream outputStream = socket.getOutputStream();
        final TransportWrapper wrapper = XotStreamTransportWrapper.create(inputStream, outputStream);

        return new XotSocketTransportWrapper(wrapper,
                inputStream,
                socket);
    }

    @Override
    public X25Packet poll() throws IOException {

        X25Packet packet;
        socket.setSoTimeout(SO_TIMEOUT) /* throws SocketTimeoutException */;

        try {
            packet = wrapper.poll();
            inputStream.commit();

        } catch (SocketTimeoutException e) {
            packet = null;
            inputStream.rollback();
        }

        return packet;
    }

    @Override
    public void submit(X25Packet packet) throws IOException {

        wrapper.submit(packet);
    }
}
