//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.transport;

import java.io.IOException;
import xot4j.x25.X25Packet;

public interface TransportWrapper {

    X25Packet poll() throws IOException;

    void submit(X25Packet packet) throws IOException;
}
