//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import xot4j.XotPacket;
import xot4j.XotPacketMarshaller;
import xot4j.x25.X25Packet;

public final class XotStreamTransportWrapper implements TransportWrapper {

    private final InputStream inputStream;
    private final OutputStream outputStream;

    XotStreamTransportWrapper(InputStream inputStream, OutputStream outputStream) {

        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public static XotStreamTransportWrapper create(InputStream inputStream, OutputStream outputStream) {

        return new XotStreamTransportWrapper(inputStream, outputStream);
    }

    @Override
    public X25Packet poll() throws IOException {

        final XotPacket packet = XotPacketMarshaller.unmarshal(inputStream);
        return packet.getPacket();
    }

    @Override
    public void submit(X25Packet packet) throws IOException {

        final XotPacket xotPacket = XotPacket.create(packet);
        XotPacketMarshaller.marshal(outputStream, xotPacket);
    }
}
