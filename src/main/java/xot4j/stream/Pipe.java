//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class Pipe {

    private final Lock lock = new ReentrantLock();
    private byte[] buffer;
    private final InputStream inputStream = new PipeInputStream();
    private final OutputStream outputStream = new PipeOutputStream();

    private Pipe(byte[] buffer) {

        this.buffer = buffer;
    }

    public static Pipe create() {

        return create(new byte[0]);
    }

    public static Pipe create(byte[] buffer) {

        return new Pipe(buffer);
    }

    public Pipe copy() {

        lock.lock();
        try {
            final byte[] copy = Arrays.copyOf(buffer, buffer.length);
            return create(copy);

        } finally {
            lock.unlock();
        }
    }

    public InputStream getInputStream() {

        return inputStream;
    }

    public OutputStream getOutputStream() {

        return outputStream;
    }

    private class PipeInputStream extends InputStream {

        @Override
        public int available() throws IOException {

            lock.lock();
            try {
                return buffer.length;

            } finally {
                lock.unlock();
            }
        }

        @Override
        public int read() throws IOException {

            lock.lock();
            try {
                final ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);
                final int read = inputStream.read();

                final int available = inputStream.available();
                final byte[] intermediate = new byte[available];

                if (0 < available) {
                    inputStream.read(intermediate);
                }
                buffer = intermediate;

                return read;

            } finally {
                lock.unlock();
            }
        }

        @Override
        public int read(byte[] b, int off, int len) throws IOException {

            lock.lock();
            try {
                final ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);
                final int read = inputStream.read(b, off, len);

                final int available = inputStream.available();
                final byte[] intermediate = new byte[available];
                inputStream.read(intermediate);

                buffer = intermediate;

                return read;

            } finally {
                lock.unlock();
            }
        }
    }

    private class PipeOutputStream extends OutputStream {

        @Override
        public void write(int b) throws IOException {

            lock.lock();
            try {
                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(buffer.length + 1);
                outputStream.write(buffer);
                outputStream.write(b);

                buffer = outputStream.toByteArray();

            } finally {
                lock.unlock();
            }
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {

            lock.lock();
            try {
                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(buffer.length + len);
                outputStream.write(buffer);
                outputStream.write(b, off, len);

                buffer = outputStream.toByteArray();

            } finally {
                lock.unlock();
            }
        }
    }
}
