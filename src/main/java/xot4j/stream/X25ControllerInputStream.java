//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.IOException;
import java.io.InputStream;
import xot4j.x25.X25Controller;

public final class X25ControllerInputStream extends InputStream {

    private final X25Controller controller;
    private final InputStream inputStream;

    private X25ControllerInputStream(X25Controller controller, InputStream inputStream) {

        this.controller = controller;
        this.inputStream = inputStream;
    }

    public static X25ControllerInputStream create(X25Controller controller) {

        final Pipe pipe = controller.getRemoteToLocal();
        final InputStream inputStream = BlockingPipeInputStream.create(pipe);

        return new X25ControllerInputStream(controller, inputStream);
    }

    private void check() throws IOException {

        if (0 >= available()) {
            if (!controller.isConnected()) {
                throw new IOException("Session closed");
            }
        }
    }

    @Override
    public int available() throws IOException {

        return inputStream.available();
    }

    @Override
    public int read() throws IOException {

        check();
        return inputStream.read();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {

        check();
        return inputStream.read(b, off, len);
    }
}
