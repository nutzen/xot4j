//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class TransactionalInputStream extends InputStream {

    private final Lock lock = new ReentrantLock();
    private final InputStream source;
    private volatile Pipe reference = Pipe.create();
    private volatile Pipe sandbox = reference.copy();

    private TransactionalInputStream(InputStream source) {

        this.source = source;
    }

    public static TransactionalInputStream create(InputStream source) {

        return new TransactionalInputStream(source);
    }

    public void commit() {

        lock.lock();
        try {
            reference = sandbox.copy();

        } finally {
            lock.unlock();
        }
    }

    public void rollback() {

        lock.lock();
        try {
            sandbox = reference.copy();

        } finally {
            lock.unlock();
        }
    }

    @Override
    public int available() throws IOException {

        lock.lock();
        try {
            return sandbox.getInputStream().available() + source.available();

        } finally {
            lock.unlock();
        }
    }

    @Override
    public int read() throws IOException {

        lock.lock();
        try {
            readAheadIfNecessary(1);
            return sandbox.getInputStream().read();

        } finally {
            lock.unlock();
        }
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {

        lock.lock();
        try {
            readAheadIfNecessary(len);
            return sandbox.getInputStream().read(b, off, len);

        } finally {
            lock.unlock();
        }
    }

    private int readAheadIfNecessary(int required) throws IOException {

        final int size = required - sandbox.getInputStream().available();
        int read = 0;
        if (0 < size) {
            read = readAhead(size);
        }
        return read;
    }

    private int readAhead(int size) throws IOException {

        final byte[] buffer = new byte[size];
        final int read = source.read(buffer);
        if (0 < read) {
            reference.getOutputStream().write(buffer, 0, read);
            sandbox.getOutputStream().write(buffer, 0, read);
        }
        return read;
    }
}
