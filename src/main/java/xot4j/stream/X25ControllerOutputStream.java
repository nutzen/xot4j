//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.IOException;
import java.io.OutputStream;
import xot4j.x25.X25Controller;

public final class X25ControllerOutputStream extends OutputStream {

    private final X25Controller controller;
    private final OutputStream outputStream;

    private X25ControllerOutputStream(X25Controller controller, OutputStream outputStream) {

        this.controller = controller;
        this.outputStream = outputStream;
    }

    public static X25ControllerOutputStream create(X25Controller controller) {

        final Pipe pipe = controller.getLocalToRemote();
        final OutputStream outputStream = BlockingPipeOutputStream.create(pipe);

        return new X25ControllerOutputStream(controller, outputStream);
    }

    private void check() throws IOException {

        if (!controller.isConnected()) {
            throw new IOException("Session closed");
        }
    }

    @Override
    public void write(int b) throws IOException {

        check();
        outputStream.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {

        check();
        outputStream.write(b, off, len);
    }
}
