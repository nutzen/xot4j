//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;

public final class BlockingPipeInputStream extends InputStream {

    private final int READ_TIMEOUT = 1000 * 30;
    private final Pipe pipe;

    private BlockingPipeInputStream(Pipe pipe) {
        this.pipe = pipe;
    }

    public static BlockingPipeInputStream create(Pipe pipe) {

        return new BlockingPipeInputStream(pipe);
    }

    private void waitForData() throws IOException {

        try {
            final int sleepInterval = 100;
            final int maxCounter = READ_TIMEOUT / sleepInterval;
            for (int counter = 0; (maxCounter > counter) && (0 >= available()); ++counter) {
                sleep(sleepInterval);
            }

            if (0 >= available()) {
                throw new SocketTimeoutException();
            }

        } catch (InterruptedException e) {
        }
    }

    private static void sleep(long interval) throws InterruptedException {

        Thread.sleep(interval);
    }

    @Override
    public int available() throws IOException {

        return pipe.getInputStream().available();
    }

    @Override
    public int read() throws IOException {

        waitForData();
        return pipe.getInputStream().read();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {

        waitForData();
        return pipe.getInputStream().read(b, off, len);
    }
}
