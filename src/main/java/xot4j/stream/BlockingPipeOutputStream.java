//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.stream;

import java.io.IOException;
import java.io.OutputStream;

public final class BlockingPipeOutputStream extends OutputStream {

    private final int WRITE_TIMEOUT = 1000 * 30;
    private final Pipe pipe;

    private BlockingPipeOutputStream(Pipe pipe) {
        this.pipe = pipe;
    }

    public static BlockingPipeOutputStream create(Pipe pipe) {

        return new BlockingPipeOutputStream(pipe);
    }

    private void waitForData() throws IOException {

        try {
            final int sleepInterval = 100;
            final int maxCounter = WRITE_TIMEOUT / sleepInterval;
            for (int counter = 0; (maxCounter > counter) && (0 < available()); ++counter) {
                sleep(sleepInterval);
            }

        } catch (InterruptedException e) {
        }
    }

    private static void sleep(long interval) throws InterruptedException {

        Thread.sleep(interval);
    }

    private int available() throws IOException {

        return pipe.getInputStream().available();
    }

    @Override
    public void write(int b) throws IOException {

        pipe.getOutputStream().write(b);
        waitForData();
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {

        pipe.getOutputStream().write(b, off, len);
        waitForData();
    }
}
