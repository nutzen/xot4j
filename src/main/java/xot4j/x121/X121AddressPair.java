//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x121;

public final class X121AddressPair {

    public final X121Address calledAddress;
    public final X121Address callingAddress;

    X121AddressPair(X121Address calledAddress, X121Address callingAddress) {

        this.calledAddress = calledAddress;
        this.callingAddress = callingAddress;
    }

    public static X121AddressPair create(X121Address calledAddress, X121Address callingAddress) {

        return new X121AddressPair(calledAddress, callingAddress);
    }
}
