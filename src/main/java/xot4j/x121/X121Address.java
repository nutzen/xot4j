//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x121;

public final class X121Address {

    private final String address;
    private final byte[] bytes;

    X121Address(String address, byte[] bytes) {

        this.address = address;
        this.bytes = bytes;
    }

    @Override
    public String toString() {

        return address;
    }

    @Override
    public boolean equals(Object object) {

        final boolean equal;
        if (null == object) {
            equal = false;

        } else if (!(object instanceof X121Address)) {
            equal = false;

        } else {
            final X121Address other = (X121Address) object;
            equal = address.equals(other.address);
        }

        return equal;
    }

    @Override
    public int hashCode() {

        int hash = 3;
        hash = 97 * hash + (this.address != null ? this.address.hashCode() : 0);
        return hash;
    }

    public int length() {

        return address.length();
    }

    public byte[] getBytes() {

        return bytes;
    }

    public int getByteLength() {

        return bytes.length;
    }

    public static X121Address create(String addressString) {

        final byte[] bytes = extractAddress(addressString);
        return new X121Address(addressString, bytes);
    }

    static byte[] extractAddress(String addressString) {

        final byte[] bytes = addressString.getBytes();
        final int length = (int) Math.ceil((double) bytes.length / 2);
        final byte[] address = new byte[length];

        for (int index = 0; index < bytes.length; index += 2) {

            final int v0 = bytes[index];
            final int v1;

            if (index < (bytes.length - 1)) {
                v1 = bytes[index + 1];
            } else {
                v1 = 0;
            }

            address[index / 2] = (byte) (((v0 << 4) & 0x00f0) | (v1 & 0x000f));
        }

        return address;
    }
}
