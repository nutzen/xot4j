//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x121;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class X121AddressMarshaller {

    private X121AddressMarshaller() {
    }

    public static void marshal(final OutputStream outputStream, final X121Address calledAddress, final X121Address callingAddress, boolean aBit) throws IOException {

        if (aBit) {
            outputStream.write((((calledAddress.length() << 4) & 0x00f0) | (callingAddress.length() & 0x000f)));
        } else {
            outputStream.write((((callingAddress.length() << 4) & 0x00f0) | (calledAddress.length() & 0x000f)));
        }

        final byte[] called = calledAddress.getBytes();
        final byte[] calling = callingAddress.getBytes();

        if (0 == (calledAddress.length() % 2)) {
            outputStream.write(called, 0, called.length);
            outputStream.write(calling, 0, calling.length);

        } else {
            outputStream.write(called, 0, called.length - 1);
            byte temporary = (byte) (0x00f0 & called[called.length - 1]);

            for (int counter = 0; callingAddress.length() > counter; counter += 2) {

                final int index = counter / 2;

                temporary = (byte) ((0x00f0 & temporary) | (0x000f & (calling[index] >> 4)));
                outputStream.write(temporary);

                temporary = (byte) (0x00f0 & (calling[index] << 4));
                if (callingAddress.length() == (2 + counter)) {
                    outputStream.write(temporary);
                }
            }
        }
    }

    public static X121AddressPair unmarshal(final InputStream inputStream, boolean aBit) throws IOException {

        final int lengths = (int) (0x00ff & inputStream.read());
        final int calledLength;
        final int callingLength;

        if (aBit) {
            calledLength = (int) (0x00f0 & lengths) >> 4;
            callingLength = (0x000f & lengths);
        } else {
            callingLength = (0x00f0 & lengths) >> 4;
            calledLength = (int) (0x000f & lengths);
        }

        final int totalLength = calledLength + callingLength;

        final StringBuilder builder = new StringBuilder();
        while (builder.length() < totalLength) {
            final int value = (int) (0x00ff & inputStream.read());

            final int value0 = (int) (0x00f0 & value) >> 4;
            builder.append(Integer.toHexString(value0));

            final int value1 = (int) (0x000f & value);
            builder.append(Integer.toHexString(value1));
        }

        final String addressBlock = builder.toString();
        final String calledAddressText = addressBlock.substring(0, calledLength);
        final String callingAddressText = addressBlock.substring(calledLength, calledLength + callingLength);

        final X121Address calledAddress = X121Address.create(calledAddressText);
        final X121Address callingAddress = X121Address.create(callingAddressText);

        return X121AddressPair.create(calledAddress, callingAddress);
    }
}
