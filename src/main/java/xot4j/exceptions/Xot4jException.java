//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.exceptions;

public class Xot4jException extends Exception {

    public Xot4jException() {

        super();
    }

    public Xot4jException(String message) {

        super(message);
    }

    public Xot4jException(String message, Throwable cause) {

        super(message, cause);
    }

    public Xot4jException(Throwable cause) {

        super(cause);
    }
}
