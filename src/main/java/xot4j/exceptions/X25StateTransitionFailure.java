package xot4j.exceptions;

import java.text.MessageFormat;
import xot4j.x25.states.StateEnumeration;
import xot4j.x25.transitions.TransitionEnumeration;

public class X25StateTransitionFailure extends Xot4jException {

    private X25StateTransitionFailure(String message) {

        super(message);
    }

    public static X25StateTransitionFailure create(StateEnumeration state, TransitionEnumeration transition) {

        final String pattern = "Unhandled transition {0} in state {1}";
        final String message = MessageFormat.format(pattern, transition, state);

        return new X25StateTransitionFailure(message);
    }
}
