//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.exceptions;

public class CallSetupFailure extends Xot4jException {

    public CallSetupFailure() {

        super();
    }

    public CallSetupFailure(String message) {

        super(message);
    }

    public CallSetupFailure(String message, Throwable cause) {

        super(message, cause);
    }

    public CallSetupFailure(Throwable cause) {

        super(cause);
    }
}
