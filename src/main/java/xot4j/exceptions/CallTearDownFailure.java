//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.exceptions;

public class CallTearDownFailure extends Xot4jException {

    public CallTearDownFailure() {

        super();
    }

    public CallTearDownFailure(String message) {

        super(message);
    }

    public CallTearDownFailure(String message, Throwable cause) {

        super(message, cause);
    }

    public CallTearDownFailure(Throwable cause) {

        super(cause);
    }
}
