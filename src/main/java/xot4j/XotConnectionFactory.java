//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j;

import java.net.Socket;
import java.util.Collection;
import xot4j.transport.TransportWrapper;
import xot4j.transport.XotSocketTransportWrapper;
import xot4j.x121.X121Address;
import xot4j.x25.X25Controller;
import xot4j.x25.context.BasicSessionContext;
import xot4j.x25.context.SessionContext;
import xot4j.x25.context.SystemContext;
import xot4j.x25.facilities.Facility;
import xot4j.x25.lci.LogicalChannelIdentifier;

public final class XotConnectionFactory {

    private XotConnectionFactory() {
    }

    public static XotConnection accept(Socket socket,
            SystemContext systemContext) throws Exception {

        final TransportWrapper wrapper = XotSocketTransportWrapper.create(socket);
        final SessionContext sessionContext = createSessionContext(systemContext);

        final X25Controller controller = X25Controller.create(sessionContext, wrapper);
        controller.accept();

        return BasicXotConnection.create(sessionContext, controller);
    }

    public static XotConnection call(Socket socket,
            SystemContext systemContext,
            X121Address remoteAddress) throws Exception {

        return call(socket, systemContext, remoteAddress, new byte[]{(byte) 0xc4});
    }

    public static XotConnection call(Socket socket,
            SystemContext systemContext,
            X121Address remoteAddress,
            byte[] userData) throws Exception {

        final TransportWrapper wrapper = XotSocketTransportWrapper.create(socket);
        final SessionContext sessionContext = createSessionContext(systemContext, remoteAddress);

        final X25Controller controller = X25Controller.create(sessionContext, wrapper);
        controller.connect(userData);

        return BasicXotConnection.create(sessionContext, controller);
    }

    static SessionContext createSessionContext(SystemContext systemContext) {

        return createSessionContext(systemContext, null);
    }

    static SessionContext createSessionContext(SystemContext systemContext,
            X121Address remoteAddress) {

        final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create(systemContext.nextLci());
        final X121Address calledAddress = remoteAddress;
        final X121Address callingAddress = systemContext.getLocalAddress();
        final Collection<Facility> facilities = systemContext.getFacilities();

        return BasicSessionContext.create(callingAddress, facilities, calledAddress, logicalChannelIdentifier);
    }
}
