//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.example;

import java.net.Socket;
import java.util.Properties;
import xot4j.XotConnection;
import xot4j.XotConnectionFactory;
import xot4j.socket.SocketFactory;
import xot4j.x121.X121Address;
import xot4j.x25.context.SystemContext;

public class SimpleCaller {

    static {
        final String name = "log4j.configuration";
        final String value = "console-log4j.xml";
        final Properties properties = System.getProperties();

        if (properties.getProperty(name) == null) {
            properties.setProperty(name, value);
        }
    }

    public static void main(final String... arguments) {

        try {

            try (final Socket socket = new SocketFactory("10.10.10.2", 1998).create()) {
                final SystemContext systemContext = SystemContext.create(X121Address.create("1234567"));
                final XotConnection connection = XotConnectionFactory.call(socket, systemContext, X121Address.create("654321"));
                System.out.println("Call established");

                final byte[] buffer = new byte[1024];
                final int read = connection.getInputStream().read(buffer);
                System.out.println("read " + read + " bytes: " + new String(buffer));

                connection.disconnect();
                System.out.println("Call ended");
            }

            System.exit(0);

        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
            System.exit(-1);
        }
    }
}
