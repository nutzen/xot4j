//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.example;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import xot4j.XotConnection;
import xot4j.XotConnectionFactory;
import xot4j.x121.X121Address;
import xot4j.x25.context.SystemContext;

public class SimpleReceiver {

    static {
        final String name = "log4j.configuration";
        final String value = "console-log4j.xml";
        final Properties properties = System.getProperties();

        if (properties.getProperty(name) == null) {
            properties.setProperty(name, value);
        }
    }

    public static void main(final String... arguments) {

        boolean running = true;
        try {
            final ServerSocket serverSocket = new ServerSocket(1998);
            while (running) {
                try (final Socket socket = serverSocket.accept()) {
                    final SystemContext systemContext = SystemContext.create(X121Address.create("1234567"));
                    final XotConnection connection = XotConnectionFactory.accept(socket, systemContext);
                    System.out.println("I received a call from " + connection.getRemoteAddress());

                    connection.getOutputStream().write("sorry mate the lights are on, but nobody is home.".getBytes());

                    connection.disconnect();
                }
            }

            System.exit(0);

        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace(System.err);
            System.exit(-1);
        }
    }
}
