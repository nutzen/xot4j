//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import xot4j.x25.gfi.GeneralFormatIdentifier;
import xot4j.x25.gfi.GeneralFormatIdentifierMarshaller;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.pti.PacketTypeIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierMarshaller;

public final class X25HeaderMarshaller {

    private X25HeaderMarshaller() {
    }

    public static void marshal(final OutputStream outputStream, final X25Header header) throws IOException {

        final byte[] target = new byte[3];
        // [0] = GFI | LCGN
        // [1] = LCN
        // [2] = PTI
        final byte gfi = (byte) (0x00f0 & (GeneralFormatIdentifierMarshaller.marshal(header.getGeneralFormatIdentifier()) << 4));
        final byte lcgn = (byte) (header.getLogicalChannelIdentifier().getLogicalChannelGroupNumber() & 0x000f);
        final byte pti = PacketTypeIdentifierMarshaller.marshalModule8(header.getPacketTypeIdentifier());

        target[0] = (byte) (0x00ff & (gfi | lcgn));
        target[1] = (byte) (0x00ff & header.getLogicalChannelIdentifier().getLogicalChannelNumber());
        target[2] = (byte) (0x00ff & pti);

        outputStream.write(target);
    }

    public static X25Header unmarshal(final InputStream inputStream) throws IOException {

        // [0] = GFI | LCGN
        // [1] = LCN
        // [2] = PTI
        final int value0 = (0x00ff & inputStream.read());
        final byte gfi = (byte) (0x000f & value0);
        final byte lcgn = (byte) (0x000f & value0);
        final byte lcn = (byte) (0x00ff & inputStream.read());
        final byte pti = (byte) (0x00ff & inputStream.read());

        final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifierMarshaller.unmarshal(gfi);
        final LogicalChannelIdentifier logicalChannelIdentifier = LogicalChannelIdentifier.create(lcgn, lcn);
        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierMarshaller.unmarshalModule8(pti);

        return X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }
}
