//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

/**
 *
 * MODULO 8 :
 *
 *        .====================================================.
 *  O  1  ||  Q    D  ||   X   X   ||        L C G N          || R
 *  C     |====================================================| E
 *  T  1  ||                    L  C  N                       || C
 *  E     |====================================================| E
 *  T  1  ||   Pr3   Pr2   Pr1   0     Pt4   Pt3   Pt2   Pt1  || I
 *  S     `====================================================' V
 *
 * MODULO 128 :
 *
 *        .====================================================.
 *  O  1  ||  Q    D  ||   X   X   ||        L C G N          || R
 *  C     |====================================================| E
 *  T  1  ||                    L  C  N                       || C
 *  E     |====================================================| E
 *  T  1  ||   0     0     0     0     Pt4   Pt3   Pt2   Pt1  || I
 *  S     |====================================================| V
 *     1  ||   Pr7   Pr6   Pr5   Pr4   Pr3   Pr2   Pr1   M    || E
 *        `===================================================='
 *
 * P(R)     Packet receive sequence number which appears in data
 *          and flow control packets or the called DTE address which
 *          may appear in call setup, clearing and registration
 *          packets.
 *
 * P(T)     Packet type.
 *
 * M        More data bit which appears only in data packets. The field
 *          is set to 1 to indicate that the packet is part of a
 *          sequence of packets that should be treated as a logical
 *          whole.
 */
public interface ReceiverPacketTypeIdentifier extends PacketTypeIdentifier {

    int getPacketReceiveSequenceNumber();
}
