//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

final class PacketTypeIdentifierEnumerationUtility {

    // DESCRIPTION
    public static final String CALL_REQUEST_INCOMING_CALL_DESCRIPTION = "Call_Request/Incoming_Call";
    public static final String CALL_ACCEPTED_CALL_CONNECTED_DESCRIPTION = "Call_Accepted/Call_Connected";
    public static final String CLEAR_REQUEST_CLEAR_INDICATION_DESCRIPTION = "Clear_Request/Clear_Indication";
    public static final String CLEAR_CONFIRMATION_DESCRIPTION = "Clear_Confirmation";
    public static final String DATA_DESCRIPTION = "Data";
    public static final String INTERRUPT_DESCRIPTION = "Interrupt";
    public static final String INTERRUPT_CONFIRMATION_DESCRIPTION = "Interrupt_Confirmation";
    public static final String RECEIVER_READY_DESCRIPTION = "Receiver_Ready";
    public static final String RECEIVER_NOT_READY_DESCRIPTION = "Receiver_Not_Ready";
    public static final String REJECT_DESCRIPTION = "Reject";
    public static final String RESET_REQUEST_RESET_INDICATION_DESCRIPTION = "Reset_Request/Reset_Indication";
    public static final String RESET_CONFIRMATION_DESCRIPTION = "Reset_Confirmation";
    public static final String RESTART_REQUEST_RESTART_INDICATION_DESCRIPTION = "Restart_Request/Restart_Indication";
    public static final String RESTART_CONFIRMATION_DESCRIPTION = "Restart_Confirmation";
    public static final String DIAGNOSTIC_DESCRIPTION = "Diagnostic";
    // MASK
    public static final int CALL_REQUEST_INCOMING_CALL_MASK = 0x0b;
    public static final int CALL_ACCEPTED_CALL_CONNECTED_MASK = 0x0f;
    public static final int CLEAR_REQUEST_CLEAR_INDICATION_MASK = 0x13;
    public static final int CLEAR_CONFIRMATION_MASK = 0x17;
    public static final int DATA_MASK = 0x00;
    public static final int INTERRUPT_MASK = 0x23;
    public static final int INTERRUPT_CONFIRMATION_MASK = 0x27;
    public static final int RECEIVER_READY_MASK = 0x01;
    public static final int RECEIVER_NOT_READY_MASK = 0x05;
    public static final int REJECT_MASK = 0x09;
    public static final int RESET_REQUEST_RESET_INDICATION_MASK = 0x1b;
    public static final int RESET_CONFIRMATION_MASK = 0x1f;
    public static final int RESTART_REQUEST_RESTART_INDICATION_MASK = 0xfb;
    public static final int RESTART_CONFIRMATION_MASK = 0xff;
    public static final int DIAGNOSTIC_MASK = 0xf1;

    static boolean compareWithMask(final int mask, final int value) {

        switch (mask) {
            case RECEIVER_READY_MASK:
            case RECEIVER_NOT_READY_MASK:
            case REJECT_MASK:
                return (0x0f & value) == mask;

            case DATA_MASK:
                return (0x01 & value) == mask;

            default:
                return mask == value;
        }
    }

    private PacketTypeIdentifierEnumerationUtility() {
    }
}
