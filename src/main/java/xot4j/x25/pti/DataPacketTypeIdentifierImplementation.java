//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

final class DataPacketTypeIdentifierImplementation extends BasicPacketTypeIdentifier implements DataPacketTypeIdentifier {

    private final boolean moreData;
    private final int packetReceiveSequenceNumber;
    private final int packetSendSequenceNumber;

    DataPacketTypeIdentifierImplementation(
            PacketTypeIdentifierEnumeration enumeration,
            boolean moreData,
            int packetReceiveSequenceNumber,
            int packetSendSequenceNumber) {

        super(enumeration);

        this.moreData = moreData;
        this.packetReceiveSequenceNumber = packetReceiveSequenceNumber;
        this.packetSendSequenceNumber = packetSendSequenceNumber;
    }

    @Override
    public boolean isDataPacketTypeIdentifier() {

        return true;
    }

    @Override
    public boolean isMoreData() {

        return moreData;
    }

    @Override
    public int getPacketReceiveSequenceNumber() {

        return packetReceiveSequenceNumber;
    }

    @Override
    public int getPacketSendSequenceNumber() {

        return packetSendSequenceNumber;
    }
}
