//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

/**
 *
 * .========================================================================.
 * || Packet type || Binary code || Hex code ||
 * || DTE to DCE ( / DCE to DTE ) || 8 7 6 5 4 3 2 1 || 0xXX ||
 * |========================================================================|
 * || Call set-up and clearing - - - - - - || - - - - - - - - || - -- ||
 * || Call Request / Incoming Call || 0 0 0 0 1 0 1 1 || 0x0b ||
 * || Call Accepted / Call Connected || 0 0 0 0 1 1 1 1 || 0x0f ||
 * || Clear Request / Clear Indication || 0 0 0 1 0 0 1 1 || 0x13 ||
 * || Clear Confirmation || 0 0 0 1 0 1 1 1 || 0x17 ||
 * || Data and interrupt - - - - - - - - - || - - - - - - - - || - -- ||
 * || Data || X X X X X X X 0 || 0xX0 ||
 * || Interrupt || 0 0 1 0 0 0 1 1 || 0x23 ||
 * || Interrupt Confirmation || 0 0 1 0 0 1 1 1 || 0x27 ||
 * || Flow control and reset - - - - - - - || - - - - - - - - || - -- ||
 * || Receiver Ready || X X X 0 0 0 0 1 || 0xX1 ||
 * || Receiver Not Ready || X X X 0 0 1 0 1 || 0xX5 ||
 * || Reject || X X X 0 1 0 0 1 || 0xX9 ||
 * || Reset Request / Reset Indication || 0 0 0 1 1 0 1 1 || 0x1b ||
 * || Reset Confirmation || 0 0 0 1 1 1 1 1 || 0x1f ||
 * || Restart - - - - - - - - - - - - - - || - - - - - - - - || - -- ||
 * || Restart Request / Restart Indication || 1 1 1 1 1 0 1 1 || 0xfb ||
 * || Restart Confirmation || 1 1 1 1 1 1 1 1 || 0xff ||
 * || Diagnostic - - - - - - - - - - - - - || - - - - - - - - || - -- ||
 * || Diagnostic || 1 1 1 1 0 0 0 1 || 0xf1 ||
 * `========================================================================'
 *
 */
import static xot4j.x25.pti.PacketTypeIdentifierEnumerationUtility.*;

public enum PacketTypeIdentifierEnumeration {

    CallRequest_IncomingCall,
    CallAccepted_CallConnected,
    ClearRequest_ClearIndication,
    ClearConfirmation,
    Data,
    Interrupt,
    InterruptConfirmation,
    ReceiverReady,
    ReceiverNotReady,
    Reject,
    ResetRequest_ResetIndication,
    ResetConfirmation,
    RestartRequest_RestartIndication,
    RestartConfirmation,
    Diagnostic;

    public String toDescription() {

        String destination;

        switch (this) {
            case CallRequest_IncomingCall:
                destination = CALL_REQUEST_INCOMING_CALL_DESCRIPTION;
                break;

            case CallAccepted_CallConnected:
                destination = CALL_ACCEPTED_CALL_CONNECTED_DESCRIPTION;
                break;

            case ClearRequest_ClearIndication:
                destination = CLEAR_REQUEST_CLEAR_INDICATION_DESCRIPTION;
                break;

            case ClearConfirmation:
                destination = CLEAR_CONFIRMATION_DESCRIPTION;
                break;

            case Data:
                destination = DATA_DESCRIPTION;
                break;

            case Interrupt:
                destination = INTERRUPT_DESCRIPTION;
                break;

            case InterruptConfirmation:
                destination = INTERRUPT_CONFIRMATION_DESCRIPTION;
                break;

            case ReceiverReady:
                destination = RECEIVER_READY_DESCRIPTION;
                break;

            case ReceiverNotReady:
                destination = RECEIVER_NOT_READY_DESCRIPTION;
                break;

            case Reject:
                destination = REJECT_DESCRIPTION;
                break;

            case ResetRequest_ResetIndication:
                destination = RESET_REQUEST_RESET_INDICATION_DESCRIPTION;
                break;

            case ResetConfirmation:
                destination = RESET_CONFIRMATION_DESCRIPTION;
                break;

            case RestartRequest_RestartIndication:
                destination = RESTART_REQUEST_RESTART_INDICATION_DESCRIPTION;
                break;

            case RestartConfirmation:
                destination = RESTART_CONFIRMATION_DESCRIPTION;
                break;

            case Diagnostic:
                destination = DIAGNOSTIC_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static PacketTypeIdentifierEnumeration fromDescription(final String value) {

        final PacketTypeIdentifierEnumeration destination;
        switch (value) {
            case CALL_REQUEST_INCOMING_CALL_DESCRIPTION:
                destination = CallRequest_IncomingCall;
                break;

            case CALL_ACCEPTED_CALL_CONNECTED_DESCRIPTION:
                destination = CallAccepted_CallConnected;
                break;

            case CLEAR_REQUEST_CLEAR_INDICATION_DESCRIPTION:
                destination = ClearRequest_ClearIndication;
                break;

            case CLEAR_CONFIRMATION_DESCRIPTION:
                destination = ClearConfirmation;
                break;

            case DATA_DESCRIPTION:
                destination = Data;
                break;

            case INTERRUPT_DESCRIPTION:
                destination = Interrupt;
                break;

            case INTERRUPT_CONFIRMATION_DESCRIPTION:
                destination = InterruptConfirmation;
                break;

            case RECEIVER_READY_DESCRIPTION:
                destination = ReceiverReady;
                break;

            case RECEIVER_NOT_READY_DESCRIPTION:
                destination = ReceiverNotReady;
                break;

            case REJECT_DESCRIPTION:
                destination = Reject;
                break;

            case RESET_REQUEST_RESET_INDICATION_DESCRIPTION:
                destination = ResetRequest_ResetIndication;
                break;

            case RESET_CONFIRMATION_DESCRIPTION:
                destination = ResetConfirmation;
                break;

            case RESTART_REQUEST_RESTART_INDICATION_DESCRIPTION:
                destination = RestartRequest_RestartIndication;
                break;

            case RESTART_CONFIRMATION_DESCRIPTION:
                destination = RestartConfirmation;
                break;

            case DIAGNOSTIC_DESCRIPTION:
                destination = Diagnostic;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }

    public int toInt() {

        int destination;

        switch (this) {
            case CallRequest_IncomingCall:
                destination = CALL_REQUEST_INCOMING_CALL_MASK;
                break;

            case CallAccepted_CallConnected:
                destination = CALL_ACCEPTED_CALL_CONNECTED_MASK;
                break;

            case ClearRequest_ClearIndication:
                destination = CLEAR_REQUEST_CLEAR_INDICATION_MASK;
                break;

            case ClearConfirmation:
                destination = CLEAR_CONFIRMATION_MASK;
                break;

            case Data:
                destination = DATA_MASK;
                break;

            case Interrupt:
                destination = INTERRUPT_MASK;
                break;

            case InterruptConfirmation:
                destination = INTERRUPT_CONFIRMATION_MASK;
                break;

            case ReceiverReady:
                destination = RECEIVER_READY_MASK;
                break;

            case ReceiverNotReady:
                destination = RECEIVER_NOT_READY_MASK;
                break;

            case Reject:
                destination = REJECT_MASK;
                break;

            case ResetRequest_ResetIndication:
                destination = RESET_REQUEST_RESET_INDICATION_MASK;
                break;

            case ResetConfirmation:
                destination = RESET_CONFIRMATION_MASK;
                break;

            case RestartRequest_RestartIndication:
                destination = RESTART_REQUEST_RESTART_INDICATION_MASK;
                break;

            case RestartConfirmation:
                destination = RESTART_CONFIRMATION_MASK;
                break;

            case Diagnostic:
                destination = DIAGNOSTIC_MASK;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static PacketTypeIdentifierEnumeration fromInt(final int value) {

        final PacketTypeIdentifierEnumeration destination;

        if (compareWithMask(DIAGNOSTIC_MASK, value)) {
            destination = Diagnostic;

        } else if (compareWithMask(CALL_REQUEST_INCOMING_CALL_MASK, value)) {
            destination = CallRequest_IncomingCall;

        } else if (compareWithMask(CALL_ACCEPTED_CALL_CONNECTED_MASK, value)) {
            destination = CallAccepted_CallConnected;

        } else if (compareWithMask(CLEAR_REQUEST_CLEAR_INDICATION_MASK, value)) {
            destination = ClearRequest_ClearIndication;

        } else if (compareWithMask(CLEAR_CONFIRMATION_MASK, value)) {
            destination = ClearConfirmation;

        } else if (compareWithMask(DATA_MASK, value)) {
            destination = Data;

        } else if (compareWithMask(INTERRUPT_MASK, value)) {
            destination = Interrupt;

        } else if (compareWithMask(INTERRUPT_CONFIRMATION_MASK, value)) {
            destination = InterruptConfirmation;

        } else if (compareWithMask(RECEIVER_READY_MASK, value)) {
            destination = ReceiverReady;

        } else if (compareWithMask(RECEIVER_NOT_READY_MASK, value)) {
            destination = ReceiverNotReady;

        } else if (compareWithMask(REJECT_MASK, value)) {
            destination = Reject;

        } else if (compareWithMask(RESET_REQUEST_RESET_INDICATION_MASK, value)) {
            destination = ResetRequest_ResetIndication;

        } else if (compareWithMask(RESET_CONFIRMATION_MASK, value)) {
            destination = ResetConfirmation;

        } else if (compareWithMask(RESTART_REQUEST_RESTART_INDICATION_MASK, value)) {
            destination = RestartRequest_RestartIndication;

        } else if (compareWithMask(RESTART_CONFIRMATION_MASK, value)) {
            destination = RestartConfirmation;

        } else {
            throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }
}
