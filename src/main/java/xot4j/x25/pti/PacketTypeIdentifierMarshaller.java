//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

/**
 * 
 * @todo Add module 128 support
 */
public final class PacketTypeIdentifierMarshaller {

    private PacketTypeIdentifierMarshaller() {
    }

    public static byte marshalModule8(final PacketTypeIdentifier source) {

        final byte target;

        if (source.isDataPacketTypeIdentifier()) {
            final DataPacketTypeIdentifier identifier = (DataPacketTypeIdentifier) source;
            final boolean moreData = identifier.isMoreData();
            final int packetReceiveSequenceNumber = identifier.getPacketReceiveSequenceNumber();
            final int packetSendSequenceNumber = identifier.getPacketSendSequenceNumber();

            int value = (packetReceiveSequenceNumber & 0x0007);
            value = ((value << 1) & 0x00fe) | (moreData ? 0x01 : 0x00);
            value = ((value << 3) & 0x00f8) | (packetSendSequenceNumber & 0x0007);
            value = ((value << 1) & 0x00fe);
            target = (byte) (value);

        } else if (source.isReceiverPacketTypeIdentifier()) {
            final ReceiverPacketTypeIdentifier identifier = (ReceiverPacketTypeIdentifier) source;
            final PacketTypeIdentifierEnumeration enumeration = identifier.getEnumeration();
            final int packetReceiveSequenceNumber = identifier.getPacketReceiveSequenceNumber();
            int value = (packetReceiveSequenceNumber & 0x0007);
            value = (0x00e0 & (value << 5));
            value = (value | (0x0f & enumeration.toInt()));
            target = (byte) (value & 0x00ff);

        } else {
            final PacketTypeIdentifierEnumeration enumeration = source.getEnumeration();
            final int value = enumeration.toInt();
            target = (byte) (value & 0x00ff);
        }

        return target;
    }

    public static PacketTypeIdentifier unmarshalModule8(final byte source) {

        PacketTypeIdentifier target;

        final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.fromInt(0x00ff & source);
        switch (enumeration) {
            case Data: {
                int value = (source & 0x00fe);

                final boolean moreData = ((value & 0x0010) == 0x0010);
                final int packetReceiveSequenceNumber = ((value >> 5) & 0x0007);
                final int packetSendSequenceNumber = ((value >> 1) & 0x0007);

                target = PacketTypeIdentifierFactory.create(enumeration, moreData, packetReceiveSequenceNumber, packetSendSequenceNumber);
            }
            break;

            case ReceiverReady:
            case ReceiverNotReady:
            case Reject: {
                int value = 0x00e0 & source;
                final int packetReceiveSequenceNumber = ((value >> 5) & 0x0007);
                target = PacketTypeIdentifierFactory.create(enumeration, packetReceiveSequenceNumber);
            }
            break;

            default:
                target = PacketTypeIdentifierFactory.create(enumeration);
                break;
        }

        return target;
    }
}
