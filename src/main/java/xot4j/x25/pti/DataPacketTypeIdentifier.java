//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

/**
 *
 * MODULO 8 :
 *
 *        .====================================================.
 *  O  1  ||  Q    D  ||   X   X   ||        L C G N          || D
 *  C     |====================================================| A
 *  T  1  ||                    L  C  N                       || T
 *  E     |====================================================| A
 *  T  1  ||    Pr3   Pr2   Pr1   M     Ps3   Ps2   Ps1   0   ||
 *  S     |====================================================| P
 *     1  ||    USER INFORMATION OR HIGHER-LAYER PROTOCOL     || K
 *        `====================================================' T
 *
 * MODULO 128 :
 *
 *        .====================================================.
 *  O  1  ||  Q    D  ||   X   X   ||        L C G N          || D
 *  C     |====================================================| A
 *  T  1  ||                    L  C  N                       || T
 *  E     |====================================================| A
 *  T  1  ||    Ps7   Ps6   Ps5   Ps4   Ps3   Ps2   Ps1   0   ||
 *  S     |====================================================| P
 *     1  ||    Pr7   Pr6   Pr5   Pr4   Pr3   Pr2   Pr1   M   || K
 *        |====================================================| T
 *     1  ||    USER INFORMATION OR HIGHER-LAYER PROTOCOL     ||
 *        `===================================================='
 *
 * P(R)     Packet receive sequence number which appears in data
 *          and flow control packets or the called DTE address which
 *          may appear in call setup, clearing and registration
 *          packets.
 *
 * P(S)     Packet send sequence number which appears in data packets
 *          or the calling DTE address field which may appear in call
 *          setup, clearing, and registration packets.
 *
 * M        More data bit which appears only in data packets. The field
 *          is set to 1 to indicate that the packet is part of a
 *          sequence of packets that should be treated as a logical
 *          whole.
 */
public interface DataPacketTypeIdentifier extends PacketTypeIdentifier {

    boolean isMoreData();

    int getPacketReceiveSequenceNumber();

    int getPacketSendSequenceNumber();
}
