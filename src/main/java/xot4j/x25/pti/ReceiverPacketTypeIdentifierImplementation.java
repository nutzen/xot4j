//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

final class ReceiverPacketTypeIdentifierImplementation extends BasicPacketTypeIdentifier implements ReceiverPacketTypeIdentifier {

    private final int packetReceiveSequenceNumber;

    ReceiverPacketTypeIdentifierImplementation(
            PacketTypeIdentifierEnumeration enumeration,
            int packetReceiveSequenceNumber) {

        super(enumeration);

        this.packetReceiveSequenceNumber = packetReceiveSequenceNumber;
    }

    @Override
    public boolean isReceiverPacketTypeIdentifier() {

        return true;
    }

    @Override
    public int getPacketReceiveSequenceNumber() {

        return packetReceiveSequenceNumber;
    }
}
