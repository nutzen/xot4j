//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

public final class PacketTypeIdentifierFactory {

    private PacketTypeIdentifierFactory() {
    }

    public static PacketTypeIdentifier create(PacketTypeIdentifierEnumeration enumeration) {

        switch (enumeration) {
            case Data:
            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
                throw new RuntimeException("Expecting more parameters for PTI " + enumeration);
            default:
                break;
        }

        return new BasicPacketTypeIdentifier(enumeration);
    }

    public static PacketTypeIdentifier create(
            PacketTypeIdentifierEnumeration enumeration,
            int packetReceiveSequenceNumber) {


        switch (enumeration) {
            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
                break;
            default:
                throw new RuntimeException("Only Receiver PTIs suppert these parameters");
        }

        return new ReceiverPacketTypeIdentifierImplementation(enumeration, packetReceiveSequenceNumber);
    }

    public static PacketTypeIdentifier create(
            PacketTypeIdentifierEnumeration enumeration,
            boolean moreData,
            int packetReceiveSequenceNumber,
            int packetSendSequenceNumber) {

        if (PacketTypeIdentifierEnumeration.Data != enumeration) {
            throw new RuntimeException("Only Data PTIs suppert these parameters");
        }

        return new DataPacketTypeIdentifierImplementation(enumeration, moreData, packetReceiveSequenceNumber, packetSendSequenceNumber);
    }
}
