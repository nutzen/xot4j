//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.pti;

/**
 *                .=================================.
 *                ||                    ||         ||
 *    LCN <------ ||  M  M  M  M  M  M  || MX  X0  ||---> PACKET
 *                ||                    ||         ||     CONTENT
 *                `================================='
 *                |<- - - - - - - PTI - - - - - - ->|
 *
 *     M = MODIFIER BIT   MX = MODIFIER/OTHER BIT   X0 = 0 OR 1 BIT
 *
 *      (MX,X0) SETTING                   PACKET TYPES
 *       ---------------    ---------------------------------------
 *         (1,1)            All CALLING/CLEARING, INTERRUPT, RESET,
 *                             RESTART, and REGISTRATION packets
 *         (0,1)            RECEIVER READY, RECEIVER NOT READY,
 *                             REJECT, DIAGNOSTIC packets
 *         (X,0)            DATA packets
 */
public interface PacketTypeIdentifier {

    boolean isDataPacketTypeIdentifier();

    boolean isReceiverPacketTypeIdentifier();

    PacketTypeIdentifierEnumeration getEnumeration();
}
