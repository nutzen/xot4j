//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import xot4j.x25.payload.X25Payload;

public final class X25Packet {

    private final X25Header header;
    private final X25Payload payload;

    X25Packet(X25Header header, X25Payload payload) {

        this.header = header;
        this.payload = payload;
    }

    public static X25Packet create(X25Header header, X25Payload payload) {

        return new X25Packet(header, payload);
    }

    public X25Header getHeader() {

        return header;
    }

    public X25Payload getPayload() {

        return payload;
    }
}
