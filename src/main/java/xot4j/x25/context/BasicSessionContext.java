//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.context;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import xot4j.x121.X121Address;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.FacilityEnumeration;
import xot4j.x25.lci.LogicalChannelIdentifier;

public final class BasicSessionContext implements SessionContext {

    private final X121Address localAddress;
    private final Map<FacilityEnumeration, Facility> facilities;
    private volatile X121Address remoteAddress;
    private volatile LogicalChannelIdentifier logicalChannelIdentifier;
    private volatile int packetReceiveSequenceNumber;
    private volatile int packetSendSequenceNumber;
    private volatile ClearRequestEnumeration cause;
    private volatile DiagnosticCodeEnumeration diagnostic;
    private byte[] lastData;
    private volatile boolean expectingMoreData;
    private volatile int resetIndicationCounter;

    private BasicSessionContext(
            X121Address localAddress,
            Map<FacilityEnumeration, Facility> facilities,
            X121Address remoteAddress,
            LogicalChannelIdentifier logicalChannelIdentifier) {

        this.localAddress = localAddress;
        this.facilities = facilities;
        this.remoteAddress = remoteAddress;
        this.logicalChannelIdentifier = logicalChannelIdentifier;
    }

    public static SessionContext create(
            X121Address localAddress,
            Collection<Facility> facilities,
            X121Address remoteAddress,
            LogicalChannelIdentifier logicalChannelIdentifier) {

        if (localAddress.equals(remoteAddress)) {
            throw new RuntimeException("Something must be wrong, because I shouldn't call myself!");
        }

        final Map<FacilityEnumeration, Facility> facilityMap = new LinkedHashMap<>();
        for (Facility facility : facilities) {
            if (null != facility) {
                facilityMap.put(facility.getEnumeration(), facility);
            }
        }

        return new BasicSessionContext(
                localAddress,
                facilityMap,
                remoteAddress,
                logicalChannelIdentifier);
    }

    @Override
    public X121Address getLocalAddress() {

        return localAddress;
    }

    @Override
    public Collection<Facility> getFacilities() {

        return facilities.values();
    }

    @Override
    public void addFacilities(Collection<Facility> facilities) {

        for (Facility facility : facilities) {
            if (null != facility) {
                this.facilities.put(facility.getEnumeration(), facility);
            }
        }
    }

    @Override
    public X121Address getRemoteAddress() {

        return remoteAddress;
    }

    @Override
    public void setRemoteAddress(X121Address address) {

        remoteAddress = address;
        if (localAddress.equals(remoteAddress)) {
            throw new RuntimeException("Something must be wrong, because I shouldn't call myself!");
        }
    }

    @Override
    public LogicalChannelIdentifier getLogicalChannelIdentifier() {

        return logicalChannelIdentifier;
    }

    @Override
    public void setLogicalChannelIdentifier(LogicalChannelIdentifier logicalChannelIdentifier) {

        this.logicalChannelIdentifier = logicalChannelIdentifier;
    }

    @Override
    public int getPacketReceiveSequenceNumber() {

        return packetReceiveSequenceNumber;
    }

    @Override
    public void setPacketReceiveSequenceNumber(int packetReceiveSequenceNumber) {

        this.packetReceiveSequenceNumber = packetReceiveSequenceNumber;
    }

    @Override
    public int getPacketSendSequenceNumber() {

        return packetSendSequenceNumber;
    }

    @Override
    public void setPacketSendSequenceNumber(int packetSendSequenceNumber) {

        this.packetSendSequenceNumber = packetSendSequenceNumber;
    }

    @Override
    public ClearRequestEnumeration getCause() {

        return cause;
    }

    @Override
    public void setCause(ClearRequestEnumeration cause) {

        this.cause = cause;
    }

    @Override
    public DiagnosticCodeEnumeration getDiagnostic() {

        return diagnostic;
    }

    @Override
    public void setDiagnostic(DiagnosticCodeEnumeration diagnostic) {

        this.diagnostic = diagnostic;
    }

    @Override
    public byte[] getLastData() {

        return lastData;
    }

    @Override
    public void setLastData(byte[] data) {

        lastData = data;
    }

    @Override
    public boolean isExpectingMoreData() {

        return expectingMoreData;
    }

    @Override
    public void setExpectingMoreData(boolean expectingMoreData) {

        this.expectingMoreData = expectingMoreData;
    }

    @Override
    public int getResetIndicationCounter() {

        return resetIndicationCounter;
    }

    @Override
    public void setResetIndicationCounter(int resetIndicationCounter) {

        this.resetIndicationCounter = resetIndicationCounter;
    }
}
