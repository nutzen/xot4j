package xot4j.x25.context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import xot4j.x121.X121Address;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.PacketSizeSelection;
import xot4j.x25.facilities.PacketSizeSelectionEnumeration;
import xot4j.x25.facilities.WindowSizeSelection;

public final class SystemContext {

    private final X121Address localAddress;
    private final Collection<Facility> facilities;
    private final AtomicInteger lciSeed;

    SystemContext(X121Address localAddress,
            Collection<Facility> facilities,
            AtomicInteger lciSeed) {

        this.localAddress = localAddress;
        this.facilities = facilities;
        this.lciSeed = lciSeed;
    }

    public X121Address getLocalAddress() {

        return localAddress;
    }

    public Collection<Facility> getFacilities() {

        return facilities;
    }

    public int nextLci() {

        final int lci = lciSeed.getAndDecrement();
        if (1 >= lci) {
            lciSeed.set(0x0fff);
        }
        return lci;
    }

    public static SystemContext create(X121Address localAddress,
            Collection<Facility> facilities) {

        return new SystemContext(localAddress,
                facilities,
                new AtomicInteger(0x0fff));
    }

    public static SystemContext create(X121Address localAddress) {

        return create(localAddress, defaultFacilities());
    }

    static Collection<Facility> defaultFacilities() {

        final Collection<Facility> facilities = new ArrayList<>();
        facilities.add(WindowSizeSelection.create(1, 1));
        facilities.add(PacketSizeSelection.create(PacketSizeSelectionEnumeration.Size128octets, PacketSizeSelectionEnumeration.Size128octets));

        return Collections.unmodifiableCollection(facilities);
    }
}
