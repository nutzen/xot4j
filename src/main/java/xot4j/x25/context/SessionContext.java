//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.context;

import java.util.Collection;
import xot4j.x121.X121Address;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.facilities.Facility;
import xot4j.x25.lci.LogicalChannelIdentifier;

public interface SessionContext {

    X121Address getLocalAddress();

    Collection<Facility> getFacilities();

    void addFacilities(Collection<Facility> facilities);

    X121Address getRemoteAddress();

    void setRemoteAddress(X121Address address);

    LogicalChannelIdentifier getLogicalChannelIdentifier();

    void setLogicalChannelIdentifier(LogicalChannelIdentifier logicalChannelIdentifier);

    int getPacketReceiveSequenceNumber();

    void setPacketReceiveSequenceNumber(int packetReceiveSequenceNumber);

    int getPacketSendSequenceNumber();

    void setPacketSendSequenceNumber(int packetSendSequenceNumber);

    ClearRequestEnumeration getCause();

    void setCause(ClearRequestEnumeration cause);

    DiagnosticCodeEnumeration getDiagnostic();

    void setDiagnostic(DiagnosticCodeEnumeration diagnostic);

    byte[] getLastData();

    void setLastData(byte[] data);

    boolean isExpectingMoreData();

    void setExpectingMoreData(boolean expectingMoreData);

    int getResetIndicationCounter();

    void setResetIndicationCounter(int resetIndicationCounter);
}
