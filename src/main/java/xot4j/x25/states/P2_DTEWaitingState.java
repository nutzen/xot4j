//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public class P2_DTEWaitingState extends BaseState implements State {

    private P2_DTEWaitingState() {

        super(StateEnumeration.P2_DTEWaiting);
    }

    public static State create() {

        return new P2_DTEWaitingState();
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {

            case CallIncoming:
                stateContext.setState(P5_CallCollisionState.create());
                break;

            case CallConnected:
                stateContext.setState(P4_DataTransferState.create());
                break;

            case ClearRequest:
                stateContext.setState(P6_DTEClearingState.create());
                break;

            case ClearIndication:
                stateContext.setState(P7_DCEClearingState.create());
                break;

            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
