//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

class BaseState implements State {

    private final StateEnumeration enumeration;

    protected BaseState(StateEnumeration enumeration) {
        this.enumeration = enumeration;
    }

    @Override
    public StateEnumeration getEnumeration() {

        return enumeration;
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {
            case Data:
            case Interrupt:
            case InterruptConfirmation:
            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
            case Diagnostic:
            // P-State transitions
            case CallRequest:
            case CallIncoming:
            case CallAccepted:
            case CallConnected:
            case ClearRequest:
            case ClearIndication:
            case ClearConfirmation:
            // D-State transitions
            case ResetRequest:
            case ResetIndication:
            case ResetConfirmation:
            // R-State transitions
            case RestartRequest:
            case RestartIndication:
            case RestartConfirmation:
            default:
                throw X25StateTransitionFailure.create(getEnumeration(), transition);
        }
    }
}
