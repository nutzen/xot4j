//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import static xot4j.x25.states.StateEnumerationUtility.*;

public enum StateEnumeration {

    // P-states
    P1_Ready,
    P2_DTEWaiting,
    P3_DCEWaiting,
    P4_DataTransfer,
    P5_CallCollision,
    P6_DTEClearing,
    P7_DCEClearing,
    // D-states
    D1_FlowControlReady,
    D2_DTEReseting,
    D3_DCEReseting,
    // R-states
    R1_PacketLayerReady,
    R2_DTERestarting,
    R3_DCERestarting;

    public String toDescription() {

        String destination;

        switch (this) {
            case P1_Ready:
                destination = P1_READY_DESCRIPTION;
                break;

            case P2_DTEWaiting:
                destination = P2_DTE_WAITING_DESCRIPTION;
                break;

            case P3_DCEWaiting:
                destination = P3_DCE_WAITING_DESCRIPTION;
                break;

            case P4_DataTransfer:
                destination = P4_DATA_TRANSFER_DESCRIPTION;
                break;

            case P5_CallCollision:
                destination = P5_CALL_COLLISION_DESCRIPTION;
                break;

            case P6_DTEClearing:
                destination = P6_DTE_CLEARING_DESCRIPTION;
                break;

            case P7_DCEClearing:
                destination = P7_DCE_CLEARING_DESCRIPTION;
                break;

            case D1_FlowControlReady:
                destination = D1_FLOW_CONTROL_READY_DESCRIPTION;
                break;

            case D2_DTEReseting:
                destination = D2_DTE_RESETING_DESCRIPTION;
                break;

            case D3_DCEReseting:
                destination = D3_DCE_RESETING_DESCRIPTION;
                break;

            case R1_PacketLayerReady:
                destination = R1_PACKET_LAYER_READY_DESCRIPTION;
                break;

            case R2_DTERestarting:
                destination = R2_DTE_RESTARTING_DESCRIPTION;
                break;

            case R3_DCERestarting:
                destination = R3_DCE_RESTARTING_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static StateEnumeration fromDescription(final String value) {

        final StateEnumeration destination;
        switch (value) {
            case P1_READY_DESCRIPTION:
                destination = P1_Ready;
                break;

            case P2_DTE_WAITING_DESCRIPTION:
                destination = P2_DTEWaiting;
                break;

            case P3_DCE_WAITING_DESCRIPTION:
                destination = P3_DCEWaiting;
                break;

            case P4_DATA_TRANSFER_DESCRIPTION:
                destination = P4_DataTransfer;
                break;

            case P5_CALL_COLLISION_DESCRIPTION:
                destination = P5_CallCollision;
                break;

            case P6_DTE_CLEARING_DESCRIPTION:
                destination = P6_DTEClearing;
                break;

            case P7_DCE_CLEARING_DESCRIPTION:
                destination = P7_DCEClearing;
                break;

            case D1_FLOW_CONTROL_READY_DESCRIPTION:
                destination = D1_FlowControlReady;
                break;

            case D2_DTE_RESETING_DESCRIPTION:
                destination = D2_DTEReseting;
                break;

            case D3_DCE_RESETING_DESCRIPTION:
                destination = D3_DCEReseting;
                break;

            case R1_PACKET_LAYER_READY_DESCRIPTION:
                destination = R1_PacketLayerReady;
                break;

            case R2_DTE_RESTARTING_DESCRIPTION:
                destination = R2_DTERestarting;
                break;

            case R3_DCE_RESTARTING_DESCRIPTION:
                destination = R3_DCERestarting;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }
}
