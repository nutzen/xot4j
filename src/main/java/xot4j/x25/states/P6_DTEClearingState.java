//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public class P6_DTEClearingState extends BaseState implements State {

    private P6_DTEClearingState() {

        super(StateEnumeration.P6_DTEClearing);
    }

    public static State create() {

        return new P6_DTEClearingState();
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {

            case CallIncoming:
                break;

            case CallConnected:
                break;

            case ClearRequest:
                break;

            case ClearIndication:
                stateContext.setState(P1_ReadyState.create());
                break;

            case ClearConfirmation:
                stateContext.setState(P1_ReadyState.create());
                break;

            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
