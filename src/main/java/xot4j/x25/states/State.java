//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

/**
 *
 * Circuits state transition table
 *
 * .===================================================================================================.
 * ||                      State || Packet Layer Ready  || DTE Restarting      || DCE Restarting      ||
 * || Input                      || (R1)                || (R2)                || (R3)                ||
 * |===================================================================================================|
 * || Call set-up and clearing - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Call Request               ||                     ||                     ||                     ||
 * || Call Incoming              ||                     ||                     ||                     ||
 * || Call Accepted              ||                     ||                     ||                     ||
 * || Call Connected             ||                     ||                     ||                     ||
 * || Clear Request              ||                     ||                     ||                     ||
 * || Clear Indication           ||                     ||                     ||                     ||
 * || Clear Confirmation         ||                     ||                     ||                     ||
 * || Data and interrupt - - - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Data                       ||                     ||                     ||                     ||
 * || Interrupt                  ||                     ||                     ||                     ||
 * || Interrupt Confirmation     ||                     ||                     ||                     ||
 * || Flow control and reset - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Receiver Ready             ||                     ||                     ||                     ||
 * || Receiver Not Ready         ||                     ||                     ||                     ||
 * || Reject                     ||                     ||                     ||                     ||
 * || Reset Request              ||                     ||                     ||                     ||
 * || Reset Indication           ||                     ||                     ||                     ||
 * || Reset Confirmation         ||                     ||                     ||                     ||
 * || Restart  - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Restart Request            || DTE Restarting      || DTE Restarting      || Packet Layer Ready  ||
 * || Restart Indication         || DCE Restarting      || Packet Layer Ready  || DCE Restarting      ||
 * || Restart Confirmation       ||                     || Packet Layer Ready  || Packet Layer Ready  ||
 * || Diagnostic - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Diagnostic                 ||                     ||                     ||                     ||
 * `==================================================================================================='
 *
 *
 * Call phase state transition table
 * (Subset of Packet Layer Ready - R1)
 *
 * .===================================================================================================================================================================.
 * ||                      State || Ready           || DTE Waiting     || DCE Waiting     || Data Transfer   || Call Collision  || DTE Clearing    || DCE Clearing    ||
 * || Input                      || (P1)            || (P2)            || (P3)            || (P4)            || (P5)            || (P6)            || (P7)            ||
 * |===================================================================================================================================================================|
 * || Call set-up and clearing - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - ||
 * || Call Request               || DTE Waiting     ||                 || Call Collision  ||                 ||                 ||                 || DCE Clearing    ||
 * || Call Incoming              || DCE Waiting     || Call Collision  ||                 ||                 ||                 || DTE Clearing    ||                 ||
 * || Call Accepted              ||                 ||                 || Data Transfer   ||                 ||                 ||                 || DCE Clearing    ||
 * || Call Connected             ||                 || Data Transfer   ||                 ||                 || Data Transfer   || DTE Clearing    ||                 ||
 * || Clear Request              || DTE Clearing    || DTE Clearing    || DTE Clearing    || DTE Clearing    || DTE Clearing    || DTE Clearing    || Ready           ||
 * || Clear Indication           || DCE Clearing    || DCE Clearing    || DCE Clearing    || DCE Clearing    || DCE Clearing    || Ready           || DCE Clearing    ||
 * || Clear Confirmation         ||                 ||                 ||                 ||                 ||                 || Ready           || Ready           ||
 * || Data and interrupt - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - ||
 * || Data                       ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Interrupt                  ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Interrupt Confirmation     ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Flow control and reset - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - ||
 * || Receiver Ready             ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Receiver Not Ready         ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Reject                     ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Reset Request              ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Reset Indication           ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Reset Confirmation         ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Restart  - - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - ||
 * || Restart Request            ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Restart Indication         ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Restart Confirmation       ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * || Diagnostic - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - || - - - - - - - - ||
 * || Diagnostic                 ||                 ||                 ||                 ||                 ||                 ||                 ||                 ||
 * `==================================================================================================================================================================='
 *
 * 
 * Flow control state transition table
 * (Subset of Data Transfer - P4)
 *
 * .===================================================================================================.
 * ||                      State || Flow Control Ready  || DTE Reseting        || DCE Reseting        ||
 * || Input                      || (D1)                || (D2)                || (D3)                ||
 * |===================================================================================================|
 * || Call set-up and clearing - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Call Request               ||                     ||                     ||                     ||
 * || Call Incoming              ||                     ||                     ||                     ||
 * || Call Accepted              ||                     ||                     ||                     ||
 * || Call Connected             ||                     ||                     ||                     ||
 * || Clear Request              ||                     ||                     ||                     ||
 * || Clear Indication           ||                     ||                     ||                     ||
 * || Clear Confirmation         ||                     ||                     ||                     ||
 * || Data and interrupt - - - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Data                       ||                     ||                     ||                     ||
 * || Interrupt                  ||                     ||                     ||                     ||
 * || Interrupt Confirmation     ||                     ||                     ||                     ||
 * || Flow control and reset - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Receiver Ready             ||                     ||                     ||                     ||
 * || Receiver Not Ready         ||                     ||                     ||                     ||
 * || Reject                     ||                     ||                     ||                     ||
 * || Reset Request              || DTE Reseting        || DTE Reseting        || Flow Control Ready  ||
 * || Reset Indication           || DCE Reseting        || Flow Control Ready  || DCE Reset Requested ||
 * || Reset Confirmation         ||                     || Flow Control Ready  || Flow Control Ready  ||
 * || Restart  - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Restart Request            ||                     ||                     ||                     ||
 * || Restart Indication         ||                     ||                     ||                     ||
 * || Restart Confirmation       ||                     ||                     ||                     ||
 * || Diagnostic - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - || - - - - - - - - - - ||
 * || Diagnostic                 ||                     ||                     ||                     ||
 * `==================================================================================================='
 *
 */
public interface State {

    StateEnumeration getEnumeration();

    void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure;
}
