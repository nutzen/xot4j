//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public class R2_DTERestartingState extends BaseState implements State {

    private R2_DTERestartingState() {

        super(StateEnumeration.R2_DTERestarting);
    }

    public static State create() {

        return new R2_DTERestartingState();
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {
            case RestartRequest:
                break;

            case RestartIndication:
                stateContext.setState(R1_PacketLayerReadyState.create());
                break;

            case RestartConfirmation:
                stateContext.setState(R1_PacketLayerReadyState.create());
                break;

            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
