//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public class D2_DTEResetingState extends BaseState implements State {

    private D2_DTEResetingState() {

        super(StateEnumeration.D2_DTEReseting);
    }

    public static State create() {

        return new D2_DTEResetingState();
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {
            case ResetRequest:
                break;

            case ResetIndication:
                stateContext.setState(D1_FlowControlReadyState.create());
                break;

            case ResetConfirmation:
                stateContext.setState(D1_FlowControlReadyState.create());
                break;

            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
