//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public final class StateMachine {

    private final Lock lock = new ReentrantLock();
    private final Collection<String> history = new ArrayList<>();
    private final long timeOfBirth = System.currentTimeMillis();
    private final StateContext stateContext;

    private StateMachine(StateContext stateContext) {
        this.stateContext = stateContext;
    }

    public static StateMachine create() {

        final State state = R1_PacketLayerReadyState.create();
        final StateContext stateContext = BasicStateContext.create(state);

        return new StateMachine(stateContext);
    }

    public StateEnumeration getStateEnumeration() {

        lock.lock();
        try {
            final State state = stateContext.getState();
            return state.getEnumeration();

        } finally {
            lock.unlock();
        }
    }

    public Collection<String> getHistory() {

        lock.lock();
        try {
            return Collections.unmodifiableCollection(history);

        } finally {
            lock.unlock();
        }
    }

    public void handle(TransitionEnumeration transitionEnumeration) throws X25StateTransitionFailure {

        lock.lock();
        try {
            final StateEnumeration original = getStateEnumeration();
            try {
                final State state = stateContext.getState();
                state.handle(stateContext, transitionEnumeration);

            } finally {
                updateHistory(original, transitionEnumeration);
            }

        } finally {
            lock.unlock();
        }
    }

    private void updateHistory(StateEnumeration original, TransitionEnumeration transition) {

        final StateEnumeration enumeration = getStateEnumeration();
        if (original != enumeration) {
            final long timeStamp = System.currentTimeMillis() - timeOfBirth;
            final String pattern = "(3) {0} ^ {1} => {2}";
            final String transaction = MessageFormat.format(pattern,
                    original.toDescription(),
                    transition.toDescription(),
                    enumeration.toDescription(),
                    Long.toString(timeStamp));
            history.add(transaction);
        }
    }
}
