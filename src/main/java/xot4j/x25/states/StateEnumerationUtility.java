//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

public final class StateEnumerationUtility {

    // P-states DESCRIPTION
    public static final String P1_READY_DESCRIPTION = "P1_Ready";
    public static final String P2_DTE_WAITING_DESCRIPTION = "P2_DTE_Waiting";
    public static final String P3_DCE_WAITING_DESCRIPTION = "P3_DCE_Waiting";
    public static final String P4_DATA_TRANSFER_DESCRIPTION = "P4_Data_Transfer";
    public static final String P5_CALL_COLLISION_DESCRIPTION = "P5_Call_Collision";
    public static final String P6_DTE_CLEARING_DESCRIPTION = "P6_DTE_Clearing";
    public static final String P7_DCE_CLEARING_DESCRIPTION = "P7_DCE_Clearing";
    // D-states DESCRIPTION
    public static final String D1_FLOW_CONTROL_READY_DESCRIPTION = "D1_Flow_Control_Ready";
    public static final String D2_DTE_RESETING_DESCRIPTION = "D2_DTE_Reseting";
    public static final String D3_DCE_RESETING_DESCRIPTION = "D3_DCE_Reseting";
    // R-states DESCRIPTION
    public static final String R1_PACKET_LAYER_READY_DESCRIPTION = "R1_Packet_Layer_Ready";
    public static final String R2_DTE_RESTARTING_DESCRIPTION = "R2_DTE_Restarting";
    public static final String R3_DCE_RESTARTING_DESCRIPTION = "R3_DCE_Restarting";

    private StateEnumerationUtility() {
    }
}
