//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

class BasicStateContext implements StateContext {

    private volatile State state;

    private BasicStateContext(State state) {

        this.state = state;
    }

    public static StateContext create(State state) {

        return new BasicStateContext(state);
    }

    @Override
    public State getState() {

        return state;
    }

    @Override
    public void setState(State state) {

        this.state = state;
    }
}
