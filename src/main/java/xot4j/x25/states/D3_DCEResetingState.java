//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public class D3_DCEResetingState extends BaseState implements State {

    private D3_DCEResetingState() {

        super(StateEnumeration.D3_DCEReseting);
    }

    public static State create() {

        return new D3_DCEResetingState();
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {
            case ResetRequest:
                stateContext.setState(D1_FlowControlReadyState.create());
                break;

            case ResetIndication:
                break;

            case ResetConfirmation:
                stateContext.setState(D1_FlowControlReadyState.create());
                break;

            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
