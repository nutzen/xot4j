//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public class P4_DataTransferState extends BaseState implements State, StateContext {

    private final StateContext stateContext /* D-states */;

    private P4_DataTransferState(StateContext stateContext) {

        super(StateEnumeration.P4_DataTransfer);
        this.stateContext = stateContext;
    }

    public static State create() {

        final State state = D1_FlowControlReadyState.create();
        final StateContext stateContext = BasicStateContext.create(state);
        return new P4_DataTransferState(stateContext);
    }

    @Override
    public StateEnumeration getEnumeration() {

        final State state = stateContext.getState();
        return state.getEnumeration();
    }

    @Override
    public State getState() {

        final State state = stateContext.getState();
        return state;
    }

    @Override
    public void setState(State state) {

        stateContext.setState(state);
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {

            case ClearRequest:
                stateContext.setState(P6_DTEClearingState.create());
                break;

            case ClearIndication:
                stateContext.setState(P7_DCEClearingState.create());
                break;

            case Data:
            case Interrupt:
            case InterruptConfirmation:
            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
            case Diagnostic:
            // D-State transitions
            case ResetRequest:
            case ResetIndication:
            case ResetConfirmation:
                final State state = getState();
                state.handle(this, transition);
                break;

            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
