//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public final class R1_PacketLayerReadyState extends BaseState implements State, StateContext {

    private final StateContext stateContext /* P-states */;

    private R1_PacketLayerReadyState(StateContext stateContext) {

        super(StateEnumeration.R1_PacketLayerReady);
        this.stateContext = stateContext;
    }

    public static State create() {

        final State state = P1_ReadyState.create();
        final StateContext stateContext = BasicStateContext.create(state);
        return new R1_PacketLayerReadyState(stateContext);
    }

    @Override
    public StateEnumeration getEnumeration() {

        final State state = stateContext.getState();
        return state.getEnumeration();
    }

    @Override
    public State getState() {

        final State state = stateContext.getState();
        return state;
    }

    @Override
    public void setState(State state) {

        stateContext.setState(state);
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {
            case Data:
            case Interrupt:
            case InterruptConfirmation:
            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
            case Diagnostic:
            // P-State transitions
            case CallRequest:
            case CallIncoming:
            case CallAccepted:
            case CallConnected:
            case ClearRequest:
            case ClearIndication:
            case ClearConfirmation:
            // D-State transitions
            case ResetRequest:
            case ResetIndication:
            case ResetConfirmation:
                final State state = getState();
                state.handle(this, transition);
                break;

            // R-State transitions
            case RestartRequest:
                stateContext.setState(R2_DTERestartingState.create());
                break;

            case RestartIndication:
                stateContext.setState(R3_DCERestartingState.create());
                break;

            case RestartConfirmation:
            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
