//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.states;

import xot4j.exceptions.X25StateTransitionFailure;
import xot4j.x25.transitions.TransitionEnumeration;

public final class D1_FlowControlReadyState extends BaseState implements State {

    private D1_FlowControlReadyState() {

        super(StateEnumeration.D1_FlowControlReady);
    }

    public static State create() {

        return new D1_FlowControlReadyState();
    }

    @Override
    public void handle(StateContext stateContext, TransitionEnumeration transition) throws X25StateTransitionFailure {

        switch (transition) {
            case Data:
            case Interrupt:
            case InterruptConfirmation:
            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
                break;

            case ResetRequest:
                stateContext.setState(D2_DTEResetingState.create());
                break;

            case ResetIndication:
                stateContext.setState(D3_DCEResetingState.create());
                break;

            case ResetConfirmation:
            default:
                super.handle(stateContext, transition);
                break;
        }
    }
}
