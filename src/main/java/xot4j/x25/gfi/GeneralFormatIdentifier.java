//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.gfi;

/**
 *                  .=============================.
 *  FRAME LAYER     ||     ||     ||             ||    LOGICAL
 *  CONTROL BYTE <--||  Q  ||  D  ||   X    X    ||--> CHANNEL
 *                  ||     ||     ||             ||  IDENTIFIER
 *                  `============================='
 * 
 *     Q: QUALIFIED DATA BIT        - 0 = Data for user
 *                                    1 = Data for PAD
 * 
 *     D: DELIVERY CONFIRMATION BIT - 0 = For local acknowledgment
 *                                    1 = For remote acknowledgment
 * 
 *    XX: PROTOCOL IDENTIFICATION  - 00 = Reserved for future use
 *                                   01 = Modulo 8 sequencing
 *                                   10 = Modulo 128 sequencing
 *                                   11 = Extended format
 */
public final class GeneralFormatIdentifier {

    private final QualifiedData qualifiedData;
    private final DeliveryConfirmation deliveryConfirmation;
    private final ProtocolIdentification protocolIdentification;

    // GFI 4bit value, default 00 01
    GeneralFormatIdentifier(final QualifiedData qualifiedData,
            final DeliveryConfirmation deliveryConfirmation,
            final ProtocolIdentification protocolIdentification) {

        this.qualifiedData = qualifiedData;
        this.deliveryConfirmation = deliveryConfirmation;
        this.protocolIdentification = protocolIdentification;
    }

    public static GeneralFormatIdentifier create(final QualifiedData qualifiedData,
            final DeliveryConfirmation deliveryConfirmation,
            final ProtocolIdentification protocolIdentification) {

        return new GeneralFormatIdentifier(qualifiedData, deliveryConfirmation, protocolIdentification);
    }

    public QualifiedData getQualifiedData() {

        return qualifiedData;
    }

    public DeliveryConfirmation getDeliveryConfirmation() {

        return deliveryConfirmation;
    }

    public ProtocolIdentification getProtocolIdentification() {

        return protocolIdentification;
    }

    /**
     * Q: QUALIFIED DATA BIT
     * - 0 = Data for user
     * - 1 = Data for PAD
     */
    public enum QualifiedData {

        DataForUser,
        DataForPad;

        public int toInt() {

            switch (this) {
                case DataForUser:
                    return 0;

                case DataForPad:
                    return 1;

                default:
                    throw new RuntimeException("Unhandled type: " + this);
            }
        }

        public static QualifiedData fromInt(final int value) {

            switch (value) {
                case 0:
                    return DataForUser;

                case 1:
                    return DataForPad;

                default:
                    throw new RuntimeException("Unhandled value: " + value);
            }
        }
    }

    /**
     * D: DELIVERY CONFIRMATION BIT
     * - 0 = For local acknowledgment
     * - 1 = For remote acknowledgment
     */
    public enum DeliveryConfirmation {

        ForLocalAcknowledgment,
        ForRemoteAcknowledgment;

        public int toInt() {

            switch (this) {
                case ForLocalAcknowledgment:
                    return 0;

                case ForRemoteAcknowledgment:
                    return 1;

                default:
                    throw new RuntimeException("Unhandled type: " + this);
            }
        }

        public static DeliveryConfirmation fromInt(final int value) {

            switch (value) {
                case 0:
                    return ForLocalAcknowledgment;

                case 1:
                    return ForRemoteAcknowledgment;

                default:
                    throw new RuntimeException("Unhandled value: " + value);
            }
        }
    }

    /**
     * XX: PROTOCOL IDENTIFICATION
     * - 00 = Reserved for future use
     * - 01 = Modulo 8 sequencing
     * - 10 = Modulo 128 sequencing
     * - 11 = Extended format
     */
    public enum ProtocolIdentification {

        ReservedForFutureUse,
        Modulo8Sequencing,
        Modulo128Sequencing,
        ExtendedFormat;

        public int toInt() {

            switch (this) {
                case ReservedForFutureUse:
                    return 0x00;

                case Modulo8Sequencing:
                    return 0x01;

                case Modulo128Sequencing:
                    return 0x02;

                case ExtendedFormat:
                    return 0x03;

                default:
                    throw new RuntimeException("Unhandled type: " + this);
            }
        }

        public static ProtocolIdentification fromInt(final int value) {

            switch (value) {
                case 0x00:
                    return ReservedForFutureUse;

                case 0x01:
                    return Modulo8Sequencing;

                case 0x02:
                    return Modulo128Sequencing;

                case 0x03:
                    return ExtendedFormat;

                default:
                    throw new RuntimeException("Unhandled value: " + value);
            }
        }
    }
}
