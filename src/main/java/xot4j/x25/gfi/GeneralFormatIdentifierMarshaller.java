//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.gfi;

public final class GeneralFormatIdentifierMarshaller {

    private GeneralFormatIdentifierMarshaller() {
    }

    public static byte marshal(final GeneralFormatIdentifier generalFormatIdentifier) {

        int target = 0;

        target = target | generalFormatIdentifier.getQualifiedData().toInt();
        target = ((target << 1) & 0x00fe) | generalFormatIdentifier.getDeliveryConfirmation().toInt();
        target = ((target << 2) & 0x00fc) | generalFormatIdentifier.getProtocolIdentification().toInt();

        return (byte) (target & 0x00ff);
    }

    public static GeneralFormatIdentifier unmarshal(final byte gfi) {

        final GeneralFormatIdentifier.QualifiedData qualifiedData = GeneralFormatIdentifier.QualifiedData.fromInt((gfi >> 3) & 0x01);
        final GeneralFormatIdentifier.DeliveryConfirmation deliveryConfirmation = GeneralFormatIdentifier.DeliveryConfirmation.fromInt((gfi >> 2) & 0x01);
        final GeneralFormatIdentifier.ProtocolIdentification protocolIdentification = GeneralFormatIdentifier.ProtocolIdentification.fromInt(gfi & 0x03);

        return GeneralFormatIdentifier.create(qualifiedData, deliveryConfirmation, protocolIdentification);
    }
}
