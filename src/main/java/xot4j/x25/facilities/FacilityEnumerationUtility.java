//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

public final class FacilityEnumerationUtility {

    // DESCRIPTION
    public static final String REVERSE_CHARGING_AND_FAST_SELECT_DESCRIPTION = "Reverse_charging_and_fast_select";
    public static final String THROUGHPUT_CLASS_DESCRIPTION = "Throughput_class";
    public static final String CLOSED_USER_GROUP_SELECTION_DESCRIPTION = "Closed_user_group_selection";
    public static final String CHARGING_INFORMATION_REQUEST_DESCRIPTION = "Charging_information_request";
    public static final String CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_DESCRIPTION = "Called_line_address_modified_notification";
    public static final String CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_DESCRIPTION = "Closed_User_Group_(CUG)_with_outgoing_access";
    public static final String QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_DESCRIPTION = "Quality_of_Service_Negotiation_-_minimum_throughput_class";
    public static final String EXPEDITED_DATA_NEGOTIATION_DESCRIPTION = "Expedited_Data_Negotiation";
    public static final String BILATERAL_CLOSED_USER_GROUP_SELECTION_DESCRIPTION = "Bilateral_closed_user_group_selection";
    public static final String PACKET_SIZE_SELECTION_DESCRIPTION = "Packet_size_selection";
    public static final String WINDOW_SIZE_SELECTION_DESCRIPTION = "Window_size_selection";
    public static final String RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_DESCRIPTION = "Recognized_Private_Operating_Agency_(RPOA)_selection_(basic_format)";
    public static final String TRANSIT_DELAY_SELECTION_AND_INDICATION_DESCRIPTION = "Transit_delay_selection_and_indication";
    public static final String CHARGING_CALL_DURATION_DESCRIPTION = "Charging_(call_duration)";
    public static final String CHARGING_SEGMENT_COUNT_DESCRIPTION = "Charging_(segment_count)";
    public static final String CALL_REDIRECTION_NOTIFICATION_DESCRIPTION = "Call_redirection_notification";
    public static final String RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_DESCRIPTION = "Recognized_Private_Operating_Agency_(RPOA)_selection_(extended_format)";
    public static final String CHARGING_MONETARY_UNIT_DESCRIPTION = "Charging_(monetary_unit)";
    public static final String NETWORK_USER_IDENTIFICATION_DESCRIPTION = "Network_User_identification_(NUI)";
    public static final String CALLED_ADDRESS_EXTENSION_OSI_DESCRIPTION = "Called_Address_Extension_(OSI)";
    public static final String QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_DESCRIPTION = "Quality_of_Service_Negotiation_-_End_to_end_transit_delay";
    public static final String CALLING_ADDRESS_EXTENTION_OSI_DESCRIPTION = "Calling_Address_Extension_(OSI)";
    // MASK
    public static final int REVERSE_CHARGING_AND_FAST_SELECT_MASK = 0x01;
    public static final int THROUGHPUT_CLASS_MASK = 0x02;
    public static final int CLOSED_USER_GROUP_SELECTION_MASK = 0x03;
    public static final int CHARGING_INFORMATION_REQUEST_MASK = 0x04;
    public static final int CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_MASK = 0x08;
    public static final int CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_MASK = 0x09;
    public static final int QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_MASK = 0x0a;
    public static final int EXPEDITED_DATA_NEGOTIATION_MASK = 0x0b;
    public static final int BILATERAL_CLOSED_USER_GROUP_SELECTION_MASK = 0x41;
    public static final int PACKET_SIZE_SELECTION_MASK = 0x42;
    public static final int WINDOW_SIZE_SELECTION_MASK = 0x43;
    public static final int RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_MASK = 0x44;
    public static final int TRANSIT_DELAY_SELECTION_AND_INDICATION_MASK = 0x49;
    public static final int CHARGING_CALL_DURATION_MASK = 0xc1;
    public static final int CHARGING_SEGMENT_COUNT_MASK = 0xc2;
    public static final int CALL_REDIRECTION_NOTIFICATION_MASK = 0xc4;
    public static final int RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_MASK = 0xc5;
    public static final int CHARGING_MONETARY_UNIT_MASK = 0xc3;
    public static final int NETWORK_USER_IDENTIFICATION_MASK = 0xc6;
    public static final int CALLED_ADDRESS_EXTENSION_OSI_MASK = 0xc9;
    public static final int QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_MASK = 0xca;
    public static final int CALLING_ADDRESS_EXTENTION_OSI_MASK = 0xcb;

    static boolean compareWithMask(final int mask, final int value) {

        return (mask == value);
    }

    private FacilityEnumerationUtility() {
    }
}
