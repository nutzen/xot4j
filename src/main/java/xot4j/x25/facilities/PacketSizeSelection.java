//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

public final class PacketSizeSelection extends BaseFacility {

    private static final FacilityEnumeration ENUMERATION = FacilityEnumeration.PacketSizeSelection;
    private final PacketSizeSelectionEnumeration transmitPacketSize;
    private final PacketSizeSelectionEnumeration receivePacketSize;

    private PacketSizeSelection(
            PacketSizeSelectionEnumeration transmitPacketSize,
            PacketSizeSelectionEnumeration receivePacketSize) {

        super(ENUMERATION);
        this.transmitPacketSize = transmitPacketSize;
        this.receivePacketSize = receivePacketSize;
    }

    public static PacketSizeSelection create(
            PacketSizeSelectionEnumeration transmitPacketSize,
            PacketSizeSelectionEnumeration receivePacketSize) {

        return new PacketSizeSelection(transmitPacketSize, receivePacketSize);
    }

    public PacketSizeSelectionEnumeration getTransmitPacketSize() {

        return transmitPacketSize;
    }

    public PacketSizeSelectionEnumeration getReceivePacketSize() {

        return receivePacketSize;
    }
}
