//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

public final class FacilityMarshaller {

    private FacilityMarshaller() {
    }

    public static byte[] marshal(Collection<Facility> source) {

        try {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            for (Facility facility : source) {
                final byte[] buffer = marshal(facility);
                if (null == buffer) {
                    // ignore
                } else {
                    outputStream.write(buffer);
                }
            }

            return outputStream.toByteArray();

        } catch (IOException e) {
            throw new RuntimeException("This should not happen: " + e.getMessage(), e);
        }
    }

    public static Collection<Facility> unmarshal(final byte[] source) {

        try {
            final Collection<Facility> target = new ArrayList<>();
            final InputStream inputStream = new ByteArrayInputStream(source);

            for (int read = inputStream.read(); -1 != read; read = inputStream.read()) {
                final FacilityEnumeration enumeration = FacilityEnumeration.fromInt(read & 0x00ff);
                final Facility facility = unmarshal(enumeration, inputStream);

                if (null == facility) {
                    // ignore
                } else {
                    target.add(facility);
                }
            }

            return target;

        } catch (IOException e) {
            throw new RuntimeException("This should not happen: " + e.getMessage(), e);
        }
    }

    static byte[] marshal(Facility source) {

        byte[] target = null;
        if (null == source) {
            // ignore
        } else {
            final FacilityEnumeration enumeration = source.getEnumeration();

            switch (enumeration) {

                // one parameter
                case ReverseChargingAndFastSelect:
                    target = marshalReverseChargingAndFastSelect(source);
                    break;

                case ThroughputClass:
                    target = marshalThroughputClass(source);
                    break;

                case ClosedUserGroupSelection:
                    target = marshalClosedUserGroupSelection(source);
                    break;

                case ChargingInformationRequest:
                    target = marshalChargingInformationRequest(source);
                    break;

                case CalledLineAddressModifiedNotification:
                    target = marshalCalledLineAddressModifiedNotification(source);
                    break;

                case ClosedUserGroupWithOutgoingAccess:
                    target = marshalClosedUserGroupWithOutgoingAccess(source);
                    break;

                case QualityOfServiceNegotiationMinimumThroughputClass:
                    target = marshalQualityOfServiceNegotiationMinimumThroughputClass(source);
                    break;

                case ExpeditedDataNegotiation:
                    target = marshalExpeditedDataNegotiation(source);
                    break;

                // two parameters
                case BilateralClosedUserGroupSelection:
                    target = marshalBilateralClosedUserGroupSelection(source);
                    break;

                case PacketSizeSelection:
                    target = marshalPacketSizeSelection(source);
                    break;

                case WindowSizeSelection:
                    target = marshalWindowSizeSelection(source);
                    break;

                case RecognizedPrivateOperatingAgencySelectionBasicFormat:
                    target = marshalRecognizedPrivateOperatingAgencySelectionBasicFormat(source);
                    break;

                case TransitDelaySelectionAndIndication:
                    target = marshalTransitDelaySelectionAndIndication(source);
                    break;

                // variable parameters
                case ChargingCallDuration:
                    target = marshalChargingCallDuration(source);
                    break;

                case ChargingSegmentCount:
                    target = marshalChargingSegmentCount(source);
                    break;

                case CallRedirectionNotification:
                    target = marshalCallRedirectionNotification(source);
                    break;

                case RecognizedPrivateOperatingAgencySelectionExtendedFormat:
                    target = marshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(source);
                    break;

                case ChargingMonetaryUnit:
                    target = marshalChargingMonetaryUnit(source);
                    break;

                case NetworkUserIdentification:
                    target = marshalNetworkUserIdentification(source);
                    break;

                case CalledAddressExtensionOsi:
                    target = marshalCalledAddressExtensionOsi(source);
                    break;

                case QualityOfServiceNegotiationEndToEndTransitDelay:
                    target = marshalQualityOfServiceNegotiationEndToEndTransitDelay(source);
                    break;

                case CallingAddressExtensionOsi:
                    target = marshalCallingAddressExtensionOsi(source);
                    break;

                default:
                    throw new RuntimeException("Unhandled type: " + enumeration);
            }
        }

        return target;
    }

    static Facility unmarshal(FacilityEnumeration enumeration, InputStream source) throws IOException {

        Facility target = null;

        switch (enumeration) {

            // one parameter
            case ReverseChargingAndFastSelect:
                target = unmarshalReverseChargingAndFastSelect(source);
                break;

            case ThroughputClass:
                target = unmarshalThroughputClass(source);
                break;

            case ClosedUserGroupSelection:
                target = unmarshalClosedUserGroupSelection(source);
                break;

            case ChargingInformationRequest:
                target = unmarshalChargingInformationRequest(source);
                break;

            case CalledLineAddressModifiedNotification:
                target = unmarshalCalledLineAddressModifiedNotification(source);
                break;

            case ClosedUserGroupWithOutgoingAccess:
                target = unmarshalClosedUserGroupWithOutgoingAccess(source);
                break;

            case QualityOfServiceNegotiationMinimumThroughputClass:
                target = unmarshalQualityOfServiceNegotiationMinimumThroughputClass(source);
                break;

            case ExpeditedDataNegotiation:
                target = unmarshalExpeditedDataNegotiation(source);
                break;

            // two parameters
            case BilateralClosedUserGroupSelection:
                target = unmarshalBilateralClosedUserGroupSelection(source);
                break;

            case PacketSizeSelection:
                target = unmarshalPacketSizeSelection(source);
                break;

            case WindowSizeSelection:
                target = unmarshalWindowSizeSelection(source);
                break;

            case RecognizedPrivateOperatingAgencySelectionBasicFormat:
                target = unmarshalRecognizedPrivateOperatingAgencySelectionBasicFormat(source);
                break;

            case TransitDelaySelectionAndIndication:
                target = unmarshalTransitDelaySelectionAndIndication(source);
                break;

            // variable parameters
            case ChargingCallDuration:
                target = unmarshalChargingCallDuration(source);
                break;

            case ChargingSegmentCount:
                target = unmarshalChargingSegmentCount(source);
                break;

            case CallRedirectionNotification:
                target = unmarshalCallRedirectionNotification(source);
                break;

            case RecognizedPrivateOperatingAgencySelectionExtendedFormat:
                target = unmarshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(source);
                break;

            case ChargingMonetaryUnit:
                target = unmarshalChargingMonetaryUnit(source);
                break;

            case NetworkUserIdentification:
                target = unmarshalNetworkUserIdentification(source);
                break;

            case CalledAddressExtensionOsi:
                target = unmarshalCalledAddressExtensionOsi(source);
                break;

            case QualityOfServiceNegotiationEndToEndTransitDelay:
                target = unmarshalQualityOfServiceNegotiationEndToEndTransitDelay(source);
                break;

            case CallingAddressExtensionOsi:
                target = unmarshalCallingAddressExtensionOsi(source);
                break;

            default:
                throw new RuntimeException("Unhandled type: " + enumeration);
        }

        return target;
    }

    // one parameter
    private static byte[] marshalReverseChargingAndFastSelect(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ReverseChargingAndFastSelect facility = (ReverseChargingAndFastSelect) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalReverseChargingAndFastSelect(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return ReverseChargingAndFastSelect.create(parameter);
    }

    private static byte[] marshalThroughputClass(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ThroughputClass facility = (ThroughputClass) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalThroughputClass(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return ThroughputClass.create(parameter);
    }

    private static byte[] marshalClosedUserGroupSelection(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ClosedUserGroupSelection facility = (ClosedUserGroupSelection) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalClosedUserGroupSelection(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return ClosedUserGroupSelection.create(parameter);
    }

    private static byte[] marshalChargingInformationRequest(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ChargingInformationRequest facility = (ChargingInformationRequest) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalChargingInformationRequest(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return ChargingInformationRequest.create(parameter);
    }

    private static byte[] marshalCalledLineAddressModifiedNotification(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final CalledLineAddressModifiedNotification facility = (CalledLineAddressModifiedNotification) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalCalledLineAddressModifiedNotification(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return CalledLineAddressModifiedNotification.create(parameter);
    }

    private static byte[] marshalClosedUserGroupWithOutgoingAccess(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ClosedUserGroupWithOutgoingAccess facility = (ClosedUserGroupWithOutgoingAccess) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalClosedUserGroupWithOutgoingAccess(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return ClosedUserGroupWithOutgoingAccess.create(parameter);
    }

    private static byte[] marshalQualityOfServiceNegotiationMinimumThroughputClass(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final QualityOfServiceNegotiationMinimumThroughputClass facility = (QualityOfServiceNegotiationMinimumThroughputClass) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalQualityOfServiceNegotiationMinimumThroughputClass(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return QualityOfServiceNegotiationMinimumThroughputClass.create(parameter);
    }

    private static byte[] marshalExpeditedDataNegotiation(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ExpeditedDataNegotiation facility = (ExpeditedDataNegotiation) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter);

        return target.toByteArray();
    }

    private static Facility unmarshalExpeditedDataNegotiation(InputStream source) throws IOException {

        final int read = source.read();
        final byte parameter = (byte) (read & 0x00ff);
        return ExpeditedDataNegotiation.create(parameter);
    }

    // two parameters
    private static byte[] marshalBilateralClosedUserGroupSelection(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final BilateralClosedUserGroupSelection facility = (BilateralClosedUserGroupSelection) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter0 = facility.getParameter0();
        final byte parameter1 = facility.getParameter1();

        target.write(enumeration.toInt());
        target.write(parameter0);
        target.write(parameter1);

        return target.toByteArray();
    }

    private static Facility unmarshalBilateralClosedUserGroupSelection(InputStream source) throws IOException {

        final byte parameter0 = (byte) (source.read() & 0x00ff);
        final byte parameter1 = (byte) (source.read() & 0x00ff);
        return BilateralClosedUserGroupSelection.create(parameter0, parameter1);
    }

    private static byte[] marshalPacketSizeSelection(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final PacketSizeSelection facility = (PacketSizeSelection) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final PacketSizeSelectionEnumeration transmitPacketSize = facility.getTransmitPacketSize();
        final PacketSizeSelectionEnumeration receivePacketSize = facility.getReceivePacketSize();

        target.write(enumeration.toInt());
        target.write(transmitPacketSize.toInt());
        target.write(receivePacketSize.toInt());

        return target.toByteArray();
    }

    private static Facility unmarshalPacketSizeSelection(InputStream source) throws IOException {

        final PacketSizeSelectionEnumeration transmitPacketSize = PacketSizeSelectionEnumeration.fromInt(source.read() & 0x000f);
        final PacketSizeSelectionEnumeration receivePacketSize = PacketSizeSelectionEnumeration.fromInt(source.read() & 0x000f);

        return PacketSizeSelection.create(transmitPacketSize, receivePacketSize);
    }

    private static byte[] marshalWindowSizeSelection(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final WindowSizeSelection facility = (WindowSizeSelection) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final int transmissionWindowSize = facility.getTransmissionWindowSize();
        final int receiveWindowSize = facility.getReceiveWindowSize();

        target.write(enumeration.toInt());
        target.write(transmissionWindowSize);
        target.write(receiveWindowSize);

        return target.toByteArray();
    }

    private static Facility unmarshalWindowSizeSelection(InputStream source) throws IOException {

        final int transmissionWindowSize = (source.read() & 0x000f);
        final int receiveWindowSize = (source.read() & 0x000f);

        return WindowSizeSelection.create(transmissionWindowSize, receiveWindowSize);
    }

    private static byte[] marshalRecognizedPrivateOperatingAgencySelectionBasicFormat(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final RecognizedPrivateOperatingAgencySelectionBasicFormat facility = (RecognizedPrivateOperatingAgencySelectionBasicFormat) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter0 = facility.getParameter0();
        final byte parameter1 = facility.getParameter1();

        target.write(enumeration.toInt());
        target.write(parameter0);
        target.write(parameter1);

        return target.toByteArray();
    }

    private static Facility unmarshalRecognizedPrivateOperatingAgencySelectionBasicFormat(InputStream source) throws IOException {

        final byte parameter0 = (byte) (source.read() & 0x00ff);
        final byte parameter1 = (byte) (source.read() & 0x00ff);
        return RecognizedPrivateOperatingAgencySelectionBasicFormat.create(parameter0, parameter1);
    }

    private static byte[] marshalTransitDelaySelectionAndIndication(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final TransitDelaySelectionAndIndication facility = (TransitDelaySelectionAndIndication) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte parameter0 = facility.getParameter0();
        final byte parameter1 = facility.getParameter1();

        target.write(enumeration.toInt());
        target.write(parameter0);
        target.write(parameter1);

        return target.toByteArray();
    }

    private static Facility unmarshalTransitDelaySelectionAndIndication(InputStream source) throws IOException {

        final byte parameter0 = (byte) (source.read() & 0x00ff);
        final byte parameter1 = (byte) (source.read() & 0x00ff);
        return TransitDelaySelectionAndIndication.create(parameter0, parameter1);
    }

    // variable parameters
    private static byte[] marshalChargingCallDuration(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ChargingCallDuration facility = (ChargingCallDuration) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalChargingCallDuration(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return ChargingCallDuration.create(parameter);
    }

    private static byte[] marshalChargingSegmentCount(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ChargingSegmentCount facility = (ChargingSegmentCount) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalChargingSegmentCount(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return ChargingSegmentCount.create(parameter);
    }

    private static byte[] marshalCallRedirectionNotification(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final CallRedirectionNotification facility = (CallRedirectionNotification) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalCallRedirectionNotification(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return CallRedirectionNotification.create(parameter);
    }

    private static byte[] marshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final RecognizedPrivateOperatingAgencySelectionExtendedFormat facility = (RecognizedPrivateOperatingAgencySelectionExtendedFormat) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalRecognizedPrivateOperatingAgencySelectionExtendedFormat(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return RecognizedPrivateOperatingAgencySelectionExtendedFormat.create(parameter);
    }

    private static byte[] marshalChargingMonetaryUnit(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final ChargingMonetaryUnit facility = (ChargingMonetaryUnit) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalChargingMonetaryUnit(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return ChargingMonetaryUnit.create(parameter);
    }

    private static byte[] marshalNetworkUserIdentification(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final NetworkUserIdentification facility = (NetworkUserIdentification) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalNetworkUserIdentification(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return NetworkUserIdentification.create(parameter);
    }

    private static byte[] marshalCalledAddressExtensionOsi(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final CalledAddressExtensionOsi facility = (CalledAddressExtensionOsi) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalCalledAddressExtensionOsi(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return CalledAddressExtensionOsi.create(parameter);
    }

    private static byte[] marshalQualityOfServiceNegotiationEndToEndTransitDelay(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final QualityOfServiceNegotiationEndToEndTransitDelay facility = (QualityOfServiceNegotiationEndToEndTransitDelay) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalQualityOfServiceNegotiationEndToEndTransitDelay(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return QualityOfServiceNegotiationEndToEndTransitDelay.create(parameter);
    }

    private static byte[] marshalCallingAddressExtensionOsi(Facility source) {

        final ByteArrayOutputStream target = new ByteArrayOutputStream();
        final CallingAddressExtensionOsi facility = (CallingAddressExtensionOsi) source;
        final FacilityEnumeration enumeration = facility.getEnumeration();
        final byte[] parameter = facility.getParameter();

        target.write(enumeration.toInt());
        target.write(parameter.length);
        target.write(parameter, 0, parameter.length);

        return target.toByteArray();
    }

    private static Facility unmarshalCallingAddressExtensionOsi(InputStream source) throws IOException {

        final int length = source.read();
        final byte[] parameter = new byte[Math.max(0, length)];

        source.read(parameter, 0, parameter.length);

        return CallingAddressExtensionOsi.create(parameter);
    }
}
