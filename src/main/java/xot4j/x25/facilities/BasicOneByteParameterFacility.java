//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

/**
 *
 * @todo improve parameter abstraction in all subclasses.
 *
 * This class is only a gap filler to speed up initial development.
 * It should be removed one day.
 */
class BasicOneByteParameterFacility extends BaseFacility {

    private final byte parameter;

    protected BasicOneByteParameterFacility(FacilityEnumeration enumeration, byte parameter) {

        super(enumeration);
        this.parameter = parameter;
    }

    public byte getParameter() {

        return parameter;
    }
}
