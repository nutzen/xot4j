//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

/**
 *
 * @todo improve parameter abstraction in all subclasses.
 *
 * This class is only a gap filler to speed up initial development.
 * It should be removed one day.
 */
class BasicTwoByteParameterFacility extends BaseFacility {

    private final byte parameter0;
    private final byte parameter1;

    protected BasicTwoByteParameterFacility(FacilityEnumeration enumeration, byte parameter0, byte parameter1) {

        super(enumeration);
        this.parameter0 = parameter0;
        this.parameter1 = parameter1;
    }

    public byte getParameter0() {

        return parameter0;
    }

    public byte getParameter1() {

        return parameter1;
    }
}
