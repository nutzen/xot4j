//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import java.text.MessageFormat;

public final class WindowSizeSelection extends BaseFacility {

    private static final FacilityEnumeration ENUMERATION = FacilityEnumeration.WindowSizeSelection;
    private final int transmissionWindowSize;
    private final int receiveWindowSize;

    private WindowSizeSelection(int transmissionWindowSize,
            int receiveWindowSize) {

        super(ENUMERATION);
        this.transmissionWindowSize = transmissionWindowSize;
        this.receiveWindowSize = receiveWindowSize;
    }

    public static WindowSizeSelection create(int transmissionWindowSize,
            int receiveWindowSize) {

        final int max = 0x07;
        final int min = 0x01;

        if ((max < transmissionWindowSize) || (min > transmissionWindowSize)) {
            final String pattern = "Expecting a value between {0} and {1} for {2}, but got {3} instead.";
            final String message = MessageFormat.format(pattern, min, max, "transmissionWindowSize", transmissionWindowSize);

            throw new RuntimeException(message);
        }

        if ((max < receiveWindowSize) || (min > receiveWindowSize)) {
            final String pattern = "Expecting a value between {0} and {1} for {2}, but got {3} instead.";
            final String message = MessageFormat.format(pattern, min, max, "receiveWindowSize", receiveWindowSize);

            throw new RuntimeException(message);
        }

        return new WindowSizeSelection(transmissionWindowSize, receiveWindowSize);
    }

    public int getTransmissionWindowSize() {

        return transmissionWindowSize;
    }

    public int getReceiveWindowSize() {

        return receiveWindowSize;
    }
}
