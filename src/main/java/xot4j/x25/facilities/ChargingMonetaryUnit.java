//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

public final class ChargingMonetaryUnit extends BasicVariableLengthParameterFacility {

    private static final FacilityEnumeration ENUMERATION = FacilityEnumeration.ChargingMonetaryUnit;

    private ChargingMonetaryUnit(byte[] parameter) {

        super(ENUMERATION, parameter);
    }

    public static ChargingMonetaryUnit create(byte[] parameter) {

        return new ChargingMonetaryUnit(parameter);
    }
}
