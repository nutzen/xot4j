//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import static xot4j.x25.facilities.PacketSizeSelectionEnumerationUtility.*;

public enum PacketSizeSelectionEnumeration {

    Size16octets,
    Size32octets,
    Size64octets,
    Size128octets,
    Size256octets,
    Size512octets,
    Size1024octets,
    Size2048octets,
    Size4096octets;

    public String toDescription() {

        switch (this) {
            case Size16octets:
                return SIZE_16_OCTETS_DESCRIPTION;

            case Size32octets:
                return SIZE_32_OCTETS_DESCRIPTION;

            case Size64octets:
                return SIZE_64_OCTETS_DESCRIPTION;

            case Size128octets:
                return SIZE_128_OCTETS_DESCRIPTION;

            case Size256octets:
                return SIZE_256_OCTETS_DESCRIPTION;

            case Size512octets:
                return SIZE_512_OCTETS_DESCRIPTION;

            case Size1024octets:
                return SIZE_1024_OCTETS_DESCRIPTION;

            case Size2048octets:
                return SIZE_2048_OCTETS_DESCRIPTION;

            case Size4096octets:
                return SIZE_4096_OCTETS_DESCRIPTION;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }
    }

    public static PacketSizeSelectionEnumeration fromDescription(final String value) {
        switch (value) {
            case SIZE_16_OCTETS_DESCRIPTION:
                return Size16octets;

            case SIZE_32_OCTETS_DESCRIPTION:
                return Size32octets;

            case SIZE_64_OCTETS_DESCRIPTION:
                return Size64octets;

            case SIZE_128_OCTETS_DESCRIPTION:
                return Size128octets;

            case SIZE_256_OCTETS_DESCRIPTION:
                return Size256octets;

            case SIZE_512_OCTETS_DESCRIPTION:
                return Size512octets;

            case SIZE_1024_OCTETS_DESCRIPTION:
                return Size1024octets;

            case SIZE_2048_OCTETS_DESCRIPTION:
                return Size2048octets;

            case SIZE_4096_OCTETS_DESCRIPTION:
                return Size4096octets;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }
    }

    public int toInt() {

        int destination;

        switch (this) {
            case Size16octets:
                destination = SIZE_16_OCTETS_MASK;
                break;

            case Size32octets:
                destination = SIZE_32_OCTETS_MASK;
                break;

            case Size64octets:
                destination = SIZE_64_OCTETS_MASK;
                break;

            case Size128octets:
                destination = SIZE_128_OCTETS_MASK;
                break;

            case Size256octets:
                destination = SIZE_256_OCTETS_MASK;
                break;

            case Size512octets:
                destination = SIZE_512_OCTETS_MASK;
                break;

            case Size1024octets:
                destination = SIZE_1024_OCTETS_MASK;
                break;

            case Size2048octets:
                destination = SIZE_2048_OCTETS_MASK;
                break;

            case Size4096octets:
                destination = SIZE_4096_OCTETS_MASK;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static PacketSizeSelectionEnumeration fromInt(final int value) {

        final PacketSizeSelectionEnumeration destination;

        if (compareWithMask(SIZE_16_OCTETS_MASK, value)) {
            destination = Size16octets;

        } else if (compareWithMask(SIZE_32_OCTETS_MASK, value)) {
            destination = Size32octets;

        } else if (compareWithMask(SIZE_64_OCTETS_MASK, value)) {
            destination = Size64octets;

        } else if (compareWithMask(SIZE_128_OCTETS_MASK, value)) {
            destination = Size128octets;

        } else if (compareWithMask(SIZE_256_OCTETS_MASK, value)) {
            destination = Size256octets;

        } else if (compareWithMask(SIZE_512_OCTETS_MASK, value)) {
            destination = Size512octets;

        } else if (compareWithMask(SIZE_1024_OCTETS_MASK, value)) {
            destination = Size1024octets;

        } else if (compareWithMask(SIZE_2048_OCTETS_MASK, value)) {
            destination = Size2048octets;

        } else if (compareWithMask(SIZE_4096_OCTETS_MASK, value)) {
            destination = Size4096octets;

        } else {
            throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }

    public int toPacketSize() {

        int destination;

        switch (this) {
            case Size16octets:
                destination = 16;
                break;

            case Size32octets:
                destination = 32;
                break;

            case Size64octets:
                destination = 64;
                break;

            case Size128octets:
                destination = 128;
                break;

            case Size256octets:
                destination = 256;
                break;

            case Size512octets:
                destination = 512;
                break;

            case Size1024octets:
                destination = 1024;
                break;

            case Size2048octets:
                destination = 2048;
                break;

            case Size4096octets:
                destination = 4096;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static PacketSizeSelectionEnumeration fromPacketSize(int value) {

        PacketSizeSelectionEnumeration destination;

        switch (value) {
            case 16:
                destination = Size16octets;
                break;

            case 32:
                destination = Size32octets;
                break;

            case 64:
                destination = Size64octets;
                break;

            case 128:
                destination = Size128octets;
                break;

            case 256:
                destination = Size256octets;
                break;

            case 512:
                destination = Size512octets;
                break;

            case 1024:
                destination = Size1024octets;
                break;

            case 2048:
                destination = Size2048octets;
                break;

            case 4096:
                destination = Size4096octets;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }
}
