//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

public final class PacketSizeSelectionEnumerationUtility {

    // DESCRIPTION
    public static final String SIZE_16_OCTETS_DESCRIPTION = "16_octets";
    public static final String SIZE_32_OCTETS_DESCRIPTION = "32_octets";
    public static final String SIZE_64_OCTETS_DESCRIPTION = "64_octets";
    public static final String SIZE_128_OCTETS_DESCRIPTION = "128_octets";
    public static final String SIZE_256_OCTETS_DESCRIPTION = "256_octets";
    public static final String SIZE_512_OCTETS_DESCRIPTION = "512_octets";
    public static final String SIZE_1024_OCTETS_DESCRIPTION = "1024_octets";
    public static final String SIZE_2048_OCTETS_DESCRIPTION = "2048_octets";
    public static final String SIZE_4096_OCTETS_DESCRIPTION = "4096_octets";
    // MASK
    public static final int SIZE_16_OCTETS_MASK = 0x04;
    public static final int SIZE_32_OCTETS_MASK = 0x05;
    public static final int SIZE_64_OCTETS_MASK = 0x06;
    public static final int SIZE_128_OCTETS_MASK = 0x07;
    public static final int SIZE_256_OCTETS_MASK = 0x08;
    public static final int SIZE_512_OCTETS_MASK = 0x09;
    public static final int SIZE_1024_OCTETS_MASK = 0x0a;
    public static final int SIZE_2048_OCTETS_MASK = 0x0b;
    public static final int SIZE_4096_OCTETS_MASK = 0x0c;

    static boolean compareWithMask(final int mask, final int value) {

        return (mask == value);
    }

    private PacketSizeSelectionEnumerationUtility() {
    }
}
