//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

public final class BilateralClosedUserGroupSelection extends BasicTwoByteParameterFacility {

    private static final FacilityEnumeration ENUMERATION = FacilityEnumeration.BilateralClosedUserGroupSelection;

    private BilateralClosedUserGroupSelection(byte parameter0, byte parameter1) {

        super(ENUMERATION, parameter0, parameter1);
    }

    public static BilateralClosedUserGroupSelection create(byte parameter0, byte parameter1) {

        return new BilateralClosedUserGroupSelection(parameter0, parameter1);
    }
}
