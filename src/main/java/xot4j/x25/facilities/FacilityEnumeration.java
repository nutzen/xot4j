//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.facilities;

import static xot4j.x25.facilities.FacilityEnumerationUtility.*;

public enum FacilityEnumeration {

    // one parameter
    ReverseChargingAndFastSelect,
    ThroughputClass,
    ClosedUserGroupSelection,
    ChargingInformationRequest,
    CalledLineAddressModifiedNotification,
    ClosedUserGroupWithOutgoingAccess,
    QualityOfServiceNegotiationMinimumThroughputClass,
    ExpeditedDataNegotiation,
    // two parameters
    BilateralClosedUserGroupSelection,
    PacketSizeSelection,
    WindowSizeSelection,
    RecognizedPrivateOperatingAgencySelectionBasicFormat,
    TransitDelaySelectionAndIndication,
    // variable parameters
    ChargingCallDuration,
    ChargingSegmentCount,
    CallRedirectionNotification,
    RecognizedPrivateOperatingAgencySelectionExtendedFormat,
    ChargingMonetaryUnit,
    NetworkUserIdentification,
    CalledAddressExtensionOsi,
    QualityOfServiceNegotiationEndToEndTransitDelay,
    CallingAddressExtensionOsi;

    public String toDescription() {

        switch (this) {
            case ReverseChargingAndFastSelect:
                return REVERSE_CHARGING_AND_FAST_SELECT_DESCRIPTION;

            case ThroughputClass:
                return THROUGHPUT_CLASS_DESCRIPTION;

            case ClosedUserGroupSelection:
                return CLOSED_USER_GROUP_SELECTION_DESCRIPTION;

            case ChargingInformationRequest:
                return CHARGING_INFORMATION_REQUEST_DESCRIPTION;

            case CalledLineAddressModifiedNotification:
                return CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_DESCRIPTION;

            case ClosedUserGroupWithOutgoingAccess:
                return CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_DESCRIPTION;

            case QualityOfServiceNegotiationMinimumThroughputClass:
                return QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_DESCRIPTION;

            case ExpeditedDataNegotiation:
                return EXPEDITED_DATA_NEGOTIATION_DESCRIPTION;

            case BilateralClosedUserGroupSelection:
                return BILATERAL_CLOSED_USER_GROUP_SELECTION_DESCRIPTION;

            case PacketSizeSelection:
                return PACKET_SIZE_SELECTION_DESCRIPTION;

            case WindowSizeSelection:
                return WINDOW_SIZE_SELECTION_DESCRIPTION;

            case RecognizedPrivateOperatingAgencySelectionBasicFormat:
                return RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_DESCRIPTION;

            case TransitDelaySelectionAndIndication:
                return TRANSIT_DELAY_SELECTION_AND_INDICATION_DESCRIPTION;

            case ChargingCallDuration:
                return CHARGING_CALL_DURATION_DESCRIPTION;

            case ChargingSegmentCount:
                return CHARGING_SEGMENT_COUNT_DESCRIPTION;

            case CallRedirectionNotification:
                return CALL_REDIRECTION_NOTIFICATION_DESCRIPTION;

            case RecognizedPrivateOperatingAgencySelectionExtendedFormat:
                return RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_DESCRIPTION;

            case ChargingMonetaryUnit:
                return CHARGING_MONETARY_UNIT_DESCRIPTION;

            case NetworkUserIdentification:
                return NETWORK_USER_IDENTIFICATION_DESCRIPTION;

            case CalledAddressExtensionOsi:
                return CALLED_ADDRESS_EXTENSION_OSI_DESCRIPTION;

            case QualityOfServiceNegotiationEndToEndTransitDelay:
                return QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_DESCRIPTION;

            case CallingAddressExtensionOsi:
                return CALLING_ADDRESS_EXTENTION_OSI_DESCRIPTION;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }
    }

    public static FacilityEnumeration fromDescription(final String value) {

        switch (value) {
            case REVERSE_CHARGING_AND_FAST_SELECT_DESCRIPTION:
                return ReverseChargingAndFastSelect;

            case THROUGHPUT_CLASS_DESCRIPTION:
                return ThroughputClass;

            case CLOSED_USER_GROUP_SELECTION_DESCRIPTION:
                return ClosedUserGroupSelection;

            case CHARGING_INFORMATION_REQUEST_DESCRIPTION:
                return ChargingInformationRequest;

            case CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_DESCRIPTION:
                return CalledLineAddressModifiedNotification;

            case CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_DESCRIPTION:
                return ClosedUserGroupWithOutgoingAccess;

            case QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_DESCRIPTION:
                return QualityOfServiceNegotiationMinimumThroughputClass;

            case EXPEDITED_DATA_NEGOTIATION_DESCRIPTION:
                return ExpeditedDataNegotiation;

            case BILATERAL_CLOSED_USER_GROUP_SELECTION_DESCRIPTION:
                return BilateralClosedUserGroupSelection;

            case PACKET_SIZE_SELECTION_DESCRIPTION:
                return PacketSizeSelection;

            case WINDOW_SIZE_SELECTION_DESCRIPTION:
                return WindowSizeSelection;

            case RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_DESCRIPTION:
                return RecognizedPrivateOperatingAgencySelectionBasicFormat;

            case TRANSIT_DELAY_SELECTION_AND_INDICATION_DESCRIPTION:
                return TransitDelaySelectionAndIndication;

            case CHARGING_CALL_DURATION_DESCRIPTION:
                return ChargingCallDuration;

            case CHARGING_SEGMENT_COUNT_DESCRIPTION:
                return ChargingSegmentCount;

            case CALL_REDIRECTION_NOTIFICATION_DESCRIPTION:
                return CallRedirectionNotification;

            case RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_DESCRIPTION:
                return RecognizedPrivateOperatingAgencySelectionExtendedFormat;

            case CHARGING_MONETARY_UNIT_DESCRIPTION:
                return ChargingMonetaryUnit;

            case NETWORK_USER_IDENTIFICATION_DESCRIPTION:
                return NetworkUserIdentification;

            case CALLED_ADDRESS_EXTENSION_OSI_DESCRIPTION:
                return CalledAddressExtensionOsi;

            case QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_DESCRIPTION:
                return QualityOfServiceNegotiationEndToEndTransitDelay;

            case CALLING_ADDRESS_EXTENTION_OSI_DESCRIPTION:
                return CallingAddressExtensionOsi;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }
    }

    public int toInt() {

        switch (this) {
            case ReverseChargingAndFastSelect:
                return REVERSE_CHARGING_AND_FAST_SELECT_MASK;

            case ThroughputClass:
                return THROUGHPUT_CLASS_MASK;

            case ClosedUserGroupSelection:
                return CLOSED_USER_GROUP_SELECTION_MASK;

            case ChargingInformationRequest:
                return CHARGING_INFORMATION_REQUEST_MASK;

            case CalledLineAddressModifiedNotification:
                return CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_MASK;

            case ClosedUserGroupWithOutgoingAccess:
                return CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_MASK;

            case QualityOfServiceNegotiationMinimumThroughputClass:
                return QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_MASK;

            case ExpeditedDataNegotiation:
                return EXPEDITED_DATA_NEGOTIATION_MASK;

            case BilateralClosedUserGroupSelection:
                return BILATERAL_CLOSED_USER_GROUP_SELECTION_MASK;

            case PacketSizeSelection:
                return PACKET_SIZE_SELECTION_MASK;

            case WindowSizeSelection:
                return WINDOW_SIZE_SELECTION_MASK;

            case RecognizedPrivateOperatingAgencySelectionBasicFormat:
                return RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_MASK;

            case TransitDelaySelectionAndIndication:
                return TRANSIT_DELAY_SELECTION_AND_INDICATION_MASK;

            case ChargingCallDuration:
                return CHARGING_CALL_DURATION_MASK;

            case ChargingSegmentCount:
                return CHARGING_SEGMENT_COUNT_MASK;

            case CallRedirectionNotification:
                return CALL_REDIRECTION_NOTIFICATION_MASK;

            case RecognizedPrivateOperatingAgencySelectionExtendedFormat:
                return RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_MASK;

            case ChargingMonetaryUnit:
                return CHARGING_MONETARY_UNIT_MASK;

            case NetworkUserIdentification:
                return NETWORK_USER_IDENTIFICATION_MASK;

            case CalledAddressExtensionOsi:
                return CALLED_ADDRESS_EXTENSION_OSI_MASK;

            case QualityOfServiceNegotiationEndToEndTransitDelay:
                return QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_MASK;

            case CallingAddressExtensionOsi:
                return CALLING_ADDRESS_EXTENTION_OSI_MASK;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }
    }

    public static FacilityEnumeration fromInt(final int value) {

        if (compareWithMask(REVERSE_CHARGING_AND_FAST_SELECT_MASK, value)) {
            return ReverseChargingAndFastSelect;

        } else if (compareWithMask(THROUGHPUT_CLASS_MASK, value)) {
            return ThroughputClass;

        } else if (compareWithMask(CLOSED_USER_GROUP_SELECTION_MASK, value)) {
            return ClosedUserGroupSelection;

        } else if (compareWithMask(CHARGING_INFORMATION_REQUEST_MASK, value)) {
            return ChargingInformationRequest;

        } else if (compareWithMask(CALLED_LINE_ADDRESS_MODIFIED_NOTIFICATION_MASK, value)) {
            return CalledLineAddressModifiedNotification;

        } else if (compareWithMask(CLOSED_USER_GROUP_WITH_OUTGOING_ACCESS_MASK, value)) {
            return ClosedUserGroupWithOutgoingAccess;

        } else if (compareWithMask(QUALITY_OF_SERVICE_NEGOTIATION_MINIMUM_THROUGHPUT_CLASS_MASK, value)) {
            return QualityOfServiceNegotiationMinimumThroughputClass;

        } else if (compareWithMask(EXPEDITED_DATA_NEGOTIATION_MASK, value)) {
            return ExpeditedDataNegotiation;

        } else if (compareWithMask(BILATERAL_CLOSED_USER_GROUP_SELECTION_MASK, value)) {
            return BilateralClosedUserGroupSelection;

        } else if (compareWithMask(PACKET_SIZE_SELECTION_MASK, value)) {
            return PacketSizeSelection;

        } else if (compareWithMask(WINDOW_SIZE_SELECTION_MASK, value)) {
            return WindowSizeSelection;

        } else if (compareWithMask(RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_BASIC_FORMAT_MASK, value)) {
            return RecognizedPrivateOperatingAgencySelectionBasicFormat;

        } else if (compareWithMask(TRANSIT_DELAY_SELECTION_AND_INDICATION_MASK, value)) {
            return TransitDelaySelectionAndIndication;

        } else if (compareWithMask(CHARGING_CALL_DURATION_MASK, value)) {
            return ChargingCallDuration;

        } else if (compareWithMask(CHARGING_SEGMENT_COUNT_MASK, value)) {
            return ChargingSegmentCount;

        } else if (compareWithMask(CALL_REDIRECTION_NOTIFICATION_MASK, value)) {
            return CallRedirectionNotification;

        } else if (compareWithMask(RECOGNIZED_PRIVATE_OPERATING_AGENCY_SELECTION_EXTENDED_FORMAT_MASK, value)) {
            return RecognizedPrivateOperatingAgencySelectionExtendedFormat;

        } else if (compareWithMask(CHARGING_MONETARY_UNIT_MASK, value)) {
            return ChargingMonetaryUnit;

        } else if (compareWithMask(NETWORK_USER_IDENTIFICATION_MASK, value)) {
            return NetworkUserIdentification;

        } else if (compareWithMask(CALLED_ADDRESS_EXTENSION_OSI_MASK, value)) {
            return CalledAddressExtensionOsi;

        } else if (compareWithMask(QUALTIY_OF_SERVICE_NEGOTIATION_END_TO_END_TRANSIT_DELAY_MASK, value)) {
            return QualityOfServiceNegotiationEndToEndTransitDelay;

        } else if (compareWithMask(CALLING_ADDRESS_EXTENTION_OSI_MASK, value)) {
            return CallingAddressExtensionOsi;

        } else {
            throw new RuntimeException("Unhandled value: " + value);
        }
    }
}
