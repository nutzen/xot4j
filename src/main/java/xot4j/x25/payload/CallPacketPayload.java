//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

import java.util.Collection;
import xot4j.x121.X121Address;
import xot4j.x25.facilities.Facility;

/**
 *
 *       .====================================================.
 * O  1  ||  0    D  ||   X   X   ||        L C G N          || C
 * C     |====================================================| A
 * T  1  ||                    L  C  N                       || L
 * E     |====================================================| L
 * T  1  ||                    P  T  I                       ||
 * S     |====================================================| R
 *    1  ||  CALLED ADDR LENGTH   ||   CALLING ADDR LENGTH   || E
 *       |====================================================| Q
 *    8  ||           C A L L E D    A D D R E S S           || U
 *       |====================================================| E
 *    8  ||          C A L L I N G    A D D R E S S          || S
 *       |====================================================| T
 *    1  ||       F A C I L I T I E S    L E N G T H         ||
 *       |====================================================| P
 *    N  ||          F  A  C  I  L  I  T  I  E  S            || K
 *       |====================================================| T
 *    16 ||        C A L L    U S E R    D A T A             ||
 *       `===================================================='
 * 
 */
public final class CallPacketPayload implements X25Payload {

    private final X121Address calledAddress /* optional for Call_Connected / Call_Accept */;
    private final X121Address callingAddress /* optional for Call_Connected / Call_Accept */;
    private final Collection<Facility> facilities /* optional for Call_Connected / Call_Accept */;
    private final byte[] userData /* optional */;

    CallPacketPayload(X121Address calledAddress,
            X121Address callingAddress,
            Collection<Facility> facilities,
            byte[] userData) {

        this.calledAddress = calledAddress;
        this.callingAddress = callingAddress;
        this.facilities = facilities;
        this.userData = userData;
    }

    public static CallPacketPayload create() {

        return create(null, null, null, null);
    }

    public static CallPacketPayload create(X121Address calledAddress,
            X121Address callingAddress,
            Collection<Facility> facilities,
            byte[] userData) {

        return new CallPacketPayload(calledAddress,
                callingAddress,
                facilities,
                userData);
    }

    public X121Address getCalledAddress() {

        return calledAddress;
    }

    public X121Address getCallingAddress() {

        return callingAddress;
    }

    public Collection<Facility> getFacilities() {

        return facilities;
    }

    public byte[] getUserData() {

        return userData;
    }
}
