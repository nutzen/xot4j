//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.codes.RestartRequestEnumeration;

/**
 *
 *        .====================================================.
 * O   1  ||  0    0  ||   X   X   ||        L C G N          || R
 * C      |====================================================| E
 * T   1  ||                    L  C  N                       || S
 * E      |====================================================| T
 * T   1  ||                    P  T  I                       || A
 * S      |====================================================| R
 *     1  ||         R E S T A R T   C A U S E                || T
 *        |====================================================|
 *     1  ||       C L E A R I N G   D I A G N O S T I C      ||
 *        `===================================================='
 *
 */
public final class RestartPacketPayload implements X25Payload {

    private final RestartRequestEnumeration cause;
    private final DiagnosticCodeEnumeration diagnostic;

    RestartPacketPayload(RestartRequestEnumeration cause,
            DiagnosticCodeEnumeration diagnostic) {

        this.cause = cause;
        this.diagnostic = diagnostic;
    }

    public static RestartPacketPayload create(RestartRequestEnumeration cause,
            DiagnosticCodeEnumeration diagnostic) {

        return new RestartPacketPayload(cause, diagnostic);
    }

    public RestartRequestEnumeration getCause() {

        return cause;
    }

    public DiagnosticCodeEnumeration getDiagnostic() {

        return diagnostic;
    }
}
