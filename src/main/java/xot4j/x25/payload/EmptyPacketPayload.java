//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

/**
 *
 *       .====================================================.
 * O  1  ||  0    D  ||   X   X   ||        L C G N          || 
 * C     |====================================================| 
 * T  1  ||                    L  C  N                       || 
 * E     |====================================================| 
 * T  1  ||                    P  T  I                       ||
 *       `===================================================='
 *
 */
public final class EmptyPacketPayload implements X25Payload {

    EmptyPacketPayload() {
    }

    public static EmptyPacketPayload create() {

        return new EmptyPacketPayload();
    }
}
