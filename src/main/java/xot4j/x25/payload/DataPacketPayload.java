//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

/**
 *
 *       .====================================================.
 * O  1  ||  0    D  ||   X   X   ||        L C G N          || D
 * C     |====================================================| A
 * T  1  ||                    L  C  N                       || T
 * E     |====================================================| A
 * T  1  ||                    P  T  I                       ||
 * S     |====================================================| 
 *    N  ||               U S E R    D A T A                 ||
 *       `===================================================='
 *
 */
public final class DataPacketPayload implements X25Payload {

    private final byte[] userData;

    DataPacketPayload(byte[] userData) {
        this.userData = userData;
    }

    public static DataPacketPayload create(byte[] userData) {

        return new DataPacketPayload(userData);
    }

    public byte[] getUserData() {

        return userData;
    }
}
