//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

import java.util.Collection;
import xot4j.x121.X121Address;
import xot4j.x25.facilities.Facility;

/**
 *
 *        .====================================================.
 * O   1  ||  0    0  ||   X   X   ||        L C G N          || C
 * C      |====================================================| L
 * T   1  ||                    L  C  N                       || E
 * E      |====================================================| A
 * T   1  ||                    P  T  I                       || R
 * S      |====================================================|
 *     17 ||   ADDRESSING IN CALL REQUEST FORMAT IF USED      || C
 *        |====================================================| O
 *     N  ||   FACILITIES IN CALL REQUEST FORMAT IF USED      || N
 *        `====================================================' 
 *
 */
public final class ClearConfirmationPayload implements X25Payload {

    private final X121Address calledAddress /* optional */;
    private final X121Address callingAddress /* optional */;
    private final Collection<Facility> facilities /* optional */;

    ClearConfirmationPayload(X121Address calledAddress,
            X121Address callingAddress,
            Collection<Facility> facilities) {

        this.calledAddress = calledAddress;
        this.callingAddress = callingAddress;
        this.facilities = facilities;
    }

    public static ClearConfirmationPayload create(X121Address calledAddress,
            X121Address callingAddress,
            Collection<Facility> facilities) {

        return new ClearConfirmationPayload(
                calledAddress,
                callingAddress,
                facilities);
    }

    public static ClearConfirmationPayload create() {

        return create(null /* calledAddress */,
                null /* callingAddress */,
                null /* facilities */);
    }

    public X121Address getCalledAddress() {

        return calledAddress;
    }

    public X121Address getCallingAddress() {

        return callingAddress;
    }

    public Collection<Facility> getFacilities() {

        return facilities;
    }
}
