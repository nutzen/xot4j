//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import xot4j.x121.X121Address;
import xot4j.x121.X121AddressMarshaller;
import xot4j.x121.X121AddressPair;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.codes.ResetRequestEnumeration;
import xot4j.x25.codes.RestartRequestEnumeration;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.FacilityMarshaller;

public final class X25PayloadMarshaller {

    private X25PayloadMarshaller() {
    }

    public static void marshalCallPacketPayload(final OutputStream outputStream, final X25Payload source, final boolean aBit) throws IOException {

        final CallPacketPayload payload = (CallPacketPayload) source;
        final X121Address calledAddress = payload.getCalledAddress();
        final X121Address callingAddress = payload.getCallingAddress();
        final Collection<Facility> facilities = payload.getFacilities();
        final byte[] userData = payload.getUserData();

        if ((null != calledAddress) && (null != callingAddress)) {

            X121AddressMarshaller.marshal(outputStream, calledAddress, callingAddress, aBit);

            if (null != facilities) {

                final byte[] facilitiesBlock = FacilityMarshaller.marshal(facilities);
                outputStream.write(facilitiesBlock.length);
                outputStream.write(facilitiesBlock);

                if ((null != userData) && (0 < userData.length)) {

                    outputStream.write(userData);
                }
            }
        }
    }

    public static X25Payload unmarshalCallPacketPayload(final InputStream inputStream, final boolean aBit) throws IOException {

        final X121Address calledAddress;
        final X121Address callingAddress;
        final Collection<Facility> facilities;
        final byte[] userData;

        if (0 < inputStream.available()) {
            {
                final X121AddressPair addressPair = X121AddressMarshaller.unmarshal(inputStream, aBit);
                calledAddress = addressPair.calledAddress;
                callingAddress = addressPair.callingAddress;
            }

            if (0 < inputStream.available()) {

                final int octetLengthOfFacilities = inputStream.read() & 0x00ff;
                final byte[] facilitiesBlock = new byte[octetLengthOfFacilities];
                inputStream.read(facilitiesBlock, 0, facilitiesBlock.length);
                facilities = FacilityMarshaller.unmarshal(facilitiesBlock);

                if (0 < inputStream.available()) {

                    final int octetLengthOfUserData = inputStream.available();
                    userData = new byte[octetLengthOfUserData];
                    inputStream.read(userData, 0, userData.length);

                } else {
                    userData = null;
                }

            } else {
                facilities = null;
                userData = null;
            }

        } else {
            calledAddress = null;
            callingAddress = null;
            facilities = null;
            userData = null;
        }

        return CallPacketPayload.create(calledAddress, callingAddress, facilities, userData);
    }

    public static void marshalClearPacketPayload(final OutputStream outputStream, final X25Payload source, final boolean aBit) throws IOException {

        final ClearPacketPayload payload = (ClearPacketPayload) source;
        final ClearRequestEnumeration cause = payload.getCause();
        final DiagnosticCodeEnumeration diagnostic = payload.getDiagnostic();
        final X121Address calledAddress = payload.getCalledAddress();
        final X121Address callingAddress = payload.getCallingAddress();
        final Collection<Facility> facilities = payload.getFacilities();
        final byte[] userData = payload.getUserData();

        outputStream.write(0x00ff & cause.toInt());
        outputStream.write(0x00ff & diagnostic.toInt());

        if ((null != calledAddress) && (null != callingAddress)) {
            X121AddressMarshaller.marshal(outputStream, calledAddress, callingAddress, aBit);

            if ((null != facilities) && (0 < facilities.size())) {
                final byte[] facilitiesBlock = FacilityMarshaller.marshal(facilities);
                outputStream.write(facilitiesBlock.length);
                outputStream.write(facilitiesBlock);

                if ((null != userData) && (0 < userData.length)) {
                    outputStream.write(userData);
                }
            }
        }
    }

    public static X25Payload unmarshalClearPacketPayload(final InputStream inputStream, final boolean aBit) throws IOException {

        final ClearRequestEnumeration cause = ClearRequestEnumeration.fromInt(0x00ff & inputStream.read());
        final DiagnosticCodeEnumeration diagnostic;

        final X121Address calledAddress;
        final X121Address callingAddress;
        final Collection<Facility> facilities;
        final byte[] userData;

        if (0 < inputStream.available()) {
            diagnostic = DiagnosticCodeEnumeration.fromInt(0x00ff & inputStream.read());
        } else {
            diagnostic = DiagnosticCodeEnumeration.NoAdditionalInformation;
        }

        if (0 < inputStream.available()) {

            {
                final X121AddressPair addressPair = X121AddressMarshaller.unmarshal(inputStream, aBit);
                calledAddress = addressPair.calledAddress;
                callingAddress = addressPair.callingAddress;
            }

            if (0 < inputStream.available()) {

                final int octetLengthOfFacilities = 0x00ff & inputStream.read();
                final byte[] facilitiesBlock = new byte[octetLengthOfFacilities];
                inputStream.read(facilitiesBlock, 0, facilitiesBlock.length);
                facilities = FacilityMarshaller.unmarshal(facilitiesBlock);

                if (0 < inputStream.available()) {

                    final int octetLengthOfUserData = inputStream.available();
                    userData = new byte[octetLengthOfUserData];
                    inputStream.read(userData, 0, userData.length);

                } else {
                    userData = null;
                }

            } else {
                facilities = null;
                userData = null;
            }

        } else {
            calledAddress = null;
            callingAddress = null;
            facilities = null;
            userData = null;
        }

        return ClearPacketPayload.create(cause, diagnostic, calledAddress, callingAddress, facilities, userData);
    }

    public static void marshalClearConfirmationPayload(final OutputStream outputStream, final X25Payload source, final boolean aBit) throws IOException {

        final ClearConfirmationPayload payload = (ClearConfirmationPayload) source;
        final X121Address calledAddress = payload.getCalledAddress();
        final X121Address callingAddress = payload.getCallingAddress();
        final Collection<Facility> facilities = payload.getFacilities();

        if ((null != calledAddress) && (null != callingAddress)) {
            X121AddressMarshaller.marshal(outputStream, calledAddress, callingAddress, aBit);

            if ((null != facilities) && (0 < facilities.size())) {
                final byte[] facilitiesBlock = FacilityMarshaller.marshal(facilities);
                outputStream.write(facilitiesBlock.length);
                outputStream.write(facilitiesBlock);
            }
        }
    }

    public static X25Payload unmarshalClearConfirmationPayload(final InputStream inputStream, final boolean aBit) throws IOException {

        final X121Address calledAddress;
        final X121Address callingAddress;
        final Collection<Facility> facilities;

        if (0 < inputStream.available()) {

            {
                final X121AddressPair addressPair = X121AddressMarshaller.unmarshal(inputStream, aBit);
                calledAddress = addressPair.calledAddress;
                callingAddress = addressPair.callingAddress;
            }

            if (0 < inputStream.available()) {

                final int octetLengthOfFacilities = 0x00ff & inputStream.read();
                final byte[] facilitiesBlock = new byte[octetLengthOfFacilities];
                inputStream.read(facilitiesBlock, 0, facilitiesBlock.length);
                facilities = FacilityMarshaller.unmarshal(facilitiesBlock);

            } else {
                facilities = null;
            }

        } else {
            calledAddress = null;
            callingAddress = null;
            facilities = null;
        }

        return ClearConfirmationPayload.create(calledAddress, callingAddress, facilities);
    }

    public static void marshalResetPacketPayload(final OutputStream outputStream, final X25Payload source) throws IOException {

        final ResetPacketPayload payload = (ResetPacketPayload) source;
        final ResetRequestEnumeration cause = payload.getCause();
        final DiagnosticCodeEnumeration diagnostic = payload.getDiagnostic();

        outputStream.write(0x00ff & cause.toInt());
        outputStream.write(0x00ff & diagnostic.toInt());
    }

    public static X25Payload unmarshalResetPacketPayload(final InputStream inputStream) throws IOException {

        final ResetRequestEnumeration cause = ResetRequestEnumeration.fromInt(0x00ff & inputStream.read());
        final DiagnosticCodeEnumeration diagnostic = DiagnosticCodeEnumeration.fromInt(0x00ff & inputStream.read());

        return ResetPacketPayload.create(cause, diagnostic);
    }

    public static void marshalRestartPacketPayload(final OutputStream outputStream, final X25Payload source) throws IOException {

        final RestartPacketPayload payload = (RestartPacketPayload) source;
        final RestartRequestEnumeration cause = payload.getCause();
        final DiagnosticCodeEnumeration diagnostic = payload.getDiagnostic();

        outputStream.write(0x00ff & cause.toInt());
        outputStream.write(0x00ff & diagnostic.toInt());
    }

    public static X25Payload unmarshalRestartPacketPayload(final InputStream inputStream) throws IOException {

        final RestartRequestEnumeration cause = RestartRequestEnumeration.fromInt(0x00ff & inputStream.read());
        final DiagnosticCodeEnumeration diagnostic = DiagnosticCodeEnumeration.fromInt(0x00ff & inputStream.read());

        return RestartPacketPayload.create(cause, diagnostic);
    }

    public static void marshalDataPacketPayload(final OutputStream outputStream, final X25Payload source) throws IOException {

        final DataPacketPayload payload = (DataPacketPayload) source;
        final byte[] userData = payload.getUserData();
        outputStream.write(userData);
    }

    public static X25Payload unmarshalDataPacketPayload(final InputStream inputStream) throws IOException {

        final int octetLengthOfUserData = inputStream.available();
        final byte[] userData = new byte[octetLengthOfUserData];
        if (0 < octetLengthOfUserData) {
            inputStream.read(userData, 0, userData.length);
        }

        return DataPacketPayload.create(userData);
    }

    public static void marshalEmptyPacketPayload(final OutputStream outputStream, final X25Payload source) throws IOException {
        // do nothing
    }

    public static X25Payload unmarshalEmptyPacketPayload(final InputStream inputStream) throws IOException {

        return EmptyPacketPayload.create();
    }
}
