//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.payload;

import java.util.Collection;
import xot4j.x121.X121Address;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.facilities.Facility;

/**
 *
 *        .====================================================.
 * O   1  ||  0    0  ||   X   X   ||        L C G N          || C
 * C      |====================================================| L
 * T   1  ||                    L  C  N                       || E
 * E      |====================================================| A
 * T   1  ||                    P  T  I                       || R
 * S      |====================================================|
 *     1  ||          C L E A R I N G   C A U S E             || I
 *        |====================================================| N
 *     1  ||       C L E A R I N G   D I A G N O S T I C      || D
 *        |====================================================| I
 *     17 ||   ADDRESSING IN CALL REQUEST FORMAT IF USED      || C
 *        |====================================================| A
 *     N  ||   FACILITIES IN CALL REQUEST FORMAT IF USED      || T
 *        |====================================================| I
 *     16 ||       C L E A R   U S E R    D A T A             || O
 *        `====================================================' N
 * 
 */
public final class ClearPacketPayload implements X25Payload {

    private final ClearRequestEnumeration cause;
    private final DiagnosticCodeEnumeration diagnostic;
    private final X121Address calledAddress /* optional */;
    private final X121Address callingAddress /* optional */;
    private final Collection<Facility> facilities /* optional */;
    private final byte[] userData /* optional */;

    ClearPacketPayload(ClearRequestEnumeration cause,
            DiagnosticCodeEnumeration diagnostic,
            X121Address calledAddress,
            X121Address callingAddress,
            Collection<Facility> facilities,
            byte[] userData) {

        this.cause = cause;
        this.diagnostic = diagnostic;
        this.calledAddress = calledAddress;
        this.callingAddress = callingAddress;
        this.facilities = facilities;
        this.userData = userData;
    }

    public static ClearPacketPayload create(ClearRequestEnumeration cause,
            DiagnosticCodeEnumeration diagnostic,
            X121Address calledAddress,
            X121Address callingAddress,
            Collection<Facility> facilities,
            byte[] userData) {

        return new ClearPacketPayload(cause,
                diagnostic,
                calledAddress,
                callingAddress,
                facilities,
                userData);
    }

    public static ClearPacketPayload create(ClearRequestEnumeration cause,
            DiagnosticCodeEnumeration diagnostic) {

        return create(cause,
                diagnostic,
                null /* calledAddress */,
                null /* callingAddress */,
                null /* facilities */,
                null /* userData */);
    }

    public ClearRequestEnumeration getCause() {

        return cause;
    }

    public DiagnosticCodeEnumeration getDiagnostic() {

        return diagnostic;
    }

    public X121Address getCalledAddress() {

        return calledAddress;
    }

    public X121Address getCallingAddress() {

        return callingAddress;
    }

    public Collection<Facility> getFacilities() {

        return facilities;
    }

    public byte[] getUserData() {

        return userData;
    }
}
