//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

import java.util.Collection;
import xot4j.x121.X121Address;
import xot4j.x25.X25Header;
import xot4j.x25.X25Packet;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.context.SessionContext;
import xot4j.x25.facilities.Facility;
import xot4j.x25.gfi.GeneralFormatIdentifier;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.payload.CallPacketPayload;
import xot4j.x25.payload.ClearConfirmationPayload;
import xot4j.x25.payload.ClearPacketPayload;
import xot4j.x25.payload.DataPacketPayload;
import xot4j.x25.payload.EmptyPacketPayload;
import xot4j.x25.pti.PacketTypeIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;
import xot4j.x25.pti.PacketTypeIdentifierFactory;

/**
 *
 * @todo add modulo 128 support
 */
public final class X25PacketUtility {

    private X25PacketUtility() {
    }

    static X25Header header(LogicalChannelIdentifier logicalChannelIdentifier,
            PacketTypeIdentifierEnumeration packetTypeIdentifierEnumeration) {


        final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                GeneralFormatIdentifier.QualifiedData.DataForUser,
                GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);
        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(packetTypeIdentifierEnumeration);

        return X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    static X25Header header(LogicalChannelIdentifier logicalChannelIdentifier,
            PacketTypeIdentifierEnumeration packetTypeIdentifierEnumeration,
            boolean moreData,
            int packetReceiveSequenceNumber,
            int packetSendSequenceNumber) {


        final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                GeneralFormatIdentifier.QualifiedData.DataForUser,
                GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);

        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(
                packetTypeIdentifierEnumeration,
                moreData,
                packetReceiveSequenceNumber,
                packetSendSequenceNumber);

        return X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    static X25Header header(LogicalChannelIdentifier logicalChannelIdentifier,
            PacketTypeIdentifierEnumeration packetTypeIdentifierEnumeration,
            int packetReceiveSequenceNumber) {


        final GeneralFormatIdentifier generalFormatIdentifier = GeneralFormatIdentifier.create(
                GeneralFormatIdentifier.QualifiedData.DataForUser,
                GeneralFormatIdentifier.DeliveryConfirmation.ForLocalAcknowledgment,
                GeneralFormatIdentifier.ProtocolIdentification.Modulo8Sequencing);

        final PacketTypeIdentifier packetTypeIdentifier = PacketTypeIdentifierFactory.create(
                packetTypeIdentifierEnumeration,
                packetReceiveSequenceNumber);

        return X25Header.create(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    public static X25Packet callRequest(SessionContext sessionContext, byte[] userData) {

        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.CallRequest_IncomingCall;
            header = header(logicalChannelIdentifier, enumeration);
        }

        final CallPacketPayload payload;
        {
            final X121Address calledAddress = sessionContext.getRemoteAddress();
            final X121Address callingAddress = sessionContext.getLocalAddress();
            final Collection<Facility> facilities = sessionContext.getFacilities();

            payload = CallPacketPayload.create(calledAddress, callingAddress, facilities, userData);
        }

        return X25Packet.create(header, payload);
    }

    public static X25Packet callAccept(SessionContext sessionContext) {

        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.CallAccepted_CallConnected;
            header = header(logicalChannelIdentifier, enumeration);
        }

        final CallPacketPayload payload;
        {
            final X121Address calledAddress = null;
            final X121Address callingAddress = null;
            final Collection<Facility> facilities = null;
            final byte[] userData = null;

            payload = CallPacketPayload.create(calledAddress, callingAddress, facilities, userData);
        }

        return X25Packet.create(header, payload);
    }

    public static X25Packet clearConfirm(SessionContext sessionContext) {

        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.ClearConfirmation;
            header = header(logicalChannelIdentifier, enumeration);
        }

        final ClearConfirmationPayload payload = ClearConfirmationPayload.create();

        return X25Packet.create(header, payload);
    }

    public static X25Packet clearRequest(SessionContext sessionContext) {

        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.ClearRequest_ClearIndication;
            header = header(logicalChannelIdentifier, enumeration);
        }

        final ClearPacketPayload payload = ClearPacketPayload.create(ClearRequestEnumeration.ClearRequestDteOriginated,
                DiagnosticCodeEnumeration.NoAdditionalInformation);

        return X25Packet.create(header, payload);
    }

    public static X25Packet data(SessionContext sessionContext, byte[] userData, boolean moreData) {


        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.Data;
            final int packetReceiveSequenceNumber = sessionContext.getPacketReceiveSequenceNumber();
            final int packetSendSequenceNumber = sessionContext.getPacketSendSequenceNumber();
            header = header(logicalChannelIdentifier, enumeration, moreData, packetReceiveSequenceNumber, packetSendSequenceNumber);
        }

        final DataPacketPayload payload = DataPacketPayload.create(userData);

        return X25Packet.create(header, payload);
    }

    public static X25Packet receiverReady(SessionContext sessionContext) {

        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.ReceiverReady;
            final int packetReceiveSequenceNumber = sessionContext.getPacketReceiveSequenceNumber();
            header = header(logicalChannelIdentifier, enumeration, packetReceiveSequenceNumber);
        }

        final EmptyPacketPayload payload = EmptyPacketPayload.create();

        return X25Packet.create(header, payload);
    }

    public static X25Packet resetConfirm(SessionContext sessionContext) {

        final X25Header header;
        {
            final LogicalChannelIdentifier logicalChannelIdentifier = sessionContext.getLogicalChannelIdentifier();
            final PacketTypeIdentifierEnumeration enumeration = PacketTypeIdentifierEnumeration.ResetConfirmation;
            header = header(logicalChannelIdentifier, enumeration);
        }

        final EmptyPacketPayload payload = EmptyPacketPayload.create();

        return X25Packet.create(header, payload);
    }
}
