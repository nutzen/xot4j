//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class StreamUtility {

    private StreamUtility() {
    }

    public static byte[] readBuffer(final InputStream inputStream, final int length) throws IOException {

        final byte[] buffer = new byte[length];

        int offset = 0;
        while (offset < length) {
            final int read = inputStream.read(buffer, offset, length - offset);
            if (0 > read) {
                throw new IOException("End of stream");
            }
            offset += read;
        }

        return buffer;
    }

    public static short readShort(final InputStream inputStream) throws IOException {

        final byte[] buffer = readBuffer(inputStream, 2);
        int value = 0x00ff & buffer[0];
        value = (value << 8);
        value = value | (0x00ff & buffer[1]);

        return (short) value;
    }

    public static void write(final OutputStream outputStream, final short value) throws IOException {

        outputStream.write(0x00ff & (value >> 8));
        outputStream.write(0x00ff & value);
    }
}
