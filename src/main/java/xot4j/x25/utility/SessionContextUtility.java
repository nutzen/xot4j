//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

import xot4j.x25.context.SessionContext;
import xot4j.x25.facilities.Facility;
import xot4j.x25.facilities.FacilityEnumeration;
import xot4j.x25.facilities.PacketSizeSelection;
import xot4j.x25.facilities.PacketSizeSelectionEnumeration;
import xot4j.x25.facilities.WindowSizeSelection;

/**
 *
 * @todo add modulo 128 support
 */
public final class SessionContextUtility {

    private SessionContextUtility() {
    }

    public static void updatePacketReceiveSequenceNumber(SessionContext sessionContext) {

        int packetReceiveSequenceNumber = sessionContext.getPacketReceiveSequenceNumber();
        packetReceiveSequenceNumber = (1 + packetReceiveSequenceNumber) % 8;
        sessionContext.setPacketReceiveSequenceNumber(packetReceiveSequenceNumber);
    }

    public static void updatePacketSendSequenceNumber(SessionContext sessionContext) {

        int packetSendSequenceNumber = sessionContext.getPacketSendSequenceNumber();
        packetSendSequenceNumber = (1 + packetSendSequenceNumber) % 8;
        sessionContext.setPacketSendSequenceNumber(packetSendSequenceNumber);
    }

    public static int receivePacketSize(SessionContext sessionContext) {

        int size = PacketSizeSelectionEnumeration.Size128octets.toPacketSize();

        for (Facility facility : sessionContext.getFacilities()) {
            if (FacilityEnumeration.PacketSizeSelection == facility.getEnumeration()) {
                final PacketSizeSelection sizeSelection = (PacketSizeSelection) facility;
                final PacketSizeSelectionEnumeration enumeration = sizeSelection.getReceivePacketSize();
                size = enumeration.toPacketSize();
                break;
            }
        }

        return size;
    }

    public static int transmitPacketSize(SessionContext sessionContext) {

        int size = PacketSizeSelectionEnumeration.Size128octets.toPacketSize();

        for (Facility facility : sessionContext.getFacilities()) {
            if (FacilityEnumeration.PacketSizeSelection == facility.getEnumeration()) {
                final PacketSizeSelection sizeSelection = (PacketSizeSelection) facility;
                final PacketSizeSelectionEnumeration enumeration = sizeSelection.getTransmitPacketSize();
                size = enumeration.toPacketSize();
                break;
            }
        }

        return size;
    }

    public static int receiveWindowSize(SessionContext sessionContext) {

        int size = 2;

        for (Facility facility : sessionContext.getFacilities()) {
            if (FacilityEnumeration.WindowSizeSelection == facility.getEnumeration()) {
                final WindowSizeSelection sizeSelection = (WindowSizeSelection) facility;
                size = sizeSelection.getReceiveWindowSize();
                break;
            }
        }

        return size;
    }

    public static int transmissionWindowSize(SessionContext sessionContext) {

        int size = 2;

        for (Facility facility : sessionContext.getFacilities()) {
            if (FacilityEnumeration.WindowSizeSelection == facility.getEnumeration()) {
                final WindowSizeSelection sizeSelection = (WindowSizeSelection) facility;
                size = sizeSelection.getTransmissionWindowSize();
                break;
            }
        }

        return size;
    }
}
