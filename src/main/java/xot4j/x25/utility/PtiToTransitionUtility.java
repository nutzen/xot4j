//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

import xot4j.x25.X25Packet;
import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;
import xot4j.x25.transitions.TransitionEnumeration;
import xot4j.x25.transitions.TransitionParameters;

public final class PtiToTransitionUtility {

    private PtiToTransitionUtility() {
    }

    public static TransitionEnumeration transition(TransitionParameters parameters) {

        final RoleEnumeration origin = parameters.getOrigin();
        final X25Packet packet = parameters.getPacket();
        final PacketTypeIdentifierEnumeration pti = packet.getHeader().getPacketTypeIdentifier().getEnumeration();

        return transition(origin, pti);
    }

    public static TransitionEnumeration transition(RoleEnumeration role, PacketTypeIdentifierEnumeration packetTypeIdentifier) {

        TransitionEnumeration destination;

        switch (packetTypeIdentifier) {
            case CallRequest_IncomingCall:
                if (RoleEnumeration.DTE == role) {
                    destination = TransitionEnumeration.CallRequest;
                } else /* if (SessionRole.DCE == role) */ {
                    destination = TransitionEnumeration.CallIncoming;
                }
                break;

            case CallAccepted_CallConnected:
                if (RoleEnumeration.DTE == role) {
                    destination = TransitionEnumeration.CallAccepted;
                } else /* if (SessionRole.DCE == role) */ {
                    destination = TransitionEnumeration.CallConnected;
                }
                break;

            case ClearRequest_ClearIndication:
                if (RoleEnumeration.DTE == role) {
                    destination = TransitionEnumeration.ClearRequest;
                } else /* if (SessionRole.DCE == role) */ {
                    destination = TransitionEnumeration.ClearIndication;
                }
                break;

            case ClearConfirmation:
                destination = TransitionEnumeration.ClearConfirmation;
                break;

            case Data:
                destination = TransitionEnumeration.Data;
                break;

            case Interrupt:
                destination = TransitionEnumeration.Interrupt;
                break;

            case InterruptConfirmation:
                destination = TransitionEnumeration.InterruptConfirmation;
                break;

            case ReceiverReady:
                destination = TransitionEnumeration.ReceiverReady;
                break;

            case ReceiverNotReady:
                destination = TransitionEnumeration.ReceiverNotReady;
                break;

            case Reject:
                destination = TransitionEnumeration.Reject;
                break;

            case ResetRequest_ResetIndication:
                if (RoleEnumeration.DTE == role) {
                    destination = TransitionEnumeration.ResetRequest;
                } else /* if (SessionRole.DCE == role) */ {
                    destination = TransitionEnumeration.ResetIndication;
                }
                break;

            case ResetConfirmation:
                destination = TransitionEnumeration.ResetConfirmation;
                break;

            case RestartRequest_RestartIndication:
                if (RoleEnumeration.DTE == role) {
                    destination = TransitionEnumeration.RestartRequest;
                } else /* if (SessionRole.DCE == role) */ {
                    destination = TransitionEnumeration.RestartIndication;
                }
                break;

            case RestartConfirmation:
                destination = TransitionEnumeration.RestartConfirmation;
                break;

            case Diagnostic:
                destination = TransitionEnumeration.Diagnostic;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + packetTypeIdentifier);
        }

        return destination;
    }
}
