//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.utility;

public final class HexUtility {

    private HexUtility() {
    }

    public static String toHex(byte b) {

        final String text = "00" + Integer.toHexString(b);
        return text.substring(text.length() - 2);
    }

    public static String toHex(byte[] buffer) {
        return toHex(buffer, 0, buffer.length);
    }

    public static String toHex(byte[] buffer, int offset, int length) {

        final StringBuilder builder = new StringBuilder();
        for (int index = offset; index < length; ++index) {
            if (index > offset) {
                builder.append(" ");
            }
            builder.append(toHex(buffer[index]));
        }
        return builder.toString();
    }
}
