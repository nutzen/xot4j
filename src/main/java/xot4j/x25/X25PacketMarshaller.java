//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import xot4j.x25.payload.X25Payload;
import xot4j.x25.payload.X25PayloadMarshaller;
import xot4j.x25.pti.PacketTypeIdentifier;
import xot4j.x25.pti.PacketTypeIdentifierEnumeration;

/**
 *
 * @todo extract a-bit from x.25 header
 */
public final class X25PacketMarshaller {

    private static final boolean aBit = false;

    private X25PacketMarshaller() {
    }

    public static void marshal(final OutputStream outputStream, final X25Packet source) throws IOException {

        final X25Header header = source.getHeader();
        final X25Payload payload = source.getPayload();

        X25HeaderMarshaller.marshal(outputStream, header);

        final PacketTypeIdentifier pti = header.getPacketTypeIdentifier();
        final PacketTypeIdentifierEnumeration enumeration = pti.getEnumeration();
        switch (enumeration) {
            case CallRequest_IncomingCall:
            case CallAccepted_CallConnected:
                X25PayloadMarshaller.marshalCallPacketPayload(outputStream, payload, aBit);
                break;

            case ClearRequest_ClearIndication:
                X25PayloadMarshaller.marshalClearPacketPayload(outputStream, payload, aBit);
                break;

            case ClearConfirmation:
                X25PayloadMarshaller.marshalClearConfirmationPayload(outputStream, payload, aBit);
                break;

            case ResetRequest_ResetIndication:
                X25PayloadMarshaller.marshalResetPacketPayload(outputStream, payload);
                break;

            case RestartRequest_RestartIndication:
                X25PayloadMarshaller.marshalRestartPacketPayload(outputStream, payload);
                break;

            case Data:
                X25PayloadMarshaller.marshalDataPacketPayload(outputStream, payload);
                break;

            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
            case ResetConfirmation:
            case RestartConfirmation:
                X25PayloadMarshaller.marshalEmptyPacketPayload(outputStream, payload);
                break;

            default:
                throw new UnsupportedOperationException(enumeration + " - Not supported yet.");
        }
    }

    public static X25Packet unmarshal(final InputStream inputStream) throws IOException {

        final X25Header header = X25HeaderMarshaller.unmarshal(inputStream);

        X25Payload payload;
        final PacketTypeIdentifier pti = header.getPacketTypeIdentifier();
        final PacketTypeIdentifierEnumeration enumeration = pti.getEnumeration();
        switch (enumeration) {
            case CallRequest_IncomingCall:
            case CallAccepted_CallConnected:
                payload = X25PayloadMarshaller.unmarshalCallPacketPayload(inputStream, aBit);
                break;

            case ClearRequest_ClearIndication:
                payload = X25PayloadMarshaller.unmarshalClearPacketPayload(inputStream, aBit);
                break;

            case ClearConfirmation:
                payload = X25PayloadMarshaller.unmarshalClearConfirmationPayload(inputStream, aBit);
                break;

            case ResetRequest_ResetIndication:
                payload = X25PayloadMarshaller.unmarshalResetPacketPayload(inputStream);
                break;

            case RestartRequest_RestartIndication:
                payload = X25PayloadMarshaller.unmarshalRestartPacketPayload(inputStream);
                break;

            case Data:
                payload = X25PayloadMarshaller.unmarshalDataPacketPayload(inputStream);
                break;

            case ReceiverReady:
            case ReceiverNotReady:
            case Reject:
            case ResetConfirmation:
            case RestartConfirmation:
                payload = X25PayloadMarshaller.unmarshalEmptyPacketPayload(inputStream);
                break;

            default:
                throw new UnsupportedOperationException(enumeration + " - Not supported yet.");
        }

        return X25Packet.create(header, payload);
    }
}
