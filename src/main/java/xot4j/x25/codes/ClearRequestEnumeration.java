//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

import static xot4j.x25.codes.CauseCodeEnumerationUtility.*;

public enum ClearRequestEnumeration {

    ClearRequestDteOriginated,
    ClearRequestNumberBusy,
    ClearRequestInvalidFacilityRequest,
    ClearRequestNetworkCongestion,
    ClearRequestOutOfOrder,
    ClearRequestAccessBarred,
    ClearRequestNotObtainable,
    ClearRequestRemoteProcedureError,
    ClearRequestLocalProcedureError,
    ClearRequestRpoaOutOfOrder,
    ClearRequestReverseChargingNotAccepted,
    ClearRequestIncompatibleDestination,
    ClearRequestFastSelectNotAccepted,
    ClearRequestShipAbsent;

    public String toDescription() {

        String destination;

        switch (this) {
            case ClearRequestDteOriginated:
                destination = CLEAR_REQUEST_DTE_ORIGINATED_DESCRIPTION;
                break;

            case ClearRequestNumberBusy:
                destination = CLEAR_REQUEST_NUMBER_BUSY_DESCRIPTION;
                break;

            case ClearRequestInvalidFacilityRequest:
                destination = CLEAR_REQUEST_INVALID_FACILITY_REQUEST_DESCRIPTION;
                break;

            case ClearRequestNetworkCongestion:
                destination = CLEAR_REQUEST_NETWORK_CONGESTION_DESCRIPTION;
                break;

            case ClearRequestOutOfOrder:
                destination = CLEAR_REQUEST_OUT_OF_ORDER_DESCRIPTION;
                break;

            case ClearRequestAccessBarred:
                destination = CLEAR_REQUEST_ACCESS_BARRED_DESCRIPTION;
                break;

            case ClearRequestNotObtainable:
                destination = CLEAR_REQUEST_NOT_OBTAINABLE_DESCRIPTION;
                break;

            case ClearRequestRemoteProcedureError:
                destination = CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION;
                break;

            case ClearRequestLocalProcedureError:
                destination = CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION;
                break;

            case ClearRequestRpoaOutOfOrder:
                destination = CLEAR_REQUEST_RPOA_OUT_OF_ORDER_DESCRIPTION;
                break;

            case ClearRequestReverseChargingNotAccepted:
                destination = CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_DESCRIPTION;
                break;

            case ClearRequestIncompatibleDestination:
                destination = CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION;
                break;

            case ClearRequestFastSelectNotAccepted:
                destination = CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_DESCRIPTION;
                break;

            case ClearRequestShipAbsent:
                destination = CLEAR_REQUEST_SHIP_ABSENT_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static ClearRequestEnumeration fromDescription(final String value) {

        final ClearRequestEnumeration destination;
        switch (value) {
            case CLEAR_REQUEST_DTE_ORIGINATED_DESCRIPTION:
                destination = ClearRequestDteOriginated;
                break;

            case CLEAR_REQUEST_NUMBER_BUSY_DESCRIPTION:
                destination = ClearRequestNumberBusy;
                break;

            case CLEAR_REQUEST_INVALID_FACILITY_REQUEST_DESCRIPTION:
                destination = ClearRequestInvalidFacilityRequest;
                break;

            case CLEAR_REQUEST_NETWORK_CONGESTION_DESCRIPTION:
                destination = ClearRequestNetworkCongestion;
                break;

            case CLEAR_REQUEST_OUT_OF_ORDER_DESCRIPTION:
                destination = ClearRequestOutOfOrder;
                break;

            case CLEAR_REQUEST_ACCESS_BARRED_DESCRIPTION:
                destination = ClearRequestAccessBarred;
                break;

            case CLEAR_REQUEST_NOT_OBTAINABLE_DESCRIPTION:
                destination = ClearRequestNotObtainable;
                break;

            case CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION:
                destination = ClearRequestRemoteProcedureError;
                break;

            case CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION:
                destination = ClearRequestLocalProcedureError;
                break;

            case CLEAR_REQUEST_RPOA_OUT_OF_ORDER_DESCRIPTION:
                destination = ClearRequestRpoaOutOfOrder;
                break;

            case CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_DESCRIPTION:
                destination = ClearRequestReverseChargingNotAccepted;
                break;

            case CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION:
                destination = ClearRequestIncompatibleDestination;
                break;

            case CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_DESCRIPTION:
                destination = ClearRequestFastSelectNotAccepted;
                break;

            case CLEAR_REQUEST_SHIP_ABSENT_DESCRIPTION:
                destination = ClearRequestShipAbsent;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }

    public int toInt() {

        int destination;

        switch (this) {
            case ClearRequestDteOriginated:
                destination = CLEAR_REQUEST_DTE_ORIGINATED_MASK;
                break;

            case ClearRequestNumberBusy:
                destination = CLEAR_REQUEST_NUMBER_BUSY_MASK;
                break;

            case ClearRequestInvalidFacilityRequest:
                destination = CLEAR_REQUEST_INVALID_FACILITY_REQUEST_MASK;
                break;

            case ClearRequestNetworkCongestion:
                destination = CLEAR_REQUEST_NETWORK_CONGESTION_MASK;
                break;

            case ClearRequestOutOfOrder:
                destination = CLEAR_REQUEST_OUT_OF_ORDER_MASK;
                break;

            case ClearRequestAccessBarred:
                destination = CLEAR_REQUEST_ACCESS_BARRED_MASK;
                break;

            case ClearRequestNotObtainable:
                destination = CLEAR_REQUEST_NOT_OBTAINABLE_MASK;
                break;

            case ClearRequestRemoteProcedureError:
                destination = CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_MASK;
                break;

            case ClearRequestLocalProcedureError:
                destination = CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_MASK;
                break;

            case ClearRequestRpoaOutOfOrder:
                destination = CLEAR_REQUEST_RPOA_OUT_OF_ORDER_MASK;
                break;

            case ClearRequestReverseChargingNotAccepted:
                destination = CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_MASK;
                break;

            case ClearRequestIncompatibleDestination:
                destination = CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_MASK;
                break;

            case ClearRequestFastSelectNotAccepted:
                destination = CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_MASK;
                break;

            case ClearRequestShipAbsent:
                destination = CLEAR_REQUEST_SHIP_ABSENT_MASK;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);

        }

        return destination;
    }

    public static ClearRequestEnumeration fromInt(final int value) {

        final ClearRequestEnumeration destination;

        if (compareWithMask(CLEAR_REQUEST_DTE_ORIGINATED_MASK, value)) {
            destination = ClearRequestDteOriginated;

        } else if (compareWithMask(CLEAR_REQUEST_NUMBER_BUSY_MASK, value)) {
            destination = ClearRequestNumberBusy;

        } else if (compareWithMask(CLEAR_REQUEST_INVALID_FACILITY_REQUEST_MASK, value)) {
            destination = ClearRequestInvalidFacilityRequest;

        } else if (compareWithMask(CLEAR_REQUEST_NETWORK_CONGESTION_MASK, value)) {
            destination = ClearRequestNetworkCongestion;

        } else if (compareWithMask(CLEAR_REQUEST_OUT_OF_ORDER_MASK, value)) {
            destination = ClearRequestOutOfOrder;

        } else if (compareWithMask(CLEAR_REQUEST_ACCESS_BARRED_MASK, value)) {
            destination = ClearRequestAccessBarred;

        } else if (compareWithMask(CLEAR_REQUEST_NOT_OBTAINABLE_MASK, value)) {
            destination = ClearRequestNotObtainable;

        } else if (compareWithMask(CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_MASK, value)) {
            destination = ClearRequestRemoteProcedureError;

        } else if (compareWithMask(CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_MASK, value)) {
            destination = ClearRequestLocalProcedureError;

        } else if (compareWithMask(CLEAR_REQUEST_RPOA_OUT_OF_ORDER_MASK, value)) {
            destination = ClearRequestRpoaOutOfOrder;

        } else if (compareWithMask(CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_MASK, value)) {
            destination = ClearRequestReverseChargingNotAccepted;

        } else if (compareWithMask(CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_MASK, value)) {
            destination = ClearRequestIncompatibleDestination;

        } else if (compareWithMask(CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_MASK, value)) {
            destination = ClearRequestFastSelectNotAccepted;

        } else if (compareWithMask(CLEAR_REQUEST_SHIP_ABSENT_MASK, value)) {
            destination = ClearRequestShipAbsent;

        } else {
            throw new RuntimeException("Unhandled value: " + value);

        }

        return destination;
    }
}
