//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

import static xot4j.x25.codes.CauseCodeEnumerationUtility.*;

public enum RestartRequestEnumeration {

    RestartRequestDteRestarting,
    RestartRequestLocalProcedureError,
    RestartRequestNetworkCongestion,
    RestartRequestNetworkOperational,
    RestartRequestRegistrationCancellationConfirmed;

    public String toDescription() {

        String destination;

        switch (this) {
            case RestartRequestDteRestarting:
                destination = RESTART_REQUEST_DTE_RESTARTING_DESCRIPTION;
                break;

            case RestartRequestLocalProcedureError:
                destination = RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION;
                break;

            case RestartRequestNetworkCongestion:
                destination = RESTART_REQUEST_NETWORK_CONGESTION_DESCRIPTION;
                break;

            case RestartRequestNetworkOperational:
                destination = RESTART_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION;
                break;

            case RestartRequestRegistrationCancellationConfirmed:
                destination = RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static RestartRequestEnumeration fromDescription(final String value) {

        final RestartRequestEnumeration destination;
        switch (value) {
            case RESTART_REQUEST_DTE_RESTARTING_DESCRIPTION:
                destination = RestartRequestDteRestarting;
                break;

            case RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION:
                destination = RestartRequestLocalProcedureError;
                break;

            case RESTART_REQUEST_NETWORK_CONGESTION_DESCRIPTION:
                destination = RestartRequestNetworkCongestion;
                break;

            case RESTART_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION:
                destination = RestartRequestNetworkOperational;
                break;

            case RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_DESCRIPTION:
                destination = RestartRequestRegistrationCancellationConfirmed;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }

    public int toInt() {

        int destination;

        switch (this) {
            case RestartRequestDteRestarting:
                destination = RESTART_REQUEST_DTE_RESTARTING_MASK;
                break;

            case RestartRequestLocalProcedureError:
                destination = RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_MASK;
                break;

            case RestartRequestNetworkCongestion:
                destination = RESTART_REQUEST_NETWORK_CONGESTION_MASK;
                break;

            case RestartRequestNetworkOperational:
                destination = RESTART_REQUEST_NETWORK_OPERATIONAL_MASK;
                break;

            case RestartRequestRegistrationCancellationConfirmed:
                destination = RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_MASK;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);

        }

        return destination;
    }

    public static RestartRequestEnumeration fromInt(final int value) {

        final RestartRequestEnumeration destination;

        if (compareWithMask(RESTART_REQUEST_DTE_RESTARTING_MASK, value)) {
            destination = RestartRequestDteRestarting;

        } else if (compareWithMask(RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_MASK, value)) {
            destination = RestartRequestLocalProcedureError;

        } else if (compareWithMask(RESTART_REQUEST_NETWORK_CONGESTION_MASK, value)) {
            destination = RestartRequestNetworkCongestion;

        } else if (compareWithMask(RESTART_REQUEST_NETWORK_OPERATIONAL_MASK, value)) {
            destination = RestartRequestNetworkOperational;

        } else if (compareWithMask(RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_MASK, value)) {
            destination = RestartRequestRegistrationCancellationConfirmed;

        } else {
            throw new RuntimeException("Unhandled value: " + value);

        }

        return destination;
    }
}
