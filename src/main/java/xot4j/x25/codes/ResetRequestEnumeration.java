//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

import static xot4j.x25.codes.CauseCodeEnumerationUtility.*;

public enum ResetRequestEnumeration {

    ResetRequestDteOriginated,
    ResetRequestOutOfOrder,
    ResetRequestRemoteProcedureError,
    ResetRequestLocalProcedureError,
    ResetRequestNetworkCongestion,
    ResetRequestRemoteDteOperational,
    ResetRequestNetworkOperational,
    ResetRequestIncompatibleDestination,
    ResetRequestNetworkOutOfOrder;

    public String toDescription() {

        String destination;

        switch (this) {
            case ResetRequestDteOriginated:
                destination = RESET_REQUEST_DTE_ORIGINATED_DESCRIPTION;
                break;

            case ResetRequestOutOfOrder:
                destination = RESET_REQUEST_OUT_OF_ORDER_DESCRIPTION;
                break;

            case ResetRequestRemoteProcedureError:
                destination = RESET_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION;
                break;

            case ResetRequestLocalProcedureError:
                destination = RESET_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION;
                break;

            case ResetRequestNetworkCongestion:
                destination = RESET_REQUEST_NETWORK_CONGESTION_DESCRIPTION;
                break;

            case ResetRequestRemoteDteOperational:
                destination = RESET_REQUEST_REMOTE_DTE_OPERATIONAL_DESCRIPTION;
                break;

            case ResetRequestNetworkOperational:
                destination = RESET_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION;
                break;

            case ResetRequestIncompatibleDestination:
                destination = RESET_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION;
                break;

            case ResetRequestNetworkOutOfOrder:
                destination = RESET_REQUEST_NETWORK_OUT_OF_ORDER_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static ResetRequestEnumeration fromDescription(final String value) {

        final ResetRequestEnumeration destination;
        switch (value) {
            case RESET_REQUEST_DTE_ORIGINATED_DESCRIPTION:
                destination = ResetRequestDteOriginated;
                break;

            case RESET_REQUEST_OUT_OF_ORDER_DESCRIPTION:
                destination = ResetRequestOutOfOrder;
                break;

            case RESET_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION:
                destination = ResetRequestRemoteProcedureError;
                break;

            case RESET_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION:
                destination = ResetRequestLocalProcedureError;
                break;

            case RESET_REQUEST_NETWORK_CONGESTION_DESCRIPTION:
                destination = ResetRequestNetworkCongestion;
                break;

            case RESET_REQUEST_REMOTE_DTE_OPERATIONAL_DESCRIPTION:
                destination = ResetRequestRemoteDteOperational;
                break;

            case RESET_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION:
                destination = ResetRequestNetworkOperational;
                break;

            case RESET_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION:
                destination = ResetRequestIncompatibleDestination;
                break;

            case RESET_REQUEST_NETWORK_OUT_OF_ORDER_DESCRIPTION:
                destination = ResetRequestNetworkOutOfOrder;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }

    public int toInt() {

        int destination;

        switch (this) {
            case ResetRequestDteOriginated:
                destination = RESET_REQUEST_DTE_ORIGINATED_MASK;
                break;

            case ResetRequestOutOfOrder:
                destination = RESET_REQUEST_OUT_OF_ORDER_MASK;
                break;

            case ResetRequestRemoteProcedureError:
                destination = RESET_REQUEST_REMOTE_PROCEDURE_ERROR_MASK;
                break;

            case ResetRequestLocalProcedureError:
                destination = RESET_REQUEST_LOCAL_PROCEDURE_ERROR_MASK;
                break;

            case ResetRequestNetworkCongestion:
                destination = RESET_REQUEST_NETWORK_CONGESTION_MASK;
                break;

            case ResetRequestRemoteDteOperational:
                destination = RESET_REQUEST_REMOTE_DTE_OPERATIONAL_MASK;
                break;

            case ResetRequestNetworkOperational:
                destination = RESET_REQUEST_NETWORK_OPERATIONAL_MASK;
                break;

            case ResetRequestIncompatibleDestination:
                destination = RESET_REQUEST_INCOMPATIBLE_DESTINATION_MASK;
                break;

            case ResetRequestNetworkOutOfOrder:
                destination = RESET_REQUEST_NETWORK_OUT_OF_ORDER_MASK;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);

        }

        return destination;
    }

    public static ResetRequestEnumeration fromInt(final int value) {

        final ResetRequestEnumeration destination;

        if (compareWithMask(RESET_REQUEST_DTE_ORIGINATED_MASK, value)) {
            destination = ResetRequestDteOriginated;

        } else if (compareWithMask(RESET_REQUEST_OUT_OF_ORDER_MASK, value)) {
            destination = ResetRequestOutOfOrder;

        } else if (compareWithMask(RESET_REQUEST_REMOTE_PROCEDURE_ERROR_MASK, value)) {
            destination = ResetRequestRemoteProcedureError;

        } else if (compareWithMask(RESET_REQUEST_LOCAL_PROCEDURE_ERROR_MASK, value)) {
            destination = ResetRequestLocalProcedureError;

        } else if (compareWithMask(RESET_REQUEST_NETWORK_CONGESTION_MASK, value)) {
            destination = ResetRequestNetworkCongestion;

        } else if (compareWithMask(RESET_REQUEST_REMOTE_DTE_OPERATIONAL_MASK, value)) {
            destination = ResetRequestRemoteDteOperational;

        } else if (compareWithMask(RESET_REQUEST_NETWORK_OPERATIONAL_MASK, value)) {
            destination = ResetRequestNetworkOperational;

        } else if (compareWithMask(RESET_REQUEST_INCOMPATIBLE_DESTINATION_MASK, value)) {
            destination = ResetRequestIncompatibleDestination;

        } else if (compareWithMask(RESET_REQUEST_NETWORK_OUT_OF_ORDER_MASK, value)) {
            destination = ResetRequestNetworkOutOfOrder;

        } else {
            throw new RuntimeException("Unhandled value: " + value);

        }

        return destination;
    }
}
