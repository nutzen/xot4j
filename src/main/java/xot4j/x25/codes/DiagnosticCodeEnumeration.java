//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

import static xot4j.x25.codes.DiagnosticCodeEnumerationUtility.*;

public enum DiagnosticCodeEnumeration {

    NoAdditionalInformation,
    InvalidPacketSendSequenceNumber,
    InvalidPacketReceiveSequenceNumber,
    PacketTypeInvalid,
    PacketTypeInvalidForStateR1PacketLevelReady,
    PacketTypeInvalidForStateR2DteRestartRequest,
    PacketTypeInvalidForStateR3DceRestartIndication,
    PacketTypeInvalidForStateP1Ready,
    PacketTypeInvalidForStateP2DteWaiting,
    PacketTypeInvalidForStateP3DceWaiting,
    PacketTypeInvalidForStateP4DataTransfer,
    PacketTypeInvalidForStateP5CallCollision,
    PacketTypeInvalidForStateP6DteClearRequest,
    PacketTypeInvalidForStateP7DceClearIndication,
    PacketTypeInvalidForStateD1FlowControlReady,
    PacketTypeInvalidForStateD2DteResetReady,
    PacketTypeInvalidForStateD3DceResetIndication,
    PacketNotAllowed,
    UnidentifiablePacket,
    CallOnOneWayLogicalChannel,
    InvalidPacketTypeOnAPermanentVirtualCircuit,
    PacketOnUnassignedLogicalChannelNumber,
    RejectNotSubscribedTo,
    PacketTooShort,
    PacketTooLong,
    InvalidGeneralFormatIdentifier,
    RestartOrRegistrationPacketWithNonzeroLci,
    PacketTypeNotCompatibleWithFacility,
    UnauthorizedInterruptConfirmation,
    UnauthorizedInterrupt,
    UnauthorizedReject,
    TimerExpired,
    TimerExpiredForIncomingCall,
    TimerExpiredForClearIndication,
    TimerExpiredForResetIndication,
    TimerExpiredForRestartIndication,
    TimerExpiredForCallDeflection,
    CallSetupClearingOrRegistrationProblem,
    FacilityCodeNotAllowed,
    FacilityParameterNotAllowed,
    InvalidCalledAddress,
    InvalidCallingAddress,
    InvalidFacilityLength,
    IncomingCallBarred,
    NoLogicalChannelAvailable,
    CallCollision,
    DuplicateFacilityRequested,
    NonzeroAddressLength,
    NonzeroFacilityLength,
    FacilityNotProvidedWhenExpected,
    InvalidItuTSpecifiedDteFacility,
    MaximumNumberOfCallRedirectionsOrDeflectionsExceeded,
    Miscellaneous,
    ImproperCauseCodeForDte,
    OctetNotAaligned,
    InconsistentQBitSetting,
    NetworkUserIdentificationProblem,
    InternationalProblem,
    RemoteNetworkProblem,
    InternationalProtocolProblem,
    InternationalLinkOutOfOrder,
    InternationalLinkBusy,
    TransitNetworkFacilityProblem,
    RemoteNetworkFacilityProblem,
    InternationalRoutingProblem,
    TemporaryRoutingProblem,
    UnknownCalledDataNetworkIdentificationCode,
    MaintenanceActionClearX25VcCommandIssued,
    // 0xf..
    NormalTermination,
    OutOfResources,
    AuthenticationFailure,
    InboundUserDataTooLarge,
    IdleTimerExpired;

    public String toDescription() {

        String destination;

        switch (this) {
            case NoAdditionalInformation:
                destination = NO_ADDITIONAL_INFORMATION_DESCRIPTION;
                break;

            case InvalidPacketSendSequenceNumber:
                destination = INVALID_PACKET_SEND_SEQUENCE_NUMBER_DESCRIPTION;
                break;

            case InvalidPacketReceiveSequenceNumber:
                destination = INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_DESCRIPTION;
                break;

            case PacketTypeInvalid:
                destination = PACKET_TYPE_INVALID_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateR1PacketLevelReady:
                destination = PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateR2DteRestartRequest:
                destination = PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateR3DceRestartIndication:
                destination = PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP1Ready:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P1_READY_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP2DteWaiting:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP3DceWaiting:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP4DataTransfer:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP5CallCollision:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP6DteClearRequest:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateP7DceClearIndication:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateD1FlowControlReady:
                destination = PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateD2DteResetReady:
                destination = PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_DESCRIPTION;
                break;

            case PacketTypeInvalidForStateD3DceResetIndication:
                destination = PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_DESCRIPTION;
                break;

            case PacketNotAllowed:
                destination = PACKET_NOT_ALLOWED_DESCRIPTION;
                break;

            case UnidentifiablePacket:
                destination = UNIDENTIFIABLE_PACKET_DESCRIPTION;
                break;

            case CallOnOneWayLogicalChannel:
                destination = CALL_ON_ONE_WAY_LOGICAL_CHANNEL_DESCRIPTION;
                break;

            case InvalidPacketTypeOnAPermanentVirtualCircuit:
                destination = INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_DESCRIPTION;
                break;

            case PacketOnUnassignedLogicalChannelNumber:
                destination = PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_DESCRIPTION;
                break;

            case RejectNotSubscribedTo:
                destination = REJECT_NOT_SUBSCRIBED_TO_DESCRIPTION;
                break;

            case PacketTooShort:
                destination = PACKET_TOO_SHORT_DESCRIPTION;
                break;

            case PacketTooLong:
                destination = PACKET_TOO_LONG_DESCRIPTION;
                break;

            case InvalidGeneralFormatIdentifier:
                destination = INVALID_GENERAL_FORMAT_IDENTIFIER_DESCRIPTION;
                break;

            case RestartOrRegistrationPacketWithNonzeroLci:
                destination = RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_DESCRIPTION;
                break;

            case PacketTypeNotCompatibleWithFacility:
                destination = PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_DESCRIPTION;
                break;

            case UnauthorizedInterruptConfirmation:
                destination = UNAUTHORIZED_INTERRUPT_CONFIRMATION_DESCRIPTION;
                break;

            case UnauthorizedInterrupt:
                destination = UNAUTHORIZED_INTERRUPT_DESCRIPTION;
                break;

            case UnauthorizedReject:
                destination = UNAUTHORIZED_REJECT_DESCRIPTION;
                break;

            case TimerExpired:
                destination = TIMER_EXPIRED_DESCRIPTION;
                break;

            case TimerExpiredForIncomingCall:
                destination = TIMER_EXPIRED_FOR_INCOMING_CALL_DESCRIPTION;
                break;

            case TimerExpiredForClearIndication:
                destination = TIMER_EXPIRED_FOR_CLEAR_INDICATION_DESCRIPTION;
                break;

            case TimerExpiredForResetIndication:
                destination = TIMER_EXPIRED_FOR_RESET_INDICATION_DESCRIPTION;
                break;

            case TimerExpiredForRestartIndication:
                destination = TIMER_EXPIRED_FOR_RESTART_INDICATION_DESCRIPTION;
                break;

            case TimerExpiredForCallDeflection:
                destination = TIMER_EXPIRED_FOR_CALL_DEFLECTION_DESCRIPTION;
                break;

            case CallSetupClearingOrRegistrationProblem:
                destination = CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_DESCRIPTION;
                break;

            case FacilityCodeNotAllowed:
                destination = FACILITY_CODE_NOT_ALLOWED_DESCRIPTION;
                break;

            case FacilityParameterNotAllowed:
                destination = FACILITY_PARAMETER_NOT_ALLOWED_DESCRIPTION;
                break;

            case InvalidCalledAddress:
                destination = INVALID_CALLED_ADDRESS_DESCRIPTION;
                break;

            case InvalidCallingAddress:
                destination = INVALID_CALLING_ADDRESS_DESCRIPTION;
                break;

            case InvalidFacilityLength:
                destination = INVALID_FACILITY_LENGTH_DESCRIPTION;
                break;

            case IncomingCallBarred:
                destination = INCOMING_CALL_BARRED_DESCRIPTION;
                break;

            case NoLogicalChannelAvailable:
                destination = NO_LOGICAL_CHANNEL_AVAILABLE_DESCRIPTION;
                break;

            case CallCollision:
                destination = CALL_COLLISION_DESCRIPTION;
                break;

            case DuplicateFacilityRequested:
                destination = DUPLICATE_FACILITY_REQUESTED_DESCRIPTION;
                break;

            case NonzeroAddressLength:
                destination = NONZERO_ADDRESS_LENGTH_DESCRIPTION;
                break;

            case NonzeroFacilityLength:
                destination = NONZERO_FACILITY_LENGTH_DESCRIPTION;
                break;

            case FacilityNotProvidedWhenExpected:
                destination = FACILITY_NOT_PROVIDED_WHEN_EXPECTED_DESCRIPTION;
                break;

            case InvalidItuTSpecifiedDteFacility:
                destination = INVALID_ITU_T_SPECIFIED_DTE_FACILITY_DESCRIPTION;
                break;

            case MaximumNumberOfCallRedirectionsOrDeflectionsExceeded:
                destination = MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_DESCRIPTION;
                break;

            case Miscellaneous:
                destination = MISCELLANEOUS_DESCRIPTION;
                break;

            case ImproperCauseCodeForDte:
                destination = IMPROPER_CAUSE_CODE_FOR_DTE_DESCRIPTION;
                break;

            case OctetNotAaligned:
                destination = OCTET_NOT_ALIGNED_DESCRIPTION;
                break;

            case InconsistentQBitSetting:
                destination = INCONSISTENT_Q_BIT_SETTING_DESCRIPTION;
                break;

            case NetworkUserIdentificationProblem:
                destination = NETWORK_USER_IDENTIFICATION_PROBLEM_DESCRIPTION;
                break;

            case InternationalProblem:
                destination = INTERNATIONAL_PROBLEM_DESCRIPTION;
                break;

            case RemoteNetworkProblem:
                destination = REMOTE_NETWORK_PROBLEM_DESCRIPTION;
                break;

            case InternationalProtocolProblem:
                destination = INTERNATIONAL_PROTOCOL_PROBLEM_DESCRIPTION;
                break;

            case InternationalLinkOutOfOrder:
                destination = INTERNATIONAL_LINK_OUT_OF_ORDER_DESCRIPTION;
                break;

            case InternationalLinkBusy:
                destination = INTERNATIONAL_LINK_BUSY_DESCRIPTION;
                break;

            case TransitNetworkFacilityProblem:
                destination = TRANSIT_NETWORK_FACILITY_PROBLEM_DESCRIPTION;
                break;

            case RemoteNetworkFacilityProblem:
                destination = REMOTE_NETWORK_FACILITY_PROBLEM_DESCRIPTION;
                break;

            case InternationalRoutingProblem:
                destination = INTERNATIONAL_ROUTING_PROBLEM_DESCRIPTION;
                break;

            case TemporaryRoutingProblem:
                destination = TEMPORARY_ROUTING_PROBLEM_DESCRIPTION;
                break;

            case UnknownCalledDataNetworkIdentificationCode:
                destination = UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_DESCRIPTION;
                break;

            case MaintenanceActionClearX25VcCommandIssued:
                destination = MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_DESCRIPTION;
                break;

            case NormalTermination:
                destination = NORMAL_TERMINATION_DESCRIPTION;
                break;

            case OutOfResources:
                destination = OUT_OF_RESOURCES_DESCRIPTION;
                break;

            case AuthenticationFailure:
                destination = AUTHENTICATION_FAILURE_DESCRIPTION;
                break;

            case InboundUserDataTooLarge:
                destination = INBOUND_USER_DATA_TOO_LARGE_DESCRIPTION;
                break;

            case IdleTimerExpired:
                destination = IDLE_TIMER_EXPIRED_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static DiagnosticCodeEnumeration fromDescription(final String value) {

        final DiagnosticCodeEnumeration destination;
        switch (value) {
            case NO_ADDITIONAL_INFORMATION_DESCRIPTION:
                destination = NoAdditionalInformation;
                break;

            case INVALID_PACKET_SEND_SEQUENCE_NUMBER_DESCRIPTION:
                destination = InvalidPacketSendSequenceNumber;
                break;

            case INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_DESCRIPTION:
                destination = InvalidPacketReceiveSequenceNumber;
                break;

            case PACKET_TYPE_INVALID_DESCRIPTION:
                destination = PacketTypeInvalid;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_DESCRIPTION:
                destination = PacketTypeInvalidForStateR1PacketLevelReady;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_DESCRIPTION:
                destination = PacketTypeInvalidForStateR2DteRestartRequest;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_DESCRIPTION:
                destination = PacketTypeInvalidForStateR3DceRestartIndication;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P1_READY_DESCRIPTION:
                destination = PacketTypeInvalidForStateP1Ready;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_DESCRIPTION:
                destination = PacketTypeInvalidForStateP2DteWaiting;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_DESCRIPTION:
                destination = PacketTypeInvalidForStateP3DceWaiting;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_DESCRIPTION:
                destination = PacketTypeInvalidForStateP4DataTransfer;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_DESCRIPTION:
                destination = PacketTypeInvalidForStateP5CallCollision;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_DESCRIPTION:
                destination = PacketTypeInvalidForStateP6DteClearRequest;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_DESCRIPTION:
                destination = PacketTypeInvalidForStateP7DceClearIndication;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_DESCRIPTION:
                destination = PacketTypeInvalidForStateD1FlowControlReady;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_DESCRIPTION:
                destination = PacketTypeInvalidForStateD2DteResetReady;
                break;

            case PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_DESCRIPTION:
                destination = PacketTypeInvalidForStateD3DceResetIndication;
                break;

            case PACKET_NOT_ALLOWED_DESCRIPTION:
                destination = PacketNotAllowed;
                break;

            case UNIDENTIFIABLE_PACKET_DESCRIPTION:
                destination = UnidentifiablePacket;
                break;

            case CALL_ON_ONE_WAY_LOGICAL_CHANNEL_DESCRIPTION:
                destination = CallOnOneWayLogicalChannel;
                break;

            case INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_DESCRIPTION:
                destination = InvalidPacketTypeOnAPermanentVirtualCircuit;
                break;

            case PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_DESCRIPTION:
                destination = PacketOnUnassignedLogicalChannelNumber;
                break;

            case REJECT_NOT_SUBSCRIBED_TO_DESCRIPTION:
                destination = RejectNotSubscribedTo;
                break;

            case PACKET_TOO_SHORT_DESCRIPTION:
                destination = PacketTooShort;
                break;

            case PACKET_TOO_LONG_DESCRIPTION:
                destination = PacketTooLong;
                break;

            case INVALID_GENERAL_FORMAT_IDENTIFIER_DESCRIPTION:
                destination = InvalidGeneralFormatIdentifier;
                break;

            case RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_DESCRIPTION:
                destination = RestartOrRegistrationPacketWithNonzeroLci;
                break;

            case PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_DESCRIPTION:
                destination = PacketTypeNotCompatibleWithFacility;
                break;

            case UNAUTHORIZED_INTERRUPT_CONFIRMATION_DESCRIPTION:
                destination = UnauthorizedInterruptConfirmation;
                break;

            case UNAUTHORIZED_INTERRUPT_DESCRIPTION:
                destination = UnauthorizedInterrupt;
                break;

            case UNAUTHORIZED_REJECT_DESCRIPTION:
                destination = UnauthorizedReject;
                break;

            case TIMER_EXPIRED_DESCRIPTION:
                destination = TimerExpired;
                break;

            case TIMER_EXPIRED_FOR_INCOMING_CALL_DESCRIPTION:
                destination = TimerExpiredForIncomingCall;
                break;

            case TIMER_EXPIRED_FOR_CLEAR_INDICATION_DESCRIPTION:
                destination = TimerExpiredForClearIndication;
                break;

            case TIMER_EXPIRED_FOR_RESET_INDICATION_DESCRIPTION:
                destination = TimerExpiredForResetIndication;
                break;

            case TIMER_EXPIRED_FOR_RESTART_INDICATION_DESCRIPTION:
                destination = TimerExpiredForRestartIndication;
                break;

            case TIMER_EXPIRED_FOR_CALL_DEFLECTION_DESCRIPTION:
                destination = TimerExpiredForCallDeflection;
                break;

            case CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_DESCRIPTION:
                destination = CallSetupClearingOrRegistrationProblem;
                break;

            case FACILITY_CODE_NOT_ALLOWED_DESCRIPTION:
                destination = FacilityCodeNotAllowed;
                break;

            case FACILITY_PARAMETER_NOT_ALLOWED_DESCRIPTION:
                destination = FacilityParameterNotAllowed;
                break;

            case INVALID_CALLED_ADDRESS_DESCRIPTION:
                destination = InvalidCalledAddress;
                break;

            case INVALID_CALLING_ADDRESS_DESCRIPTION:
                destination = InvalidCallingAddress;
                break;

            case INVALID_FACILITY_LENGTH_DESCRIPTION:
                destination = InvalidFacilityLength;
                break;

            case INCOMING_CALL_BARRED_DESCRIPTION:
                destination = IncomingCallBarred;
                break;

            case NO_LOGICAL_CHANNEL_AVAILABLE_DESCRIPTION:
                destination = NoLogicalChannelAvailable;
                break;

            case CALL_COLLISION_DESCRIPTION:
                destination = CallCollision;
                break;

            case DUPLICATE_FACILITY_REQUESTED_DESCRIPTION:
                destination = DuplicateFacilityRequested;
                break;

            case NONZERO_ADDRESS_LENGTH_DESCRIPTION:
                destination = NonzeroAddressLength;
                break;

            case NONZERO_FACILITY_LENGTH_DESCRIPTION:
                destination = NonzeroFacilityLength;
                break;

            case FACILITY_NOT_PROVIDED_WHEN_EXPECTED_DESCRIPTION:
                destination = FacilityNotProvidedWhenExpected;
                break;

            case INVALID_ITU_T_SPECIFIED_DTE_FACILITY_DESCRIPTION:
                destination = InvalidItuTSpecifiedDteFacility;
                break;

            case MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_DESCRIPTION:
                destination = MaximumNumberOfCallRedirectionsOrDeflectionsExceeded;
                break;

            case MISCELLANEOUS_DESCRIPTION:
                destination = Miscellaneous;
                break;

            case IMPROPER_CAUSE_CODE_FOR_DTE_DESCRIPTION:
                destination = ImproperCauseCodeForDte;
                break;

            case OCTET_NOT_ALIGNED_DESCRIPTION:
                destination = OctetNotAaligned;
                break;

            case INCONSISTENT_Q_BIT_SETTING_DESCRIPTION:
                destination = InconsistentQBitSetting;
                break;

            case NETWORK_USER_IDENTIFICATION_PROBLEM_DESCRIPTION:
                destination = NetworkUserIdentificationProblem;
                break;

            case INTERNATIONAL_PROBLEM_DESCRIPTION:
                destination = InternationalProblem;
                break;

            case REMOTE_NETWORK_PROBLEM_DESCRIPTION:
                destination = RemoteNetworkProblem;
                break;

            case INTERNATIONAL_PROTOCOL_PROBLEM_DESCRIPTION:
                destination = InternationalProtocolProblem;
                break;

            case INTERNATIONAL_LINK_OUT_OF_ORDER_DESCRIPTION:
                destination = InternationalLinkOutOfOrder;
                break;

            case INTERNATIONAL_LINK_BUSY_DESCRIPTION:
                destination = InternationalLinkBusy;
                break;

            case TRANSIT_NETWORK_FACILITY_PROBLEM_DESCRIPTION:
                destination = TransitNetworkFacilityProblem;
                break;

            case REMOTE_NETWORK_FACILITY_PROBLEM_DESCRIPTION:
                destination = RemoteNetworkFacilityProblem;
                break;

            case INTERNATIONAL_ROUTING_PROBLEM_DESCRIPTION:
                destination = InternationalRoutingProblem;
                break;

            case TEMPORARY_ROUTING_PROBLEM_DESCRIPTION:
                destination = TemporaryRoutingProblem;
                break;

            case UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_DESCRIPTION:
                destination = UnknownCalledDataNetworkIdentificationCode;
                break;

            case MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_DESCRIPTION:
                destination = MaintenanceActionClearX25VcCommandIssued;
                break;

            case NORMAL_TERMINATION_DESCRIPTION:
                destination = NormalTermination;
                break;

            case OUT_OF_RESOURCES_DESCRIPTION:
                destination = OutOfResources;
                break;

            case AUTHENTICATION_FAILURE_DESCRIPTION:
                destination = AuthenticationFailure;
                break;

            case INBOUND_USER_DATA_TOO_LARGE_DESCRIPTION:
                destination = InboundUserDataTooLarge;
                break;

            case IDLE_TIMER_EXPIRED_DESCRIPTION:
                destination = IdleTimerExpired;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }

    public int toInt() {

        int destination;

        switch (this) {
            case NoAdditionalInformation:
                destination = NO_ADDITIONAL_INFORMATION_MASK;
                break;

            case InvalidPacketSendSequenceNumber:
                destination = INVALID_PACKET_SEND_SEQUENCE_NUMBER_MASK;
                break;

            case InvalidPacketReceiveSequenceNumber:
                destination = INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_MASK;
                break;

            case PacketTypeInvalid:
                destination = PACKET_TYPE_INVALID_MASK;
                break;

            case PacketTypeInvalidForStateR1PacketLevelReady:
                destination = PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_MASK;
                break;

            case PacketTypeInvalidForStateR2DteRestartRequest:
                destination = PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_MASK;
                break;

            case PacketTypeInvalidForStateR3DceRestartIndication:
                destination = PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_MASK;
                break;

            case PacketTypeInvalidForStateP1Ready:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P1_READY_MASK;
                break;

            case PacketTypeInvalidForStateP2DteWaiting:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_MASK;
                break;

            case PacketTypeInvalidForStateP3DceWaiting:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_MASK;
                break;

            case PacketTypeInvalidForStateP4DataTransfer:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_MASK;
                break;

            case PacketTypeInvalidForStateP5CallCollision:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_MASK;
                break;

            case PacketTypeInvalidForStateP6DteClearRequest:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_MASK;
                break;

            case PacketTypeInvalidForStateP7DceClearIndication:
                destination = PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_MASK;
                break;

            case PacketTypeInvalidForStateD1FlowControlReady:
                destination = PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_MASK;
                break;

            case PacketTypeInvalidForStateD2DteResetReady:
                destination = PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_MASK;
                break;

            case PacketTypeInvalidForStateD3DceResetIndication:
                destination = PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_MASK;
                break;

            case PacketNotAllowed:
                destination = PACKET_NOT_ALLOWED_MASK;
                break;

            case UnidentifiablePacket:
                destination = UNIDENTIFIABLE_PACKET_MASK;
                break;

            case CallOnOneWayLogicalChannel:
                destination = CALL_ON_ONE_WAY_LOGICAL_CHANNEL_MASK;
                break;

            case InvalidPacketTypeOnAPermanentVirtualCircuit:
                destination = INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_MASK;
                break;

            case PacketOnUnassignedLogicalChannelNumber:
                destination = PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_MASK;
                break;

            case RejectNotSubscribedTo:
                destination = REJECT_NOT_SUBSCRIBED_TO_MASK;
                break;

            case PacketTooShort:
                destination = PACKET_TOO_SHORT_MASK;
                break;

            case PacketTooLong:
                destination = PACKET_TOO_LONG_MASK;
                break;

            case InvalidGeneralFormatIdentifier:
                destination = INVALID_GENERAL_FORMAT_IDENTIFIER_MASK;
                break;

            case RestartOrRegistrationPacketWithNonzeroLci:
                destination = RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_MASK;
                break;

            case PacketTypeNotCompatibleWithFacility:
                destination = PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_MASK;
                break;

            case UnauthorizedInterruptConfirmation:
                destination = UNAUTHORIZED_INTERRUPT_CONFIRMATION_MASK;
                break;

            case UnauthorizedInterrupt:
                destination = UNAUTHORIZED_INTERRUPT_MASK;
                break;

            case UnauthorizedReject:
                destination = UNAUTHORIZED_REJECT_MASK;
                break;

            case TimerExpired:
                destination = TIMER_EXPIRED_MASK;
                break;

            case TimerExpiredForIncomingCall:
                destination = TIMER_EXPIRED_FOR_INCOMING_CALL_MASK;
                break;

            case TimerExpiredForClearIndication:
                destination = TIMER_EXPIRED_FOR_CLEAR_INDICATION_MASK;
                break;

            case TimerExpiredForResetIndication:
                destination = TIMER_EXPIRED_FOR_RESET_INDICATION_MASK;
                break;

            case TimerExpiredForRestartIndication:
                destination = TIMER_EXPIRED_FOR_RESTART_INDICATION_MASK;
                break;

            case TimerExpiredForCallDeflection:
                destination = TIMER_EXPIRED_FOR_CALL_DEFLECTION_MASK;
                break;

            case CallSetupClearingOrRegistrationProblem:
                destination = CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_MASK;
                break;

            case FacilityCodeNotAllowed:
                destination = FACILITY_CODE_NOT_ALLOWED_MASK;
                break;

            case FacilityParameterNotAllowed:
                destination = FACILITY_PARAMETER_NOT_ALLOWED_MASK;
                break;

            case InvalidCalledAddress:
                destination = INVALID_CALLED_ADDRESS_MASK;
                break;

            case InvalidCallingAddress:
                destination = INVALID_CALLING_ADDRESS_MASK;
                break;

            case InvalidFacilityLength:
                destination = INVALID_FACILITY_LENGTH_MASK;
                break;

            case IncomingCallBarred:
                destination = INCOMING_CALL_BARRED_MASK;
                break;

            case NoLogicalChannelAvailable:
                destination = NO_LOGICAL_CHANNEL_AVAILABLE_MASK;
                break;

            case CallCollision:
                destination = CALL_COLLISION_MASK;
                break;

            case DuplicateFacilityRequested:
                destination = DUPLICATE_FACILITY_REQUESTED_MASK;
                break;

            case NonzeroAddressLength:
                destination = NONZERO_ADDRESS_LENGTH_MASK;
                break;

            case NonzeroFacilityLength:
                destination = NONZERO_FACILITY_LENGTH_MASK;
                break;

            case FacilityNotProvidedWhenExpected:
                destination = FACILITY_NOT_PROVIDED_WHEN_EXPECTED_MASK;
                break;

            case InvalidItuTSpecifiedDteFacility:
                destination = INVALID_ITU_T_SPECIFIED_DTE_FACILITY_MASK;
                break;

            case MaximumNumberOfCallRedirectionsOrDeflectionsExceeded:
                destination = MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_MASK;
                break;

            case Miscellaneous:
                destination = MISCELLANEOUS_MASK;
                break;

            case ImproperCauseCodeForDte:
                destination = IMPROPER_CAUSE_CODE_FOR_DTE_MASK;
                break;

            case OctetNotAaligned:
                destination = OCTET_NOT_ALIGNED_MASK;
                break;

            case InconsistentQBitSetting:
                destination = INCONSISTENT_Q_BIT_SETTING_MASK;
                break;

            case NetworkUserIdentificationProblem:
                destination = NETWORK_USER_IDENTIFICATION_PROBLEM_MASK;
                break;

            case InternationalProblem:
                destination = INTERNATIONAL_PROBLEM_MASK;
                break;

            case RemoteNetworkProblem:
                destination = REMOTE_NETWORK_PROBLEM_MASK;
                break;

            case InternationalProtocolProblem:
                destination = INTERNATIONAL_PROTOCOL_PROBLEM_MASK;
                break;

            case InternationalLinkOutOfOrder:
                destination = INTERNATIONAL_LINK_OUT_OF_ORDER_MASK;
                break;

            case InternationalLinkBusy:
                destination = INTERNATIONAL_LINK_BUSY_MASK;
                break;

            case TransitNetworkFacilityProblem:
                destination = TRANSIT_NETWORK_FACILITY_PROBLEM_MASK;
                break;

            case RemoteNetworkFacilityProblem:
                destination = REMOTE_NETWORK_FACILITY_PROBLEM_MASK;
                break;

            case InternationalRoutingProblem:
                destination = INTERNATIONAL_ROUTING_PROBLEM_MASK;
                break;

            case TemporaryRoutingProblem:
                destination = TEMPORARY_ROUTING_PROBLEM_MASK;
                break;

            case UnknownCalledDataNetworkIdentificationCode:
                destination = UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_MASK;
                break;

            case MaintenanceActionClearX25VcCommandIssued:
                destination = MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_MASK;
                break;

            case NormalTermination:
                destination = NORMAL_TERMINATION_MASK;
                break;

            case OutOfResources:
                destination = OUT_OF_RESOURCES_MASK;
                break;

            case AuthenticationFailure:
                destination = AUTHENTICATION_FAILURE_MASK;
                break;

            case InboundUserDataTooLarge:
                destination = INBOUND_USER_DATA_TOO_LARGE_MASK;
                break;

            case IdleTimerExpired:
                destination = IDLE_TIMER_EXPIRED_MASK;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);

        }

        return destination;
    }

    public static DiagnosticCodeEnumeration fromInt(final int value) {

        final DiagnosticCodeEnumeration destination;

        if (compareWithMask(NO_ADDITIONAL_INFORMATION_MASK, value)) {
            destination = NoAdditionalInformation;

        } else if (compareWithMask(INVALID_PACKET_SEND_SEQUENCE_NUMBER_MASK, value)) {
            destination = InvalidPacketSendSequenceNumber;

        } else if (compareWithMask(INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_MASK, value)) {
            destination = InvalidPacketReceiveSequenceNumber;

        } else if (compareWithMask(PACKET_TYPE_INVALID_MASK, value)) {
            destination = PacketTypeInvalid;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_MASK, value)) {
            destination = PacketTypeInvalidForStateR1PacketLevelReady;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_MASK, value)) {
            destination = PacketTypeInvalidForStateR2DteRestartRequest;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_MASK, value)) {
            destination = PacketTypeInvalidForStateR3DceRestartIndication;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P1_READY_MASK, value)) {
            destination = PacketTypeInvalidForStateP1Ready;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_MASK, value)) {
            destination = PacketTypeInvalidForStateP2DteWaiting;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_MASK, value)) {
            destination = PacketTypeInvalidForStateP3DceWaiting;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_MASK, value)) {
            destination = PacketTypeInvalidForStateP4DataTransfer;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_MASK, value)) {
            destination = PacketTypeInvalidForStateP5CallCollision;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_MASK, value)) {
            destination = PacketTypeInvalidForStateP6DteClearRequest;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_MASK, value)) {
            destination = PacketTypeInvalidForStateP7DceClearIndication;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_MASK, value)) {
            destination = PacketTypeInvalidForStateD1FlowControlReady;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_MASK, value)) {
            destination = PacketTypeInvalidForStateD2DteResetReady;

        } else if (compareWithMask(PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_MASK, value)) {
            destination = PacketTypeInvalidForStateD3DceResetIndication;

        } else if (compareWithMask(PACKET_NOT_ALLOWED_MASK, value)) {
            destination = PacketNotAllowed;

        } else if (compareWithMask(UNIDENTIFIABLE_PACKET_MASK, value)) {
            destination = UnidentifiablePacket;

        } else if (compareWithMask(CALL_ON_ONE_WAY_LOGICAL_CHANNEL_MASK, value)) {
            destination = CallOnOneWayLogicalChannel;

        } else if (compareWithMask(INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_MASK, value)) {
            destination = InvalidPacketTypeOnAPermanentVirtualCircuit;

        } else if (compareWithMask(PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_MASK, value)) {
            destination = PacketOnUnassignedLogicalChannelNumber;

        } else if (compareWithMask(REJECT_NOT_SUBSCRIBED_TO_MASK, value)) {
            destination = RejectNotSubscribedTo;

        } else if (compareWithMask(PACKET_TOO_SHORT_MASK, value)) {
            destination = PacketTooShort;

        } else if (compareWithMask(PACKET_TOO_LONG_MASK, value)) {
            destination = PacketTooLong;

        } else if (compareWithMask(INVALID_GENERAL_FORMAT_IDENTIFIER_MASK, value)) {
            destination = InvalidGeneralFormatIdentifier;

        } else if (compareWithMask(RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_MASK, value)) {
            destination = RestartOrRegistrationPacketWithNonzeroLci;

        } else if (compareWithMask(PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_MASK, value)) {
            destination = PacketTypeNotCompatibleWithFacility;

        } else if (compareWithMask(UNAUTHORIZED_INTERRUPT_CONFIRMATION_MASK, value)) {
            destination = UnauthorizedInterruptConfirmation;

        } else if (compareWithMask(UNAUTHORIZED_INTERRUPT_MASK, value)) {
            destination = UnauthorizedInterrupt;

        } else if (compareWithMask(UNAUTHORIZED_REJECT_MASK, value)) {
            destination = UnauthorizedReject;

        } else if (compareWithMask(TIMER_EXPIRED_MASK, value)) {
            destination = TimerExpired;

        } else if (compareWithMask(TIMER_EXPIRED_FOR_INCOMING_CALL_MASK, value)) {
            destination = TimerExpiredForIncomingCall;

        } else if (compareWithMask(TIMER_EXPIRED_FOR_CLEAR_INDICATION_MASK, value)) {
            destination = TimerExpiredForClearIndication;

        } else if (compareWithMask(TIMER_EXPIRED_FOR_RESET_INDICATION_MASK, value)) {
            destination = TimerExpiredForResetIndication;

        } else if (compareWithMask(TIMER_EXPIRED_FOR_RESTART_INDICATION_MASK, value)) {
            destination = TimerExpiredForRestartIndication;

        } else if (compareWithMask(TIMER_EXPIRED_FOR_CALL_DEFLECTION_MASK, value)) {
            destination = TimerExpiredForCallDeflection;

        } else if (compareWithMask(CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_MASK, value)) {
            destination = CallSetupClearingOrRegistrationProblem;

        } else if (compareWithMask(FACILITY_CODE_NOT_ALLOWED_MASK, value)) {
            destination = FacilityCodeNotAllowed;

        } else if (compareWithMask(FACILITY_PARAMETER_NOT_ALLOWED_MASK, value)) {
            destination = FacilityParameterNotAllowed;

        } else if (compareWithMask(INVALID_CALLED_ADDRESS_MASK, value)) {
            destination = InvalidCalledAddress;

        } else if (compareWithMask(INVALID_CALLING_ADDRESS_MASK, value)) {
            destination = InvalidCallingAddress;

        } else if (compareWithMask(INVALID_FACILITY_LENGTH_MASK, value)) {
            destination = InvalidFacilityLength;

        } else if (compareWithMask(INCOMING_CALL_BARRED_MASK, value)) {
            destination = IncomingCallBarred;

        } else if (compareWithMask(NO_LOGICAL_CHANNEL_AVAILABLE_MASK, value)) {
            destination = NoLogicalChannelAvailable;

        } else if (compareWithMask(CALL_COLLISION_MASK, value)) {
            destination = CallCollision;

        } else if (compareWithMask(DUPLICATE_FACILITY_REQUESTED_MASK, value)) {
            destination = DuplicateFacilityRequested;

        } else if (compareWithMask(NONZERO_ADDRESS_LENGTH_MASK, value)) {
            destination = NonzeroAddressLength;

        } else if (compareWithMask(NONZERO_FACILITY_LENGTH_MASK, value)) {
            destination = NonzeroFacilityLength;

        } else if (compareWithMask(FACILITY_NOT_PROVIDED_WHEN_EXPECTED_MASK, value)) {
            destination = FacilityNotProvidedWhenExpected;

        } else if (compareWithMask(INVALID_ITU_T_SPECIFIED_DTE_FACILITY_MASK, value)) {
            destination = InvalidItuTSpecifiedDteFacility;

        } else if (compareWithMask(MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_MASK, value)) {
            destination = MaximumNumberOfCallRedirectionsOrDeflectionsExceeded;

        } else if (compareWithMask(MISCELLANEOUS_MASK, value)) {
            destination = Miscellaneous;

        } else if (compareWithMask(IMPROPER_CAUSE_CODE_FOR_DTE_MASK, value)) {
            destination = ImproperCauseCodeForDte;

        } else if (compareWithMask(OCTET_NOT_ALIGNED_MASK, value)) {
            destination = OctetNotAaligned;

        } else if (compareWithMask(INCONSISTENT_Q_BIT_SETTING_MASK, value)) {
            destination = InconsistentQBitSetting;

        } else if (compareWithMask(NETWORK_USER_IDENTIFICATION_PROBLEM_MASK, value)) {
            destination = NetworkUserIdentificationProblem;

        } else if (compareWithMask(INTERNATIONAL_PROBLEM_MASK, value)) {
            destination = InternationalProblem;

        } else if (compareWithMask(REMOTE_NETWORK_PROBLEM_MASK, value)) {
            destination = RemoteNetworkProblem;

        } else if (compareWithMask(INTERNATIONAL_PROTOCOL_PROBLEM_MASK, value)) {
            destination = InternationalProtocolProblem;

        } else if (compareWithMask(INTERNATIONAL_LINK_OUT_OF_ORDER_MASK, value)) {
            destination = InternationalLinkOutOfOrder;

        } else if (compareWithMask(INTERNATIONAL_LINK_BUSY_MASK, value)) {
            destination = InternationalLinkBusy;

        } else if (compareWithMask(TRANSIT_NETWORK_FACILITY_PROBLEM_MASK, value)) {
            destination = TransitNetworkFacilityProblem;

        } else if (compareWithMask(REMOTE_NETWORK_FACILITY_PROBLEM_MASK, value)) {
            destination = RemoteNetworkFacilityProblem;

        } else if (compareWithMask(INTERNATIONAL_ROUTING_PROBLEM_MASK, value)) {
            destination = InternationalRoutingProblem;

        } else if (compareWithMask(TEMPORARY_ROUTING_PROBLEM_MASK, value)) {
            destination = TemporaryRoutingProblem;

        } else if (compareWithMask(UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_MASK, value)) {
            destination = UnknownCalledDataNetworkIdentificationCode;

        } else if (compareWithMask(MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_MASK, value)) {
            destination = MaintenanceActionClearX25VcCommandIssued;

        } else if (compareWithMask(NORMAL_TERMINATION_MASK, value)) {
            destination = NormalTermination;

        } else if (compareWithMask(OUT_OF_RESOURCES_MASK, value)) {
            destination = OutOfResources;

        } else if (compareWithMask(AUTHENTICATION_FAILURE_MASK, value)) {
            destination = AuthenticationFailure;

        } else if (compareWithMask(INBOUND_USER_DATA_TOO_LARGE_MASK, value)) {
            destination = InboundUserDataTooLarge;

        } else if (compareWithMask(IDLE_TIMER_EXPIRED_MASK, value)) {
            destination = IdleTimerExpired;

        } else {
            throw new RuntimeException("Unhandled value: " + value);

        }

        return destination;
    }
}
