//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

public final class DiagnosticCodeEnumerationUtility {

    // DESCRIPTION
    public static final String NO_ADDITIONAL_INFORMATION_DESCRIPTION = "No_additional_information";
    public static final String INVALID_PACKET_SEND_SEQUENCE_NUMBER_DESCRIPTION = "Invalid_P(S)_(Packet_Send_sequence_number)";
    public static final String INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_DESCRIPTION = "Invalid_P(R)_(Packet_Receive_sequence_number)";
    public static final String PACKET_TYPE_INVALID_DESCRIPTION = "Packet_type_invalid";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_DESCRIPTION = "Packet_type_invalid_for_state_R1_(Packet_level_ready)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_DESCRIPTION = "Packet_type_invalid_for_state_R2_(DTE_restart_request)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_DESCRIPTION = "Packet_type_invalid_for_state_R3_(DCE_restart_indication)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P1_READY_DESCRIPTION = "Packet_type_invalid_for_state_P1_(Ready)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_DESCRIPTION = "Packet_type_invalid_for_state_P2_(DTE_Waiting)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_DESCRIPTION = "Packet_type_invalid_for_state_P3_(DCE_Waiting)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_DESCRIPTION = "Packet_type_invalid_for_state_P4_(Data_Transfer)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_DESCRIPTION = "Packet_type_invalid_for_state_P5_(Call_Collision)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_DESCRIPTION = "Packet_type_invalid_for_state_P6_(DTE_clear_request)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_DESCRIPTION = "Packet_type_invalid_for_state_P7_(DCE_clear_indication)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_DESCRIPTION = "Packet_type_invalid_for_state_D1_(Flow_control_ready)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_DESCRIPTION = "Packet_type_invalid_for_state_D2_(DTE_reset_ready)";
    public static final String PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_DESCRIPTION = "Packet_type_invalid_for_state_D3_(DCE_reset_indication)";
    public static final String PACKET_NOT_ALLOWED_DESCRIPTION = "Packet_not_allowed";
    public static final String UNIDENTIFIABLE_PACKET_DESCRIPTION = "Unidentifiable_packet";
    public static final String CALL_ON_ONE_WAY_LOGICAL_CHANNEL_DESCRIPTION = "Call_on_one-way_logical_channel";
    public static final String INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_DESCRIPTION = "Invalid_packet_type_on_a_permanent_virtual_circuit";
    public static final String PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_DESCRIPTION = "Packet_on_unassigned_LCN_(Logical_Channel_Number)";
    public static final String REJECT_NOT_SUBSCRIBED_TO_DESCRIPTION = "Reject_not_subscribed_to";
    public static final String PACKET_TOO_SHORT_DESCRIPTION = "Packet_too_short";
    public static final String PACKET_TOO_LONG_DESCRIPTION = "Packet_too_long";
    public static final String INVALID_GENERAL_FORMAT_IDENTIFIER_DESCRIPTION = "Invalid_GFI_(General_Format_Identifier)";
    public static final String RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_DESCRIPTION = "Restart_or_registration_packet_with_nonzero_LCI_(bits_1-4_of_octet_1,_or_bits_1-8_of_octet_2)";
    public static final String PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_DESCRIPTION = "Packet_type_not_compatible_with_facility";
    public static final String UNAUTHORIZED_INTERRUPT_CONFIRMATION_DESCRIPTION = "Unauthorized_interrupt_confirmation";
    public static final String UNAUTHORIZED_INTERRUPT_DESCRIPTION = "Unauthorized_interrupt";
    public static final String UNAUTHORIZED_REJECT_DESCRIPTION = "Unauthorized_reject";
    public static final String TIMER_EXPIRED_DESCRIPTION = "Timer_expired";
    public static final String TIMER_EXPIRED_FOR_INCOMING_CALL_DESCRIPTION = "Timer_expired_for_incoming_call";
    public static final String TIMER_EXPIRED_FOR_CLEAR_INDICATION_DESCRIPTION = "Timer_expired_for_clear_indication";
    public static final String TIMER_EXPIRED_FOR_RESET_INDICATION_DESCRIPTION = "Timer_expired_for_reset_indication";
    public static final String TIMER_EXPIRED_FOR_RESTART_INDICATION_DESCRIPTION = "Timer_expired_for_restart_indication";
    public static final String TIMER_EXPIRED_FOR_CALL_DEFLECTION_DESCRIPTION = "Timer_expired_for_call_deflection";
    public static final String CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_DESCRIPTION = "Call_setup,_clearing,_or_registration_problem";
    public static final String FACILITY_CODE_NOT_ALLOWED_DESCRIPTION = "Facility_code_not_allowed";
    public static final String FACILITY_PARAMETER_NOT_ALLOWED_DESCRIPTION = "Facility_parameter_not_allowed";
    public static final String INVALID_CALLED_ADDRESS_DESCRIPTION = "Invalid_called_address";
    public static final String INVALID_CALLING_ADDRESS_DESCRIPTION = "Invalid_calling_address";
    public static final String INVALID_FACILITY_LENGTH_DESCRIPTION = "Invalid_facility_length";
    public static final String INCOMING_CALL_BARRED_DESCRIPTION = "Incoming_call_barred";
    public static final String NO_LOGICAL_CHANNEL_AVAILABLE_DESCRIPTION = "No_logical_channel_available";
    public static final String CALL_COLLISION_DESCRIPTION = "Call_collision";
    public static final String DUPLICATE_FACILITY_REQUESTED_DESCRIPTION = "Duplicate_facility_requested";
    public static final String NONZERO_ADDRESS_LENGTH_DESCRIPTION = "Nonzero_address_length";
    public static final String NONZERO_FACILITY_LENGTH_DESCRIPTION = "Nonzero_facility_length";
    public static final String FACILITY_NOT_PROVIDED_WHEN_EXPECTED_DESCRIPTION = "Facility_not_provided_when_expected";
    public static final String INVALID_ITU_T_SPECIFIED_DTE_FACILITY_DESCRIPTION = "Invalid_ITU-T-specified_DTE_facility";
    public static final String MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_DESCRIPTION = "Maximum_number_of_call_redirections_or_deflections_exceeded";
    public static final String MISCELLANEOUS_DESCRIPTION = "Miscellaneous";
    public static final String IMPROPER_CAUSE_CODE_FOR_DTE_DESCRIPTION = "Improper_cause_code_for_DTE";
    public static final String OCTET_NOT_ALIGNED_DESCRIPTION = "Octet_not_aligned";
    public static final String INCONSISTENT_Q_BIT_SETTING_DESCRIPTION = "Inconsistent_Q_bit_setting";
    public static final String NETWORK_USER_IDENTIFICATION_PROBLEM_DESCRIPTION = "NUI_(Network_User_Identification)_problem";
    public static final String INTERNATIONAL_PROBLEM_DESCRIPTION = "International_problem";
    public static final String REMOTE_NETWORK_PROBLEM_DESCRIPTION = "Remote_network_problem";
    public static final String INTERNATIONAL_PROTOCOL_PROBLEM_DESCRIPTION = "International_protocol_problem";
    public static final String INTERNATIONAL_LINK_OUT_OF_ORDER_DESCRIPTION = "International_link_out_of_order";
    public static final String INTERNATIONAL_LINK_BUSY_DESCRIPTION = "International_link_busy";
    public static final String TRANSIT_NETWORK_FACILITY_PROBLEM_DESCRIPTION = "Transit_network_facility_problem";
    public static final String REMOTE_NETWORK_FACILITY_PROBLEM_DESCRIPTION = "Remote_network_facility_problem";
    public static final String INTERNATIONAL_ROUTING_PROBLEM_DESCRIPTION = "International_routing_problem";
    public static final String TEMPORARY_ROUTING_PROBLEM_DESCRIPTION = "Temporary_routing_problem";
    public static final String UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_DESCRIPTION = "Unknown_called_DNIC_(Data_Network_Identification_Code)";
    public static final String MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_DESCRIPTION = "Maintenance_action_(clear_x25_vc_command_issued)";
    //
    public static final String NORMAL_TERMINATION_DESCRIPTION = "Normal_termination";
    public static final String OUT_OF_RESOURCES_DESCRIPTION = "Out_of_resources";
    public static final String AUTHENTICATION_FAILURE_DESCRIPTION = "Authentication_failure";
    public static final String INBOUND_USER_DATA_TOO_LARGE_DESCRIPTION = "Inbound_user_data_too_large";
    public static final String IDLE_TIMER_EXPIRED_DESCRIPTION = "Idle_timer_expired";
    // MASK
    public static final int NO_ADDITIONAL_INFORMATION_MASK = 0x00;
    public static final int INVALID_PACKET_SEND_SEQUENCE_NUMBER_MASK = 0x01;
    public static final int INVALID_PACKET_RECEIVE_SEQUENCE_NUMBER_MASK = 0x02;
    public static final int PACKET_TYPE_INVALID_MASK = 0x10;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_R1_PACKET_LEVEL_READY_MASK = 0x11;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_R2_DTE_RESTART_REQUEST_MASK = 0x12;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_R3_DCE_RESTART_INDICATION_MASK = 0x13;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P1_READY_MASK = 0x14;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P2_DTE_WAITING_MASK = 0x15;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P3_DCE_WAITING_MASK = 0x16;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P4_DATA_TRANSFER_MASK = 0x17;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P5_CALL_COLLISION_MASK = 0x18;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P6_DTE_CLEAR_REQUEST_MASK = 0x19;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_P7_DCE_CLEAR_INDICATION_MASK = 0x1a;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_D1_FLOW_CONTROL_READY_MASK = 0x1b;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_D2_DTE_RESET_READY_MASK = 0x1c;
    public static final int PACKET_TYPE_INVALID_FOR_STATE_D3_DCE_RESET_INDICATION_MASK = 0x1d;
    public static final int PACKET_NOT_ALLOWED_MASK = 0x20;
    public static final int UNIDENTIFIABLE_PACKET_MASK = 0x21;
    public static final int CALL_ON_ONE_WAY_LOGICAL_CHANNEL_MASK = 0x22;
    public static final int INVALID_PACKET_TYPE_ON_A_PERMANENT_VIRTUAL_CIRCUIT_MASK = 0x23;
    public static final int PACKET_ON_UNASSIGNED_LOGICAL_CHANNEL_NUMBER_MASK = 0x24;
    public static final int REJECT_NOT_SUBSCRIBED_TO_MASK = 0x25;
    public static final int PACKET_TOO_SHORT_MASK = 0x26;
    public static final int PACKET_TOO_LONG_MASK = 0x27;
    public static final int INVALID_GENERAL_FORMAT_IDENTIFIER_MASK = 0x28;
    public static final int RESTART_OR_REGISTRATION_PACKET_WITH_NONZERO_LCI_MASK = 0x29;
    public static final int PACKET_TYPE_NOT_COMPATIBLE_WITH_FACILITY_MASK = 0x2a;
    public static final int UNAUTHORIZED_INTERRUPT_CONFIRMATION_MASK = 0x2b;
    public static final int UNAUTHORIZED_INTERRUPT_MASK = 0x2c;
    public static final int UNAUTHORIZED_REJECT_MASK = 0x2d;
    public static final int TIMER_EXPIRED_MASK = 0x30;
    public static final int TIMER_EXPIRED_FOR_INCOMING_CALL_MASK = 0x31;
    public static final int TIMER_EXPIRED_FOR_CLEAR_INDICATION_MASK = 0x32;
    public static final int TIMER_EXPIRED_FOR_RESET_INDICATION_MASK = 0x33;
    public static final int TIMER_EXPIRED_FOR_RESTART_INDICATION_MASK = 0x34;
    public static final int TIMER_EXPIRED_FOR_CALL_DEFLECTION_MASK = 0x35;
    public static final int CALL_SETUP_CLEARING_OR_REGISTRATION_PROBLEM_MASK = 0x40;
    public static final int FACILITY_CODE_NOT_ALLOWED_MASK = 0x41;
    public static final int FACILITY_PARAMETER_NOT_ALLOWED_MASK = 0x42;
    public static final int INVALID_CALLED_ADDRESS_MASK = 0x43;
    public static final int INVALID_CALLING_ADDRESS_MASK = 0x44;
    public static final int INVALID_FACILITY_LENGTH_MASK = 0x45;
    public static final int INCOMING_CALL_BARRED_MASK = 0x46;
    public static final int NO_LOGICAL_CHANNEL_AVAILABLE_MASK = 0x47;
    public static final int CALL_COLLISION_MASK = 0x48;
    public static final int DUPLICATE_FACILITY_REQUESTED_MASK = 0x49;
    public static final int NONZERO_ADDRESS_LENGTH_MASK = 0x4a;
    public static final int NONZERO_FACILITY_LENGTH_MASK = 0x4b;
    public static final int FACILITY_NOT_PROVIDED_WHEN_EXPECTED_MASK = 0x4c;
    public static final int INVALID_ITU_T_SPECIFIED_DTE_FACILITY_MASK = 0x4d;
    public static final int MAXIMUM_NUMBER_OF_CALL_REDIRECTIONS_OR_DEFLECTIONS_EXCEEDED_MASK = 0x4e;
    public static final int MISCELLANEOUS_MASK = 0x50;
    public static final int IMPROPER_CAUSE_CODE_FOR_DTE_MASK = 0x51;
    public static final int OCTET_NOT_ALIGNED_MASK = 0x52;
    public static final int INCONSISTENT_Q_BIT_SETTING_MASK = 0x53;
    public static final int NETWORK_USER_IDENTIFICATION_PROBLEM_MASK = 0x54;
    public static final int INTERNATIONAL_PROBLEM_MASK = 0x70;
    public static final int REMOTE_NETWORK_PROBLEM_MASK = 0x71;
    public static final int INTERNATIONAL_PROTOCOL_PROBLEM_MASK = 0x72;
    public static final int INTERNATIONAL_LINK_OUT_OF_ORDER_MASK = 0x73;
    public static final int INTERNATIONAL_LINK_BUSY_MASK = 0x74;
    public static final int TRANSIT_NETWORK_FACILITY_PROBLEM_MASK = 0x75;
    public static final int REMOTE_NETWORK_FACILITY_PROBLEM_MASK = 0x76;
    public static final int INTERNATIONAL_ROUTING_PROBLEM_MASK = 0x77;
    public static final int TEMPORARY_ROUTING_PROBLEM_MASK = 0x78;
    public static final int UNKNOWN_CALLED_DATA_NETWORK_IDENTIFICATION_CODE_MASK = 0x79;
    public static final int MAINTENANCE_ACTION_CLEAR_X25_VC_COMMAND_ISSUED_MASK = 0x7a;
//
    public static final int NORMAL_TERMINATION_MASK = 0xf0;
    public static final int OUT_OF_RESOURCES_MASK = 0xf1;
    public static final int AUTHENTICATION_FAILURE_MASK = 0xf2;
    public static final int INBOUND_USER_DATA_TOO_LARGE_MASK = 0xf3;
    public static final int IDLE_TIMER_EXPIRED_MASK = 0xf4;

    static boolean compareWithMask(final int mask, final int value) {

        return (mask == value);
    }

    private DiagnosticCodeEnumerationUtility() {
    }
}
