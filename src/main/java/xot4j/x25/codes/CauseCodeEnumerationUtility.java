//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http//www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.codes;

public final class CauseCodeEnumerationUtility {

    // ClearRequest Enumeration DESCRIPTION
    public static final String CLEAR_REQUEST_DTE_ORIGINATED_DESCRIPTION = "DTE_originated";
    public static final String CLEAR_REQUEST_NUMBER_BUSY_DESCRIPTION = "Number_busy";
    public static final String CLEAR_REQUEST_INVALID_FACILITY_REQUEST_DESCRIPTION = "Invalid_facility_request";
    public static final String CLEAR_REQUEST_NETWORK_CONGESTION_DESCRIPTION = "Network_congestion";
    public static final String CLEAR_REQUEST_OUT_OF_ORDER_DESCRIPTION = "Out_of_order";
    public static final String CLEAR_REQUEST_ACCESS_BARRED_DESCRIPTION = "Access_barred";
    public static final String CLEAR_REQUEST_NOT_OBTAINABLE_DESCRIPTION = "Not_obtainable";
    public static final String CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION = "Remote_procedure_error";
    public static final String CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION = "Local_procedure_error";
    public static final String CLEAR_REQUEST_RPOA_OUT_OF_ORDER_DESCRIPTION = "RPOA_out_of_order";
    public static final String CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_DESCRIPTION = "Reverse_charging_not_accepted";
    public static final String CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION = "Incompatible_destination";
    public static final String CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_DESCRIPTION = "Fast_select_not_accepted";
    public static final String CLEAR_REQUEST_SHIP_ABSENT_DESCRIPTION = "Ship_absent";
    // ClearRequest Enumeration MASK
    public static final int CLEAR_REQUEST_DTE_ORIGINATED_MASK = 0x00;
    public static final int CLEAR_REQUEST_NUMBER_BUSY_MASK = 0x01;
    public static final int CLEAR_REQUEST_INVALID_FACILITY_REQUEST_MASK = 0x03;
    public static final int CLEAR_REQUEST_NETWORK_CONGESTION_MASK = 0x05;
    public static final int CLEAR_REQUEST_OUT_OF_ORDER_MASK = 0x09;
    public static final int CLEAR_REQUEST_ACCESS_BARRED_MASK = 0x0b;
    public static final int CLEAR_REQUEST_NOT_OBTAINABLE_MASK = 0x0d;
    public static final int CLEAR_REQUEST_REMOTE_PROCEDURE_ERROR_MASK = 0x11;
    public static final int CLEAR_REQUEST_LOCAL_PROCEDURE_ERROR_MASK = 0x13;
    public static final int CLEAR_REQUEST_RPOA_OUT_OF_ORDER_MASK = 0x15;
    public static final int CLEAR_REQUEST_REVERSE_CHARGING_NOT_ACCEPTED_MASK = 0x19;
    public static final int CLEAR_REQUEST_INCOMPATIBLE_DESTINATION_MASK = 0x21;
    public static final int CLEAR_REQUEST_FAST_SELECT_NOT_ACCEPTED_MASK = 0x29;
    public static final int CLEAR_REQUEST_SHIP_ABSENT_MASK = 0x39;
    // ResetRequest Enumeration DESCRIPTION
    public static final String RESET_REQUEST_DTE_ORIGINATED_DESCRIPTION = "DTE_originated";
    public static final String RESET_REQUEST_OUT_OF_ORDER_DESCRIPTION = "Out_of_order";
    public static final String RESET_REQUEST_REMOTE_PROCEDURE_ERROR_DESCRIPTION = "Remote_procedure_error";
    public static final String RESET_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION = "Local_procedure_error";
    public static final String RESET_REQUEST_NETWORK_CONGESTION_DESCRIPTION = "Network_congestion";
    public static final String RESET_REQUEST_REMOTE_DTE_OPERATIONAL_DESCRIPTION = "Remote_DTE_operational";
    public static final String RESET_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION = "Network_operational";
    public static final String RESET_REQUEST_INCOMPATIBLE_DESTINATION_DESCRIPTION = "Incompatible_destination";
    public static final String RESET_REQUEST_NETWORK_OUT_OF_ORDER_DESCRIPTION = "Network_out_of_order";
    // ResetRequest Enumeration MASK
    public static final int RESET_REQUEST_DTE_ORIGINATED_MASK = 0x00;
    public static final int RESET_REQUEST_OUT_OF_ORDER_MASK = 0x01;
    public static final int RESET_REQUEST_REMOTE_PROCEDURE_ERROR_MASK = 0x03;
    public static final int RESET_REQUEST_LOCAL_PROCEDURE_ERROR_MASK = 0x05;
    public static final int RESET_REQUEST_NETWORK_CONGESTION_MASK = 0x07;
    public static final int RESET_REQUEST_REMOTE_DTE_OPERATIONAL_MASK = 0x09;
    public static final int RESET_REQUEST_NETWORK_OPERATIONAL_MASK = 0x0f;
    public static final int RESET_REQUEST_INCOMPATIBLE_DESTINATION_MASK = 0x11;
    public static final int RESET_REQUEST_NETWORK_OUT_OF_ORDER_MASK = 0x1d;
    // RestartRequest Enumeration DESCRIPTION
    public static final String RESTART_REQUEST_DTE_RESTARTING_DESCRIPTION = "DTE_restarting";
    public static final String RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_DESCRIPTION = "Local_procedure_error";
    public static final String RESTART_REQUEST_NETWORK_CONGESTION_DESCRIPTION = "Network_congestion";
    public static final String RESTART_REQUEST_NETWORK_OPERATIONAL_DESCRIPTION = "Network_operational";
    public static final String RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_DESCRIPTION = "Registration/cancellation_confirmed";
    // RestartRequest Enumeration MASK
    public static final int RESTART_REQUEST_DTE_RESTARTING_MASK = 0x00;
    public static final int RESTART_REQUEST_LOCAL_PROCEDURE_ERROR_MASK = 0x01;
    public static final int RESTART_REQUEST_NETWORK_CONGESTION_MASK = 0x03;
    public static final int RESTART_REQUEST_NETWORK_OPERATIONAL_MASK = 0x07;
    public static final int RESTART_REQUEST_REGISTRATION_CANCELLATION_CONFIRMED_MASK = 0x7f;

    static boolean compareWithMask(final int mask, final int value) {

        return (mask == value);
    }

    private CauseCodeEnumerationUtility() {
    }
}
