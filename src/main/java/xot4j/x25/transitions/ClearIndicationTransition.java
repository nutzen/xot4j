//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x121.X121Address;
import xot4j.x25.X25Packet;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.payload.ClearPacketPayload;
import xot4j.x25.utility.PtiToTransitionUtility;
import xot4j.x25.utility.X25PacketUtility;

public final class ClearIndicationTransition extends BaseTransition implements Transition {

    private static final Logger logger = LoggerFactory.getLogger(ClearIndicationTransition.class);

    private ClearIndicationTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static ClearIndicationTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new ClearIndicationTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        final ClearPacketPayload payload = (ClearPacketPayload) parameters.getPacket().getPayload();
        parameters.getSessionContext().setCause(payload.getCause());
        parameters.getSessionContext().setDiagnostic(payload.getDiagnostic());
        log(parameters);

        final X25Packet packet = X25PacketUtility.clearConfirm(parameters.getSessionContext());

        return TransitionResult.create(PostTransitionActionEnumeration.Submit, packet);
    }

    private static void log(TransitionParameters parameters) {

        final X121Address localAddress = parameters.getSessionContext().getLocalAddress();
        final X121Address remoteAddress = parameters.getSessionContext().getRemoteAddress();
        final LogicalChannelIdentifier lci = parameters.getPacket().getHeader().getLogicalChannelIdentifier();
        final ClearRequestEnumeration cause = parameters.getSessionContext().getCause();
        final DiagnosticCodeEnumeration diagnostic = parameters.getSessionContext().getDiagnostic();

        final String pattern = "clear-indication: local={0}, remote={1}, lcgn={2}, lcn={3}, cause={4}, diagnostic={5}";
        final String message = MessageFormat.format(pattern,
                localAddress,
                remoteAddress,
                0x00ff & lci.getLogicalChannelGroupNumber(),
                0x00ff & lci.getLogicalChannelNumber(),
                (null == cause ? null : cause.toDescription()),
                (null == diagnostic ? null : diagnostic.toDescription()));
        logger.debug(message);
    }
}
