//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.X25Packet;

public final class TransitionResult {

    private final PostTransitionActionEnumeration action;
    private final X25Packet packet /* optional */;

    private TransitionResult(PostTransitionActionEnumeration action,
            X25Packet packet) {

        this.action = action;
        this.packet = packet;
    }

    public static TransitionResult create(PostTransitionActionEnumeration action,
            X25Packet packet) {

        return new TransitionResult(action,
                packet);
    }

    public PostTransitionActionEnumeration getAction() {

        return action;
    }

    public X25Packet getPacket() {

        return packet;
    }
}
