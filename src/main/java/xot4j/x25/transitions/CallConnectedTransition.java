//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x25.payload.CallPacketPayload;
import xot4j.x25.payload.X25Payload;
import xot4j.x25.utility.PtiToTransitionUtility;

public final class CallConnectedTransition extends BaseTransition implements Transition {

    private static final Logger logger = LoggerFactory.getLogger(CallConnectedTransition.class);

    private CallConnectedTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static CallConnectedTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new CallConnectedTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        final X25Payload payload = parameters.getPacket().getPayload();

        if (null == payload) {

            final String pattern = "call-connected: local={0}, remote={1}, lcgn={2}, lcn={3}, payload=null";
            final String message = MessageFormat.format(pattern,
                    parameters.getSessionContext().getLocalAddress(),
                    parameters.getSessionContext().getRemoteAddress(),
                    0x00ff & parameters.getSessionContext().getLogicalChannelIdentifier().getLogicalChannelGroupNumber(),
                    0x00ff & parameters.getSessionContext().getLogicalChannelIdentifier().getLogicalChannelNumber());
            logger.debug(message);

        } else if (payload instanceof CallPacketPayload) {

            final CallPacketPayload callPacketPayload = (CallPacketPayload) payload;
            if ((null == callPacketPayload.getFacilities()) || callPacketPayload.getFacilities().isEmpty()) {

                final String pattern = "call-connected: local={0}, remote={1}, lcgn={2}, lcn={3}, payload.facilities=null|empty";
                final String message = MessageFormat.format(pattern,
                        parameters.getSessionContext().getLocalAddress(),
                        parameters.getSessionContext().getRemoteAddress(),
                        0x00ff & parameters.getSessionContext().getLogicalChannelIdentifier().getLogicalChannelGroupNumber(),
                        0x00ff & parameters.getSessionContext().getLogicalChannelIdentifier().getLogicalChannelNumber());
                logger.debug(message);

            } else {
                parameters.getSessionContext().addFacilities(callPacketPayload.getFacilities());

            }

        } else {

            final String pattern = "call-connected: local={0}, remote={1}, lcgn={2}, lcn={3}, payload={4}";
            final String message = MessageFormat.format(pattern,
                    parameters.getSessionContext().getLocalAddress(),
                    parameters.getSessionContext().getRemoteAddress(),
                    0x00ff & parameters.getSessionContext().getLogicalChannelIdentifier().getLogicalChannelGroupNumber(),
                    0x00ff & parameters.getSessionContext().getLogicalChannelIdentifier().getLogicalChannelNumber(),
                    payload.getClass().getName());
            logger.debug(message);

        }

        return TransitionResult.create(PostTransitionActionEnumeration.None, null);
    }
}
