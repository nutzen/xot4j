//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import static xot4j.x25.transitions.TransitionEnumerationUtility.*;

public enum TransitionEnumeration {

    // Call set-up and clearing DESCRIPTION
    CallRequest,
    CallIncoming,
    CallAccepted,
    CallConnected,
    ClearRequest,
    ClearIndication,
    ClearConfirmation,
    // Data and interrupt DESCRIPTION
    Data,
    Interrupt,
    InterruptConfirmation,
    // Flow control and reset DESCRIPTION
    ReceiverReady,
    ReceiverNotReady,
    Reject,
    ResetRequest,
    ResetIndication,
    ResetConfirmation,
    // Restart DESCRIPTION
    RestartRequest,
    RestartIndication,
    RestartConfirmation,
    // Diagnostic DESCRIPTION
    Diagnostic;

    public String toDescription() {

        String destination;

        switch (this) {
            case CallRequest:
                destination = CALL_REQUEST_DESCRIPTION;
                break;

            case CallIncoming:
                destination = CALL_INCOMING_DESCRIPTION;
                break;

            case CallAccepted:
                destination = CALL_ACCEPTED_DESCRIPTION;
                break;

            case CallConnected:
                destination = CALL_CONNECTED_DESCRIPTION;
                break;

            case ClearRequest:
                destination = CLEAR_REQUEST_DESCRIPTION;
                break;

            case ClearIndication:
                destination = CLEAR_INDICATION_DESCRIPTION;
                break;

            case ClearConfirmation:
                destination = CLEAR_CONFIRMATION_DESCRIPTION;
                break;

            case Data:
                destination = DATA_DESCRIPTION;
                break;

            case Interrupt:
                destination = INTERRUPT_DESCRIPTION;
                break;

            case InterruptConfirmation:
                destination = INTERRUPT_CONFIRMATION_DESCRIPTION;
                break;

            case ReceiverReady:
                destination = RECEIVER_READY_DESCRIPTION;
                break;

            case ReceiverNotReady:
                destination = RECEIVER_NOT_READY_DESCRIPTION;
                break;

            case Reject:
                destination = REJECT_DESCRIPTION;
                break;

            case ResetRequest:
                destination = RESET_REQUEST_DESCRIPTION;
                break;

            case ResetIndication:
                destination = RESET_INDICATION_DESCRIPTION;
                break;

            case ResetConfirmation:
                destination = RESET_CONFIRMATION_DESCRIPTION;
                break;

            case RestartRequest:
                destination = RESTART_REQUEST_DESCRIPTION;
                break;

            case RestartIndication:
                destination = RESTART_INDICATION_DESCRIPTION;
                break;

            case RestartConfirmation:
                destination = RESTART_CONFIRMATION_DESCRIPTION;
                break;

            case Diagnostic:
                destination = DIAGNOSTIC_DESCRIPTION;
                break;

            default:
                throw new RuntimeException("Unhandled type: " + this);
        }

        return destination;
    }

    public static TransitionEnumeration fromDescription(final String value) {

        final TransitionEnumeration destination;
        switch (value) {
            case CALL_REQUEST_DESCRIPTION:
                destination = CallRequest;
                break;

            case CALL_INCOMING_DESCRIPTION:
                destination = CallIncoming;
                break;

            case CALL_ACCEPTED_DESCRIPTION:
                destination = CallAccepted;
                break;

            case CALL_CONNECTED_DESCRIPTION:
                destination = CallConnected;
                break;

            case CLEAR_REQUEST_DESCRIPTION:
                destination = ClearRequest;
                break;

            case CLEAR_INDICATION_DESCRIPTION:
                destination = ClearIndication;
                break;

            case CLEAR_CONFIRMATION_DESCRIPTION:
                destination = ClearConfirmation;
                break;

            case DATA_DESCRIPTION:
                destination = Data;
                break;

            case INTERRUPT_DESCRIPTION:
                destination = Interrupt;
                break;

            case INTERRUPT_CONFIRMATION_DESCRIPTION:
                destination = InterruptConfirmation;
                break;

            case RECEIVER_READY_DESCRIPTION:
                destination = ReceiverReady;
                break;

            case RECEIVER_NOT_READY_DESCRIPTION:
                destination = ReceiverNotReady;
                break;

            case REJECT_DESCRIPTION:
                destination = Reject;
                break;

            case RESET_REQUEST_DESCRIPTION:
                destination = ResetRequest;
                break;

            case RESET_INDICATION_DESCRIPTION:
                destination = ResetIndication;
                break;

            case RESET_CONFIRMATION_DESCRIPTION:
                destination = ResetConfirmation;
                break;

            case RESTART_REQUEST_DESCRIPTION:
                destination = RestartRequest;
                break;

            case RESTART_INDICATION_DESCRIPTION:
                destination = RestartIndication;
                break;

            case RESTART_CONFIRMATION_DESCRIPTION:
                destination = RestartConfirmation;
                break;

            case DIAGNOSTIC_DESCRIPTION:
                destination = Diagnostic;
                break;

            default:
                throw new RuntimeException("Unhandled value: " + value);
        }

        return destination;
    }
}
