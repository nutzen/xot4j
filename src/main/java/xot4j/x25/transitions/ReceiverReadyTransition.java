//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.utility.PtiToTransitionUtility;

public final class ReceiverReadyTransition extends BaseTransition implements Transition {
    
    private ReceiverReadyTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {
        
        super(enumeration, parameters);
    }
    
    public static ReceiverReadyTransition create(TransitionParameters parameters) {
        
        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new ReceiverReadyTransition(transition, parameters);
    }
    
    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {
        
        final TransitionResult destination;
        
        final RoleEnumeration origin = parameters.getOrigin();
        if (RoleEnumeration.DCE == origin) {
            destination = TransitionResult.create(PostTransitionActionEnumeration.None, null);
            
        } else if (RoleEnumeration.DTE == origin) {
            // SessionContextUtility.updatePacketSendSequenceNumber(parameters.getSessionContext());
            if (parameters.getSessionContext().isExpectingMoreData()) {
                destination = TransitionResult.create(PostTransitionActionEnumeration.Poll, null);
                parameters.getSessionContext().setExpectingMoreData(false);

            } else {
                destination = TransitionResult.create(PostTransitionActionEnumeration.None, null);
            }
            
        } else {
            throw new RuntimeException("Unhandled origin: " + origin);
        }
        
        return destination;
    }
}
