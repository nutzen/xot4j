//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.states.StateMachine;

abstract class BaseTransition implements Transition {

    private final TransitionEnumeration enumeration;
    private final TransitionParameters parameters;

    protected BaseTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        this.enumeration = enumeration;
        this.parameters = parameters;
    }

    @Override
    public final TransitionEnumeration getEnumeration() {

        return enumeration;
    }

    @Override
    public final TransitionResult call() throws Exception {

        final StateMachine stateMachine = parameters.getStateMachine();
        stateMachine.handle(enumeration);

        return call(parameters);
    }

    protected abstract TransitionResult call(TransitionParameters parameters) throws Exception;
}
