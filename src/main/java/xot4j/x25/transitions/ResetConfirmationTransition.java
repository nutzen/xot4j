//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.X25Packet;
import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.utility.PtiToTransitionUtility;
import xot4j.x25.utility.X25PacketUtility;

public final class ResetConfirmationTransition extends BaseTransition implements Transition {

    private ResetConfirmationTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static ResetConfirmationTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new ResetConfirmationTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        final TransitionResult destination;

        final RoleEnumeration origin = parameters.getOrigin();

        if (RoleEnumeration.DCE == origin) {
            destination = TransitionResult.create(PostTransitionActionEnumeration.None, null);

        } else {
            if (null != parameters.getSessionContext().getLastData()) {
                final X25Packet packet = X25PacketUtility.data(parameters.getSessionContext(), parameters.getSessionContext().getLastData(), false);
                destination = TransitionResult.create(PostTransitionActionEnumeration.Submit, packet);

            } else {
                destination = TransitionResult.create(PostTransitionActionEnumeration.None, null);
            }
        }

        return destination;
    }
}
