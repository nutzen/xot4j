//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.utility.PtiToTransitionUtility;

public final class ClearConfirmationTransition extends BaseTransition implements Transition {

    private ClearConfirmationTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static ClearConfirmationTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new ClearConfirmationTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        return TransitionResult.create(PostTransitionActionEnumeration.None, null);
    }
}
