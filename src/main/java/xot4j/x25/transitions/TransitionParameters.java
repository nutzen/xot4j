//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import java.io.OutputStream;
import xot4j.x25.X25Packet;
import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.context.SessionContext;
import xot4j.x25.states.StateMachine;

/**
 *
 * @todo prune class members to bare minimum
 */
public final class TransitionParameters {

    private final OutputStream outputStream;
    private final StateMachine stateMachine;
    private final SessionContext sessionContext;
    private final RoleEnumeration origin;
    private final X25Packet packet;

    private TransitionParameters(
            OutputStream outputStream,
            StateMachine stateMachine,
            SessionContext sessionContext,
            RoleEnumeration origin,
            X25Packet packet) {

        this.outputStream = outputStream;
        this.stateMachine = stateMachine;
        this.sessionContext = sessionContext;
        this.origin = origin;
        this.packet = packet;
    }

    public static TransitionParameters create(
            OutputStream outputStream,
            StateMachine stateMachine,
            SessionContext sessionContext,
            RoleEnumeration origin,
            X25Packet packet) {

        return new TransitionParameters(
                outputStream,
                stateMachine,
                sessionContext,
                origin,
                packet);
    }

    public OutputStream getOutputStream() {

        return outputStream;
    }

    public StateMachine getStateMachine() {

        return stateMachine;
    }

    public SessionContext getSessionContext() {

        return sessionContext;
    }

    public RoleEnumeration getOrigin() {

        return origin;
    }

    public X25Packet getPacket() {

        return packet;
    }
}
