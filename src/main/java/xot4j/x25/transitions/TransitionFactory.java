//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.utility.PtiToTransitionUtility;

public final class TransitionFactory {

    private TransitionFactory() {
    }

    public static Transition create(TransitionParameters parameters) {

        Transition destination;

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);

        switch (transition) {
            case CallRequest: { destination = CallRequestTransition.create(parameters); break; }
            case CallIncoming: { destination = CallIncomingTransition.create(parameters); break; }
            case CallAccepted: { destination = CallAcceptedTransition.create(parameters); break; }
            case CallConnected: { destination = CallConnectedTransition.create(parameters); break; }
            case ClearRequest: { destination = ClearRequestTransition.create(parameters); break; }
            case ClearIndication: { destination = ClearIndicationTransition.create(parameters); break; }
            case ClearConfirmation: { destination = ClearConfirmationTransition.create(parameters); break; }
            case Data: { destination = DataTransition.create(parameters); break; }
            // case Interrupt: { destination = InterruptTransition.create(parameters); break; }
            // case InterruptConfirmation: { destination = InterruptConfirmationTransition.create(parameters); break; }
            case ReceiverReady: { destination = ReceiverReadyTransition.create(parameters); break; }
            // case ReceiverNotReady: { destination = ReceiverNotReadyTransition.create(parameters); break; }
            // case Reject: { destination = RejectTransition.create(parameters); break; }
            // case ResetRequest: { destination = ResetRequestTransition.create(parameters); break; }
            case ResetIndication: { destination = ResetIndicationTransition.create(parameters); break; }
            case ResetConfirmation: { destination = ResetConfirmationTransition.create(parameters); break; }
            // case RestartRequest: { destination = RestartRequestTransition.create(parameters); break; }
            // case RestartIndication: { destination = RestartIndicationTransition.create(parameters); break; }
            // case RestartConfirmation: { destination = RestartConfirmationTransition.create(parameters); break; }
            // case Diagnostic: { destination = DiagnosticTransition.create(parameters); break; }
            default:
                throw new RuntimeException("Unhandled transition: " + transition.toDescription());

        }

        return destination;
    }
}
