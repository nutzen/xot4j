//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

public final class TransitionEnumerationUtility {

    // Call set-up and clearing DESCRIPTION
    public static final String CALL_REQUEST_DESCRIPTION = "Call_Request";
    public static final String CALL_INCOMING_DESCRIPTION = "Call_Incoming";
    public static final String CALL_ACCEPTED_DESCRIPTION = "Call_Accepted";
    public static final String CALL_CONNECTED_DESCRIPTION = "Call_Connected";
    public static final String CLEAR_REQUEST_DESCRIPTION = "Clear_Request";
    public static final String CLEAR_INDICATION_DESCRIPTION = "Clear_Indication";
    public static final String CLEAR_CONFIRMATION_DESCRIPTION = "Clear_Confirmation";
    // Data and interrupt DESCRIPTION
    public static final String DATA_DESCRIPTION = "Data";
    public static final String INTERRUPT_DESCRIPTION = "Interrupt";
    public static final String INTERRUPT_CONFIRMATION_DESCRIPTION = "Interrupt_Confirmation";
    // Flow control and reset DESCRIPTION
    public static final String RECEIVER_READY_DESCRIPTION = "Receiver_Ready";
    public static final String RECEIVER_NOT_READY_DESCRIPTION = "Receiver_Not_Ready";
    public static final String REJECT_DESCRIPTION = "Reject";
    public static final String RESET_REQUEST_DESCRIPTION = "Reset_Request";
    public static final String RESET_INDICATION_DESCRIPTION = "Reset_Indication";
    public static final String RESET_CONFIRMATION_DESCRIPTION = "Reset_Confirmation";
    // Restart DESCRIPTION
    public static final String RESTART_REQUEST_DESCRIPTION = "Restart_Request";
    public static final String RESTART_INDICATION_DESCRIPTION = "Restart_Indication";
    public static final String RESTART_CONFIRMATION_DESCRIPTION = "Restart_Confirmation";
    // Diagnostic DESCRIPTION
    public static final String DIAGNOSTIC_DESCRIPTION = "Diagnostic";

    private TransitionEnumerationUtility() {
    }
}
