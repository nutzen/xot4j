//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x121.X121Address;
import xot4j.x25.X25Packet;
import xot4j.x25.context.SessionContext;
import xot4j.x25.payload.CallPacketPayload;
import xot4j.x25.utility.PtiToTransitionUtility;
import xot4j.x25.utility.X25PacketUtility;

public final class CallIncomingTransition extends BaseTransition implements Transition {

    private static final Logger logger = LoggerFactory.getLogger(CallIncomingTransition.class);

    private CallIncomingTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static CallIncomingTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new CallIncomingTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        final TransitionResult destination;

        final SessionContext sessionContext = parameters.getSessionContext();
        X25Packet packet = parameters.getPacket();
        sessionContext.setLogicalChannelIdentifier(packet.getHeader().getLogicalChannelIdentifier());

        final CallPacketPayload payload = (CallPacketPayload) packet.getPayload();
        sessionContext.setRemoteAddress(payload.getCallingAddress());

        if (null != payload.getFacilities()) {
            sessionContext.addFacilities(payload.getFacilities());
        }

        final X121Address localAddress = parameters.getSessionContext().getLocalAddress();
        final X121Address calledAddress = payload.getCalledAddress();
        if (localAddress.equals(calledAddress)) {

            packet = X25PacketUtility.callAccept(sessionContext);
            destination = TransitionResult.create(PostTransitionActionEnumeration.Submit, packet);

        } else {
            final String pattern = "Wrong address: I am on {0} and {1} is looking for {2}.";
            final String message = MessageFormat.format(pattern,
                    localAddress,
                    payload.getCallingAddress(),
                    calledAddress);
            logger.debug(message);

            packet = X25PacketUtility.clearRequest(sessionContext);
            destination = TransitionResult.create(PostTransitionActionEnumeration.Submit, packet);
        }

        return destination;
    }
}
