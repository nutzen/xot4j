//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import xot4j.x25.X25Packet;
import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.context.SessionContext;
import xot4j.x25.payload.DataPacketPayload;
import xot4j.x25.pti.DataPacketTypeIdentifier;
import xot4j.x25.utility.PtiToTransitionUtility;
import xot4j.x25.utility.SessionContextUtility;
import xot4j.x25.utility.X25PacketUtility;

public final class DataTransition extends BaseTransition implements Transition {

    private DataTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static DataTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new DataTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        final TransitionResult destination;

        final SessionContext sessionContext = parameters.getSessionContext();
        final RoleEnumeration origin = parameters.getOrigin();

        if (RoleEnumeration.DCE == origin) {
            X25Packet packet = parameters.getPacket();
            final DataPacketTypeIdentifier packetTypeIdentifier = (DataPacketTypeIdentifier) packet.getHeader().getPacketTypeIdentifier();
            sessionContext.setPacketReceiveSequenceNumber(packetTypeIdentifier.getPacketSendSequenceNumber());
            SessionContextUtility.updatePacketReceiveSequenceNumber(sessionContext);
            sessionContext.setExpectingMoreData(packetTypeIdentifier.isMoreData());

            final DataPacketPayload payload = (DataPacketPayload) packet.getPayload();
            parameters.getOutputStream().write(payload.getUserData());

            packet = X25PacketUtility.receiverReady(sessionContext);
            destination = TransitionResult.create(PostTransitionActionEnumeration.Submit, packet);

        } else if (RoleEnumeration.DTE == origin) {

            final X25Packet packet = parameters.getPacket();
            final DataPacketPayload payload = (DataPacketPayload) packet.getPayload();

            sessionContext.setLastData(payload.getUserData());
            SessionContextUtility.updatePacketSendSequenceNumber(sessionContext);
            destination = TransitionResult.create(PostTransitionActionEnumeration.Poll, null);

        } else {
            throw new RuntimeException("Unhandled origin: " + origin);
        }

        return destination;
    }
}
