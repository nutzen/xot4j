//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.transitions;

import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x121.X121Address;
import xot4j.x25.X25Packet;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.codes.ResetRequestEnumeration;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.payload.ResetPacketPayload;
import xot4j.x25.utility.PtiToTransitionUtility;
import xot4j.x25.utility.X25PacketUtility;

public final class ResetIndicationTransition extends BaseTransition implements Transition {

    private static final Logger logger = LoggerFactory.getLogger(ResetIndicationTransition.class);

    private ResetIndicationTransition(TransitionEnumeration enumeration,
            TransitionParameters parameters) {

        super(enumeration, parameters);
    }

    public static ResetIndicationTransition create(TransitionParameters parameters) {

        final TransitionEnumeration transition = PtiToTransitionUtility.transition(parameters);
        return new ResetIndicationTransition(transition, parameters);
    }

    @Override
    protected TransitionResult call(TransitionParameters parameters) throws Exception {

        parameters.getSessionContext().setPacketReceiveSequenceNumber(0);
        parameters.getSessionContext().setPacketSendSequenceNumber(0);
        log(parameters);

        final int resetIndicationThreshold = 20;
        final int resetIndicationCounter = 1 + parameters.getSessionContext().getResetIndicationCounter();
        parameters.getSessionContext().setResetIndicationCounter(resetIndicationCounter);

        final X25Packet packet;
        if (resetIndicationThreshold > resetIndicationCounter) {
            packet = X25PacketUtility.resetConfirm(parameters.getSessionContext());

        } else {
            packet = X25PacketUtility.clearRequest(parameters.getSessionContext());
            final String pattern = "Sorry guys, this is a bad connection... I am throwing in the towel: local={0}, remote={1}";
            final String message = MessageFormat.format(
                    pattern,
                    parameters.getSessionContext().getLocalAddress(),
                    parameters.getSessionContext().getRemoteAddress());
            logger.debug(message);
        }

        return TransitionResult.create(PostTransitionActionEnumeration.Submit, packet);
    }

    private static void log(TransitionParameters parameters) {

        final X121Address localAddress = parameters.getSessionContext().getLocalAddress();
        final X121Address remoteAddress = parameters.getSessionContext().getRemoteAddress();
        final LogicalChannelIdentifier lci = parameters.getPacket().getHeader().getLogicalChannelIdentifier();
        final ResetPacketPayload payload = (ResetPacketPayload) parameters.getPacket().getPayload();
        final ResetRequestEnumeration cause = payload.getCause();
        final DiagnosticCodeEnumeration diagnostic = payload.getDiagnostic();

        final String pattern = "reset-indication: local={0}, remote={1}, lcgn={2}, lcn={3}, cause={4}, diagnostic={5}";
        final String message = MessageFormat.format(pattern,
                localAddress,
                remoteAddress,
                0x00ff & lci.getLogicalChannelGroupNumber(),
                0x00ff & lci.getLogicalChannelNumber(),
                (null == cause ? null : cause.toDescription()),
                (null == diagnostic ? null : diagnostic.toDescription()));
        logger.debug(message);
    }
}
