//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25.lci;

/**
 *                      4               8         NUMBER OF BITS
 *                .=================================.
 *                ||    L     |         L          ||
 *    GFI <------ ||    C     |         C          ||---> PTI
 *                ||    G     |         N          ||
 *                ||    N     |                    ||
 *                `================================='`
 *                |<- - - - - - - LCI - - - - - - ->|
 */
public final class LogicalChannelIdentifier {

    private final byte logicalChannelGroupNumber /* LCGN 4-bit value*/;
    private final byte logicalChannelNumber /* LCN */;

    // Logical channel identifier (LCI) = LCGN + LCN
    LogicalChannelIdentifier(byte logicalChannelGroupNumber, byte logicalChannelNumber) {

        this.logicalChannelGroupNumber = logicalChannelGroupNumber;
        this.logicalChannelNumber = logicalChannelNumber;
    }

    public static LogicalChannelIdentifier create(int logicalChannelIdentifier) {

        final byte logicalChannelGroupNumber = (byte) ((0x0f00 & logicalChannelIdentifier) >> 8);
        final byte logicalChannelNumber = (byte) (0x00ff & logicalChannelIdentifier);

        return create(logicalChannelGroupNumber, logicalChannelNumber);
    }

    public static LogicalChannelIdentifier create(byte logicalChannelGroupNumber, byte logicalChannelNumber) {

        if (0x0f < logicalChannelGroupNumber) {
            throw new RuntimeException("Logical Channel Group Number is out of range: " + logicalChannelGroupNumber);
        }

        return new LogicalChannelIdentifier(logicalChannelGroupNumber, logicalChannelNumber);
    }

    public byte getLogicalChannelGroupNumber() {

        return logicalChannelGroupNumber;
    }

    public byte getLogicalChannelNumber() {

        return logicalChannelNumber;
    }
}
