//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.exceptions.CallSetupFailure;
import xot4j.stream.Pipe;
import xot4j.transport.TransportWrapper;
import xot4j.x121.X121Address;
import xot4j.x25.codes.ClearRequestEnumeration;
import xot4j.x25.codes.DiagnosticCodeEnumeration;
import xot4j.x25.context.RoleEnumeration;
import xot4j.x25.context.SessionContext;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.states.StateEnumeration;
import xot4j.x25.states.StateMachine;
import xot4j.x25.transitions.Transition;
import xot4j.x25.transitions.TransitionFactory;
import xot4j.x25.transitions.TransitionParameters;
import xot4j.x25.transitions.TransitionResult;
import xot4j.x25.utility.SessionContextUtility;
import xot4j.x25.utility.X25PacketUtility;

public final class X25Controller implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(X25Controller.class);
    private final Thread thread = new Thread(this);
    private final Pipe localToRemote;
    private final Pipe remoteToLocal;
    private final StateMachine stateMachine;
    private final SessionContext sessionContext;
    private final TransportWrapper wrapper;
    private volatile boolean running;

    private X25Controller(
            Pipe localToRemote,
            Pipe remoteToLocal,
            StateMachine stateMachine,
            SessionContext sessionContext,
            TransportWrapper wrapper,
            boolean running) {

        this.localToRemote = localToRemote;
        this.remoteToLocal = remoteToLocal;
        this.stateMachine = stateMachine;
        this.sessionContext = sessionContext;
        this.wrapper = wrapper;
        this.running = running;
    }

    public static X25Controller create(
            SessionContext sessionContext,
            TransportWrapper wrapper) {

        final Pipe localToRemote = Pipe.create();
        final Pipe remoteToLocal = Pipe.create();
        final StateMachine stateMachine = StateMachine.create();
        final boolean running = false;

        return new X25Controller(
                localToRemote,
                remoteToLocal,
                stateMachine,
                sessionContext,
                wrapper,
                running);
    }

    private static void sleep(long interval) throws InterruptedException {

        Thread.sleep(interval);
    }

    @Override
    public void run() {

        try {
            while (running) {
                try {
                    sleep(100);

                    while (poll(0)) {
                    }

                    if (isInFlowControlReadyState()) {
                        final InputStream inputStream = localToRemote.getInputStream();
                        final int available = inputStream.available();
                        if (0 < available) {
                            final byte[] userData;
                            {
                                final byte[] buffer = new byte[available];
                                final int read = inputStream.read(buffer);
                                userData = Arrays.copyOfRange(buffer, 0, read);
                            }

                            final List<byte[]> dataPackets = splitData(sessionContext, userData);
                            for (int index = 0; dataPackets.size() > index; ++index) {
                                final byte[] dataPacket = dataPackets.get(index);
                                final boolean more = (dataPackets.size() > (1 + index));
                                final X25Packet packet = X25PacketUtility.data(sessionContext, dataPacket, more);
                                submit(packet);
                            }
                        }
                    }

                } catch (InterruptedException | IOException e) {
                    running = false;

                } catch (Exception e) {
                    running = false;
                    logger.debug(e.getMessage(), e);
                    disconnect();
                }
            }

        } finally {
            running = false;
        }
    }

    public Pipe getLocalToRemote() {

        return localToRemote;
    }

    public Pipe getRemoteToLocal() {

        return remoteToLocal;
    }

    public boolean isConnected() {

        final boolean connected = running && isInFlowControlState();
        if (!connected) {
            final String pattern = "Link between {0} and {1} : running = {2} and state = {3} - {4}";
            final String message = MessageFormat.format(pattern,
                    sessionContext.getLocalAddress(),
                    sessionContext.getRemoteAddress(),
                    running,
                    stateMachine.getStateEnumeration(),
                    stateMachine.getHistory());
            logger.debug(message);
        }
        return connected;
    }

    private boolean isInFlowControlReadyState() {

        return (StateEnumeration.D1_FlowControlReady == stateMachine.getStateEnumeration());
    }

    private boolean isInFlowControlState() {

        boolean destination;

        switch (stateMachine.getStateEnumeration()) {
            case D1_FlowControlReady:
            case D2_DTEReseting:
            case D3_DCEReseting:
                destination = true;
                break;

            default:
                destination = false;
                break;
        }

        return destination;
    }

    private void callSetupCheck() throws CallSetupFailure {

        if (isInFlowControlState()) {
            // happy days
        } else {
            logHistory(sessionContext, stateMachine);
            final String pattern = "Call setup failure: state={0}, cause={1}, diagnostic={2}";
            final StateEnumeration state = stateMachine.getStateEnumeration();
            final ClearRequestEnumeration cause = sessionContext.getCause();
            final DiagnosticCodeEnumeration diagnostic = sessionContext.getDiagnostic();
            final String message = MessageFormat.format(pattern,
                    (null == state ? null : state.toDescription()),
                    (null == cause ? null : cause.toDescription()),
                    (null == diagnostic ? null : diagnostic.toDescription()));

            throw new CallSetupFailure(message);
        }
    }

    public void accept() throws Exception {

        poll(300);
        callSetupCheck();

        running = true;
        thread.start();
    }

    public void connect(byte[] userData) throws Exception {

        final X25Packet packet = X25PacketUtility.callRequest(sessionContext, userData);
        submit(packet);
        callSetupCheck();

        running = true;
        thread.start();
    }

    public void disconnect() {

        try {
            if (running) {
                running = false;
                thread.join();
            }

            final X25Packet packet = X25PacketUtility.clearRequest(sessionContext);
            submit(packet);

        } catch (Exception e) {
            logger.debug("disconnect failure: " + e.getMessage());
        }
    }

    private boolean poll(int depth) throws Exception {

        final boolean polled;

        final X25Packet packet = wrapper.poll();
        if (null == packet) {
            if (0 < depth) {
                polled = poll(depth - 1);
            } else {
                //ignore
                polled = false;
            }
        } else {
            final OutputStream outputStream = remoteToLocal.getOutputStream();
            final TransitionParameters parameters = TransitionParameters.create(outputStream, stateMachine, sessionContext, RoleEnumeration.DCE, packet);
            final Transition transition = TransitionFactory.create(parameters);
            final TransitionResult result = transition.call();

            handle(result);
            polled = true;
        }

        return polled;
    }

    protected void submit(X25Packet packet) throws Exception {

        final OutputStream outputStream = remoteToLocal.getOutputStream();
        final TransitionParameters parameters = TransitionParameters.create(outputStream, stateMachine, sessionContext, RoleEnumeration.DTE, packet);
        wrapper.submit(packet);
        final Transition transition = TransitionFactory.create(parameters);
        final TransitionResult result = transition.call();

        handle(result);
    }

    private void handle(TransitionResult result) throws Exception {

        switch (result.getAction()) {
            case Poll:
                poll(10);
                break;
            case Submit:
                submit(result.getPacket());
                break;
            case None:
                // ignore
                break;
        }
    }

    static List<byte[]> splitData(SessionContext sessionContext, byte[] userData) throws IOException {

        final List<byte[]> dataPackets = new ArrayList<>();
        final int packetSize = SessionContextUtility.transmitPacketSize(sessionContext);

        final ByteArrayInputStream inputStream = new ByteArrayInputStream(userData);
        while (0 < inputStream.available()) {
            final int size = Math.min(packetSize, inputStream.available());
            final byte[] buffer = new byte[size];

            inputStream.read(buffer);
            dataPackets.add(buffer);
        }

        final String pattern = "data: local={0}, remote={1}, lcgn={2}, lcn={3}, packetSize={4}, dataPackets={5}";
        final String message = MessageFormat.format(pattern,
                sessionContext.getLocalAddress(),
                sessionContext.getRemoteAddress(),
                0x00ff & sessionContext.getLogicalChannelIdentifier().getLogicalChannelGroupNumber(),
                0x00ff & sessionContext.getLogicalChannelIdentifier().getLogicalChannelNumber(),
                packetSize,
                dataPackets.size());
        logger.debug(message);

        return dataPackets;
    }

    private static void logHistory(SessionContext sessionContext, StateMachine stateMachine) {

        final X121Address localAddress = sessionContext.getLocalAddress();
        final X121Address remoteAddress = sessionContext.getRemoteAddress();
        final LogicalChannelIdentifier lci = sessionContext.getLogicalChannelIdentifier();
        final ClearRequestEnumeration cause = sessionContext.getCause();
        final DiagnosticCodeEnumeration diagnostic = sessionContext.getDiagnostic();
        final Collection<String> history = stateMachine.getHistory();

        final String pattern = "clear-indication: local={0}, remote={1}, lcgn={2}, lcn={3}, cause={4}, diagnostic={5}, history={6}";
        final String message = MessageFormat.format(pattern,
                localAddress,
                remoteAddress,
                0x00ff & lci.getLogicalChannelGroupNumber(),
                0x00ff & lci.getLogicalChannelNumber(),
                (null == cause ? null : cause.toDescription()),
                (null == diagnostic ? null : diagnostic.toDescription()),
                history);
        logger.debug(message);
    }
}
