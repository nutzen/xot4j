//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.x25;

import xot4j.x25.gfi.GeneralFormatIdentifier;
import xot4j.x25.lci.LogicalChannelIdentifier;
import xot4j.x25.pti.PacketTypeIdentifier;

/**
 *                              4   4    8    8     NUMBER OF BITS
 *                          .===================.
 *                          || G | L || L || P ||
 *                          || F | C || C || T ||
 *                          || I | G || N || I ||
 *                          ||   | N ||   ||   ||
 *                          `==================='
 *                             ^   ^    ^    ^
 *                             |   |    |    |
 * GENERAL FORMAT IDENTIFIER---'   |    |    `-- PACKET TYPE
 *                                 |    |        IDENTIFIER
 * LOGICAL CHANNEL GROUP NUMBER----'    |
 *                                      |
 * LOGICAL CHANNEL NUMBER---------------'
 */
public final class X25Header {

    private final GeneralFormatIdentifier generalFormatIdentifier;
    private final LogicalChannelIdentifier logicalChannelIdentifier;
    private final PacketTypeIdentifier packetTypeIdentifier;

    X25Header(GeneralFormatIdentifier generalFormatIdentifier,
            LogicalChannelIdentifier logicalChannelIdentifier,
            PacketTypeIdentifier packetTypeIdentifier) {

        this.generalFormatIdentifier = generalFormatIdentifier;
        this.logicalChannelIdentifier = logicalChannelIdentifier;
        this.packetTypeIdentifier = packetTypeIdentifier;
    }

    public static X25Header create(GeneralFormatIdentifier generalFormatIdentifier,
            LogicalChannelIdentifier logicalChannelIdentifier,
            PacketTypeIdentifier packetTypeIdentifier) {

        return new X25Header(generalFormatIdentifier, logicalChannelIdentifier, packetTypeIdentifier);
    }

    public GeneralFormatIdentifier getGeneralFormatIdentifier() {

        return generalFormatIdentifier;
    }

    public LogicalChannelIdentifier getLogicalChannelIdentifier() {

        return logicalChannelIdentifier;
    }

    public PacketTypeIdentifier getPacketTypeIdentifier() {

        return packetTypeIdentifier;
    }
}
