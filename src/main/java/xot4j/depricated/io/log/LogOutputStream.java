//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.log;

import java.io.IOException;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x25.utility.HexUtility;

public class LogOutputStream extends OutputStream {

    private static final Logger logger = LoggerFactory.getLogger(LogOutputStream.class);
    private final OutputStream successor;

    public LogOutputStream(OutputStream successor) {

        this.successor = successor;
    }

    @Override
    public void write(int b) throws IOException {

        successor.write(b);
        logger.debug("write(" + HexUtility.toHex((byte) b) + ")");
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {

        logger.debug("write([" + buffer.length + "], " + offset + ", " + length + ")");
        successor.write(buffer, offset, length);
        logger.debug("{" + HexUtility.toHex(buffer, offset, length) + "}");
    }
}
