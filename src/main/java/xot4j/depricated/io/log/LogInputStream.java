//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.log;

import java.io.IOException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x25.utility.HexUtility;

public class LogInputStream extends InputStream {

    private static final Logger logger = LoggerFactory.getLogger(LogInputStream.class);
    private final InputStream successor;

    public LogInputStream(InputStream successor) {

        this.successor = successor;
    }

    @Override
    public int read() throws IOException {

        final int read = successor.read();
        logger.debug("read(): " + HexUtility.toHex((byte) read));
        return read;
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {

        final int read = successor.read(buffer, offset, length);

        logger.debug("read([" + buffer.length + "], " + offset + ", " + length + "): " + read);
        logger.debug("{" + HexUtility.toHex(buffer, offset, read) + "}");

        return read;
    }
}
