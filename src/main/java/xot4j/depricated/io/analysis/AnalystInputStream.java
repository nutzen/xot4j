//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.analysis;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.depricated.codes.CauseCodes;
import xot4j.depricated.codes.DiagnosticCodes;
import xot4j.depricated.pti.DceToDte;
import xot4j.depricated.utility.TabularPrintUtility;
import xot4j.depricated.utility.X25AnalystUtility;
import xot4j.x25.utility.HexUtility;

public class AnalystInputStream extends InputStream {

    private static final Logger logger = LoggerFactory.getLogger(AnalystInputStream.class);
    private final InputStream successor;

    public AnalystInputStream(InputStream successor) {

        this.successor = successor;
    }

    @Override
    public int read() throws IOException {

        throw new UnsupportedOperationException();
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {

        final int read = successor.read(buffer, offset, length);
        analyse(buffer, offset, read);
        return read;
    }

    private void analyse(byte[] buffer, int offset, int length) {

        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            analyse(stream, buffer, offset, length);
        } finally {
            logger.trace(new String(stream.toByteArray()));
        }
    }

    private void analyse(OutputStream stream, byte[] buffer, int offset,
            int length) {

        final TabularPrintUtility printer = new TabularPrintUtility(stream, 12);
        printer.println("X25 - Analysis");

        printer.println("Q:", X25AnalystUtility.analyseQ(buffer, offset, length));
        printer.println("D:", X25AnalystUtility.analyseD(buffer, offset, length));
        printer.println("XX:", X25AnalystUtility.analyseXX(buffer, offset, length));

        printer.println("LCGN:", X25AnalystUtility.analyseLCGN(buffer, offset, length));
        printer.println("LCN:", X25AnalystUtility.analyseLCN(buffer, offset, length));

        final byte pti = buffer[offset + 2];
        final String description = DceToDte.getDescription(pti);
        printer.println("PTI:", HexUtility.toHex(pti), description);

        if (DceToDte.DATA_PACKET.equals(description)) {
            printer.println("Data:", X25AnalystUtility.analysePTI(buffer, offset, length));
        }

        if (DceToDte.DCE_RR.equals(description)) {
            printer.println("DCE RR:", X25AnalystUtility.analysePTI(buffer, offset, length));
        }

        if (DceToDte.CLEAR_INDICATION.equals(description)) {
            byte code = buffer[offset + 3];
            final String cause = CauseCodes.getClearRequestDescription(code);
            printer.println("Cause:", HexUtility.toHex(code), cause);

            code = buffer[offset + 4];
            final String diagnostic = DiagnosticCodes.getDescription(code);
            printer.println("Diagnostic:", HexUtility.toHex(code), diagnostic);
        }

        if (DceToDte.RESET_INDICATION.equals(description)) {
            byte code = buffer[offset + 3];
            final String cause = CauseCodes.getResetRequestDescription(code);
            printer.println("Cause:", HexUtility.toHex(code), cause);

            code = buffer[offset + 4];
            final String diagnostic = DiagnosticCodes.getDescription(code);
            printer.println("Diagnostic:", HexUtility.toHex(code), diagnostic);
        }

        if (DceToDte.RESTART_INDICATION.equals(description)) {
            byte code = buffer[offset + 3];
            final String cause = CauseCodes.getRestartRequestDescription(code);
            printer.println("Cause:", HexUtility.toHex(code), cause);

            code = buffer[offset + 4];
            final String diagnostic = DiagnosticCodes.getDescription(code);
            printer.println("Diagnostic:", HexUtility.toHex(code), diagnostic);
        }
    }
}
