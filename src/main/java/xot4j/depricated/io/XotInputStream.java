//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io;

import java.io.IOException;
import java.io.InputStream;

public class XotInputStream extends InputStream {

    private InputStream successor;

    public XotInputStream(InputStream successor) {
        this.successor = successor;
    }

    @Override
    public int read() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int read(byte[] destination, int offset, int length) throws IOException {
        byte[] x25 = extractX25();
        if (length < x25.length) {
            throw new ArrayIndexOutOfBoundsException("Has [" + length + "] need [" + x25.length + "]");
        }

        System.arraycopy(x25, 0, destination, offset, x25.length);
        return x25.length;
    }

    public byte[] extractX25() throws IOException {
        // pop version number and length
        byte[] xot = pop(4);

        int length = (0x00ff & xot[2]);
        length = (length << 8);
        length = length | (0x00ff & xot[3]);
        length = length & 0x0000ffff;

        // pop x.25 packet
        return pop(length);
    }

    private byte[] pop(int length) throws IOException {
        byte[] x25 = new byte[length];
        int offset = 0;
        while (offset < length) {
            int read = successor.read(x25, offset, length - offset);
            validateRead(read);
            offset += read;
        }
        return x25;
    }

    private void validateRead(int read) throws EndOfStreamException {
        if (read == -1) {
            throw new EndOfStreamException();
        }
    }
}
