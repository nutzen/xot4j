//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io;

import java.io.IOException;

@SuppressWarnings("serial")
public class EndOfStreamException extends IOException {

    public EndOfStreamException() {
    }

    public EndOfStreamException(String message) {
        super(message);
    }

    public EndOfStreamException(Throwable cause) {
        super(cause);
    }

    public EndOfStreamException(String message, Throwable cause) {
        super(message, cause);
    }
}
