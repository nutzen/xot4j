//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io;

import java.io.IOException;
import java.io.OutputStream;

public class XotOutputStream extends OutputStream {

    public static final int VERSION = 0;
    private OutputStream successor;

    public XotOutputStream(OutputStream successor) {
        this.successor = successor;
    }

    @Override
    public void write(int b) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(byte[] x25, int offset, int length) throws IOException {

        final byte[] packet;
        {
            final byte[] header = prepareHeader(VERSION, length);
            packet = new byte[length + header.length];
            System.arraycopy(header, 0, packet, 0, header.length);
        }

        System.arraycopy(x25, offset, packet, 4, length);
        successor.write(packet);
    }

    private static byte[] prepareHeader(int version, int length) {

        final byte[] header = new byte[4];

        header[0] = (byte) ((version >> 8) & 0x00ff);
        header[1] = (byte) (version & 0x00ff);
        header[2] = (byte) ((length >> 8) & 0x00ff);
        header[3] = (byte) (length & 0x00ff);

        return header;
    }
}
