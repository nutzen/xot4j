//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.trace;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Tracer extends OutputStream {

    private static final Logger logger = LoggerFactory.getLogger(Tracer.class);
    private final FileHelper helper;

    public Tracer(FileHelper helper) {

        this.helper = helper;
    }

    @Override
    public void write(int b) throws IOException {
        try {
            final FileOutputStream output = createOutputStream();
            try {
                output.write(b);
            } finally {
                close(output);
            }

        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {

        try {
            final FileOutputStream output = createOutputStream();
            try {
                output.write(buffer, offset, length);
            } finally {
                close(output);
            }

        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    private FileOutputStream createOutputStream() throws IOException,
            FileNotFoundException {

        final File file = helper.createFile();
        final FileOutputStream output = new FileOutputStream(file);

        return output;
    }

    private void close(FileOutputStream output) throws IOException {

        output.flush();
        output.close();
    }
}
