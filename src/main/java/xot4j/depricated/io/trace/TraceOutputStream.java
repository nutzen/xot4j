//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.trace;

import java.io.IOException;
import java.io.OutputStream;

public class TraceOutputStream extends OutputStream {

    private OutputStream successor;
    private Tracer tracer;

    public TraceOutputStream(OutputStream successor, String prefix) {
        this.successor = successor;

        tracer = new Tracer(new FileHelper("log/" + prefix, "snd"));
    }

    @Override
    public void write(int b) throws IOException {
        successor.write(b);
        tracer.write(b);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        successor.write(buffer, offset, length);
        tracer.write(buffer, offset, length);
    }
}
