//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.trace;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileHelper {

    private String prefix;
    private String suffix;

    public FileHelper(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String buildPathString() {
        final String format = formatTime(System.currentTimeMillis());
        final String path = prefix + "/" + format;

        if ((suffix != null) && !suffix.isEmpty()) {
            return path + "." + suffix;
        }
        return path;
    }

    public File createFile() throws IOException {
        final String path = buildPathString();
        File file = new File(path);
        File directory = file.getParentFile();
        if (!directory.exists()) {
            directory.mkdirs();
        }
        file.createNewFile();
        return file;
    }

    private String formatTime(long currentTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd/HH-mm/s.S");
        return format.format(new Date(currentTime));
    }
//	public static void main(String[] args) throws IOException {
//		FileHelper helper = new FileHelper("C:/Users/joe.els/Desktop/BSC.Trace", "rcv");
//		helper.createFile();
//	}
}
