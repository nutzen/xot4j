//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.io.trace;

import java.io.IOException;
import java.io.InputStream;

public class TraceInputStream extends InputStream {

    private InputStream successor;
    private Tracer tracer;

    public TraceInputStream(InputStream successor, String prefix) {
        this.successor = successor;

        tracer = new Tracer(new FileHelper("log/" + prefix, "rcv"));
    }

    @Override
    public int read() throws IOException {
        int read = successor.read();
        tracer.write(read);
        return read;
    }

    @Override
    public int read(byte[] buffer, int offset, int length) throws IOException {
        int read = successor.read(buffer, offset, length);
        tracer.write(buffer, offset, read);
        return read;
    }
}
