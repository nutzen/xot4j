//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.codes;

import xot4j.x25.utility.HexUtility;

public class CauseCodes {

    public static String getClearRequestDescription(byte code) {
        switch (code) {
            case (byte) 0x00:
                return "DTE originated";
            case (byte) 0x01:
                return "Number busy";
            case (byte) 0x03:
                return "Invalid facility request";
            case (byte) 0x05:
                return "Network congestion";
            case (byte) 0x09:
                return "Out of order";
            case (byte) 0x0b:
                return "Access barred";
            case (byte) 0x0d:
                return "Not obtainable";
            case (byte) 0x11:
                return "Remote procedure error";
            case (byte) 0x13:
                return "Local procedure error";
            case (byte) 0x15:
                return "RPOA out of order";
            case (byte) 0x19:
                return "Reverse charging not accepted";
            case (byte) 0x21:
                return "Incompatible destination";
            case (byte) 0x29:
                return "Fast select not accepted";
            case (byte) 0x39:
                return "Ship absent";
        }

        return "Unknow cause code [" + HexUtility.toHex(code) + "]";
    }

    public static String getResetRequestDescription(byte code) {
        switch (code) {
            case (byte) 0x00:
                return "DTE originated";
            case (byte) 0x01:
                return "Out of order";
            case (byte) 0x03:
                return "Remote procedure error";
            case (byte) 0x05:
                return "Local procedure error";
            case (byte) 0x07:
                return "Network congestion";
            case (byte) 0x09:
                return "Remote DTE operational";
            case (byte) 0x0f:
                return "Network operational";
            case (byte) 0x11:
                return "Incompatible destination";
            case (byte) 0x1d:
                return "Network out of order";
        }

        return "Unknow cause code [" + HexUtility.toHex(code) + "]";
    }

    public static String getRestartRequestDescription(byte code) {
        switch (code) {
            case (byte) 0x00:
                return "DTE restarting";
            case (byte) 0x01:
                return "Local procedure error";
            case (byte) 0x03:
                return "Network congestion";
            case (byte) 0x07:
                return "Network operational";
            case (byte) 0x7f:
                return "Registration/cancellation confirmed";
        }

        return "Unknow cause code [" + HexUtility.toHex(code) + "]";
    }
}
