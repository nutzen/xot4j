//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.codes;

import xot4j.x25.utility.HexUtility;

public class DiagnosticCodes {

    public static String getDescription(byte code) {
        switch (code) {
            case (byte) 0x00:
                return "No additional information";
            case (byte) 0x01:
                return "Invalid P(S) (Packet Send sequence number)";
            case (byte) 0x02:
                return "Invalid P(R) (Packet Receive sequence number)";
            case (byte) 0x10:
                return "Packet type invalid";
            case (byte) 0x11:
                return "Packet type invalid for state R1 (Packet level ready)";
            case (byte) 0x12:
                return "Packet type invalid for state R2 (DTE restart request)";
            case (byte) 0x13:
                return "Packet type invalid for state R3 (DCE restart indication)";
            case (byte) 0x14:
                return "Packet type invalid for state P1 (Ready)";
            case (byte) 0x15:
                return "Packet type invalid for state P2 (DTE Waiting)";
            case (byte) 0x16:
                return "Packet type invalid for state P3 (DCE Waiting)";
            case (byte) 0x17:
                return "Packet type invalid for state P4 (Data Transfer)";
            case (byte) 0x18:
                return "Packet type invalid for state P5 (Call Collision)";
            case (byte) 0x19:
                return "Packet type invalid for state P6 (DTE clear request)";
            case (byte) 0x1a:
                return "Packet type invalid for state P7 (DCE clear indication)";
            case (byte) 0x1b:
                return "Packet type invalid for state D1 (Flow control ready)";
            case (byte) 0x1c:
                return "Packet type invalid for state D2 (DTE reset ready)";
            case (byte) 0x1d:
                return "Packet type invalid for state D3 (DCE reset indication)";
            case (byte) 0x20:
                return "Packet not allowed";
            case (byte) 0x21:
                return "Unidentifiable packet";
            case (byte) 0x22:
                return "Call on one-way logical channel";
            case (byte) 0x23:
                return "Invalid packet type on a permanent virtual circuit";
            case (byte) 0x24:
                return "Packet on unassigned LCN (Logical Channel Number)";
            case (byte) 0x25:
                return "Reject not subscribed to";
            case (byte) 0x26:
                return "Packet too short";
            case (byte) 0x27:
                return "Packet too long";
            case (byte) 0x28:
                return "Invalid GFI (General Format Identifier)";
            case (byte) 0x29:
                return "Restart or registration packet with nonzero LCI (bits 1-4 of octet 1, or bits 1-8 of octet 2)";
            case (byte) 0x2a:
                return "Packet type not compatible with facility";
            case (byte) 0x2b:
                return "Unauthorized interrupt confirmation";
            case (byte) 0x2c:
                return "Unauthorized interrupt";
            case (byte) 0x2d:
                return "Unauthorized reject";
            case (byte) 0x30:
                return "Timer expired";
            case (byte) 0x31:
                return "Timer expired for incoming call";
            case (byte) 0x32:
                return "Timer expired for clear indication";
            case (byte) 0x33:
                return "Timer expired for reset indication";
            case (byte) 0x34:
                return "Timer expired for restart indication";
            case (byte) 0x35:
                return "Timer expired for call deflection";
            case (byte) 0x40:
                return "Call setup, clearing, or registration problem";
            case (byte) 0x41:
                return "Facility code not allowed";
            case (byte) 0x42:
                return "Facility parameter not allowed";
            case (byte) 0x43:
                return "Invalid called address";
            case (byte) 0x44:
                return "Invalid calling address";
            case (byte) 0x45:
                return "Invalid facility length";
            case (byte) 0x46:
                return "Incoming call barred";
            case (byte) 0x47:
                return "No logical channel available";
            case (byte) 0x48:
                return "Call collision";
            case (byte) 0x49:
                return "Duplicate facility requested";
            case (byte) 0x4a:
                return "Nonzero address length";
            case (byte) 0x4b:
                return "Nonzero facility length";
            case (byte) 0x4c:
                return "Facility not provided when expected";
            case (byte) 0x4d:
                return "Invalid ITU-T-specified DTE facility";
            case (byte) 0x4e:
                return "Maximum number of call redirections or deflections exceeded";
            case (byte) 0x50:
                return "Miscellaneous";
            case (byte) 0x51:
                return "Improper cause code for DTE";
            case (byte) 0x52:
                return "Octet not aligned";
            case (byte) 0x53:
                return "Inconsistent Q bit setting";
            case (byte) 0x54:
                return "NUI (Network User Identification) problem";
            case (byte) 0x70:
                return "International problem";
            case (byte) 0x71:
                return "Remote network problem";
            case (byte) 0x72:
                return "International protocol problem";
            case (byte) 0x73:
                return "International link out of order";
            case (byte) 0x74:
                return "International link busy";
            case (byte) 0x75:
                return "Transit network facility problem";
            case (byte) 0x76:
                return "Remote network facility problem";
            case (byte) 0x77:
                return "International routing problem";
            case (byte) 0x78:
                return "Temporary routing problem";
            case (byte) 0x79:
                return "Unknown called DNIC (Data Network Identification Code)";
            case (byte) 0x7a:
                return "Maintenance action (clear x25 vc command issued)";
        }

        return "Unknow cause code [" + HexUtility.toHex(code) + "]";
    }
}
