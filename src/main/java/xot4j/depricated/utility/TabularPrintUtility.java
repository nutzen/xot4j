//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.utility;

import java.io.OutputStream;
import java.io.PrintStream;

public final class TabularPrintUtility {

    private PrintStream stream;
    private int tabSize;

    public TabularPrintUtility(OutputStream output, int tabSize) {
        this.stream = new PrintStream(output);
        this.tabSize = tabSize;
    }

    public void println(Object... args) {
        StringBuilder builder = new StringBuilder();

        if (args != null) {
            for (int index = 0; index < args.length; ++index) {
                padAsNeeded(builder, index);
                builder.append(args[index]);
            }
        }

        stream.println(builder.toString());
    }

    private void padAsNeeded(StringBuilder builder, final int index) {
        int padding = paddingRequired(builder.toString().length(), index);
        fill(builder, padding);
    }

    private int paddingRequired(final int length, final int index) {
        final int ZERO = 0;
        int padding = columnsExpected(index) - length;
        return (padding > 0) ? padding : ZERO;
    }

    private int columnsExpected(final int index) {
        return index * tabSize;
    }

    private void fill(StringBuilder builder, final int padding) {
        for (int counter = 0; counter < padding; ++counter) {
            builder.append(' ');
        }
    }
}
