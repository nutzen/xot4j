//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import xot4j.x121.X121Address;
import xot4j.x121.X121AddressMarshaller;

public final class X25MessageUtility {

    private X25MessageUtility() {
    }

    public static int getCallAccept(byte lcgn, byte lcn, byte[] buffer) {

        buffer[0] = (byte) (0x10 | (byte) (lcgn & 0x0f)); // 00 01 00 00 | LCGN;
        buffer[1] = (byte) (lcn & 0x00ff);
        buffer[2] = 0x0f; // 0000 1011

        return 3;
    }

    public static int getCallRequest(byte lcgn, byte lcn, X121Address calledAddress, X121Address callingAddress, byte[] buffer) throws IOException {

        byte[] facilities = new byte[]{0x43, 0x01, 0x01, 0x42, 0x07, 0x07};
        // byte[] userData = new byte[]{(byte)0xc4, (byte)0x11, (byte)0x22, (byte)0x33, (byte)0x44};
        byte[] userData = new byte[]{(byte) 0xc4};

        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        output.write(0x0b); // Call request;

        X121AddressMarshaller.marshal(output, calledAddress, callingAddress, /*
                 * assume no a-bit
                 */ false);

        // facilities
        output.write(facilities.length); // should be 0x06
        output.write(facilities, 0, facilities.length);

        output.write(userData, 0, userData.length);

        final byte[] temporary = output.toByteArray();

        System.arraycopy(temporary, 0, buffer, 2, temporary.length);
        prepareHeader(lcgn, lcn, buffer);

        return 2 + temporary.length;
    }

    public static int getClearConfirm(byte lcgn, byte lcn, byte[] buffer) {

        prepareHeader(lcgn, lcn, buffer);
        buffer[2] = 0x17; // Clear confirmation

        return 3;
    }

    public static int getClearRequest(byte lcgn, byte lcn, byte[] buffer) {

        prepareHeader(lcgn, lcn, buffer);
        buffer[2] = 0x13; // Clear Request
        buffer[3] = 0x00;
        buffer[4] = 0x00;

        return 5;
    }

    public static void prepareHeader(byte lcgn, byte lcn, byte[] buffer) {

        buffer[0] = (byte) (0x10 | (lcgn & 0x000f)); // 00 01 00 00 | LCGN;
        buffer[1] = (byte) (lcn & 0x00ff); // LCN
    }
}
