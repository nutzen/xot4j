//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.utility;

import xot4j.x25.utility.HexUtility;

public final class X25AnalystUtility {

    private X25AnalystUtility() {
    }

    public static String analyseQ(byte[] buffer, int offset, int length) {
        byte b = buffer[offset];
        return ((b & 0x80) == 0) ? "Data for user" : "Data for pad";
    }

    public static String analyseD(byte[] buffer, int offset, int length) {
        byte b = buffer[offset];
        return ((b & 0x40) == 0) ? "Local acknowledgement" : "Remote acknowledgement";
    }

    public static String analyseXX(byte[] buffer, int offset, int length) {
        byte b = buffer[offset];
        if ((b & 0x30) == 0x30) {
            return "Extended format";
        }
        if ((b & 0x20) == 0x20) {
            return "Modulo 128 sequencing";
        }
        if ((b & 0x10) == 0x10) {
            return "Modulo 8 sequencing";
        }
        return "Reserver";
    }

    public static String analyseLCGN(byte[] buffer, int offset, int length) {
        byte b = buffer[offset];
        return HexUtility.toHex((byte) (b & 0x0f));
    }

    public static String analyseLCN(byte[] buffer, int offset, int length) {
        byte b = buffer[offset + 1];
        return HexUtility.toHex(b);
    }

    /**
     * Assume modulo 8
     */
    public static String analysePTI(byte[] buffer, int offset, int length) {
        byte b = buffer[offset + 2];
        boolean more = (b & 0x10) == 0x10;
        int pr = b & 0x00e0;
        pr = pr >> 5;
        pr = pr & 0x000f;
        int ps = b & 0x000e;
        ps = ps >> 1;
        ps = ps & 0x000f;

        return "Pr=" + pr + ", More=" + more + ", Ps=" + ps;
    }
}
