//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.pti;

import xot4j.x25.utility.HexUtility;

public class DceToDte {

    public static final String INCOMING_CALL = "Incoming Call";
    public static final String CALL_CONNECTED = "Call Connected";
    public static final String CLEAR_INDICATION = "Clear Indication";
    public static final String DCE_CLEAR_CONFIRMATION = "DCE Clear Confirmation";
    public static final String RESET_INDICATION = "Reset Indication";
    public static final String DCE_RESET_CONFIRMATION = "DCE Reset Confirmation";
    public static final String RESTART_INDICATION = "Restart Indication";
    public static final String DCE_RESTART_CONFIRMATION = "DCE Restart Confirmation";
    public static final String DCE_RR = "DCE RR";
    public static final String DCE_RNR = "DCE RNR";
    public static final String DCE_REJ = "DCE REJ";
    public static final String DATA_PACKET = "Data Packet";

    public static String getDescription(byte pti) {
        switch (pti) {
            case (byte) 0x0b:
                return INCOMING_CALL;
            case (byte) 0x0f:
                return CALL_CONNECTED;
            case (byte) 0x13:
                return CLEAR_INDICATION;
            case (byte) 0x17:
                return DCE_CLEAR_CONFIRMATION;
//		case (byte)0x1d : return RESET_INDICATION;
            case (byte) 0x1b:
                return RESET_INDICATION;
            case (byte) 0x1f:
                return DCE_RESET_CONFIRMATION;
            case (byte) 0xfd:
                return RESTART_INDICATION;
            case (byte) 0xff:
                return DCE_RESTART_CONFIRMATION;
        }

        switch (pti & 0x0f) {
            case 0x01:
                return DCE_RR;
            case 0x05:
                return DCE_RNR;
            case 0x09:
                return DCE_REJ;
        }

        if ((pti & 0x01) == 0) {
            return DATA_PACKET;
        }

        return "Unknow PTI [" + HexUtility.toHex(pti) + "]";
    }
}
