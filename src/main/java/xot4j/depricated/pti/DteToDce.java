//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated.pti;

import xot4j.x25.utility.HexUtility;

public class DteToDce {

    public static final String CALL_REQUEST = "Call Request";
    public static final String CALL_ACCEPT = "Call Accept";
    public static final String CLEAR_REQUEST = "Clear Request";
    public static final String DTE_CLEAR_CONFIRMATION = "DTE Clear Confirmation";
    public static final String RESET_REQUEST = "Reset Request";
    public static final String DTE_RESET_CONFIRMATION = "DTE Reset Confirmation";
    public static final String RESTART_REQUEST = "Restart Request";
    public static final String DTE_RESTART_CONFIRMATION = "DTE Restart Confirmation";
    public static final String DTE_RR = "DTE RR";
    public static final String DTE_RNR = "DTE RNR";
    public static final String DTE_REJ = "DTE REJ";
    public static final String DATA_PACKET = "Data Packet";

    public static String getDescription(byte pti) {
        switch (pti) {
            case (byte) 0x0b:
                return CALL_REQUEST;
            case (byte) 0x0f:
                return CALL_ACCEPT;
            case (byte) 0x13:
                return CLEAR_REQUEST;
            case (byte) 0x17:
                return DTE_CLEAR_CONFIRMATION;
//		case (byte)0x1d : return RESET_REQUEST;
            case (byte) 0x1b:
                return RESET_REQUEST;
            case (byte) 0x1f:
                return DTE_RESET_CONFIRMATION;
            case (byte) 0xfd:
                return RESTART_REQUEST;
            case (byte) 0xff:
                return DTE_RESTART_CONFIRMATION;
        }

        switch (pti & 0x0f) {
            case 0x01:
                return DTE_RR;
            case 0x05:
                return DTE_RNR;
            case 0x09:
                return DTE_REJ;
        }

        if ((pti & 0x01) == 0) {
            return DATA_PACKET;
        }

        return "Unknow PTI [" + HexUtility.toHex(pti) + "]";
    }
}
