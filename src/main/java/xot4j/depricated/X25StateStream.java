//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j.depricated;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.depricated.io.XotInputStream;
import xot4j.depricated.io.XotOutputStream;
import xot4j.depricated.io.analysis.AnalystInputStream;
import xot4j.depricated.io.analysis.AnalystOutputStream;
import xot4j.depricated.io.log.LogInputStream;
import xot4j.depricated.io.log.LogOutputStream;
import xot4j.depricated.io.trace.TraceInputStream;
import xot4j.depricated.io.trace.TraceOutputStream;
import xot4j.depricated.pti.DceToDte;
import xot4j.depricated.utility.X25MessageUtility;
import xot4j.exceptions.CallSetupFailure;
import xot4j.exceptions.CallTearDownFailure;
import xot4j.x121.X121Address;
import xot4j.x121.X121AddressMarshaller;
import xot4j.x121.X121AddressPair;
import xot4j.x25.utility.HexUtility;

public class X25StateStream {

    private static final int DEFAULT_BUFFER_SIZE = 1024;
    private static final int OVERFLOW_LIMIT = 16;
    private static final boolean TRACE = false;
    private static final Logger logger = LoggerFactory.getLogger(X25StateStream.class);
    private final InputStream input;
    private final OutputStream output;
//	private static int lciSource = 1;
    private byte lcgn = 0;
    private byte lcn = 1; // 8;
    private int ps;
    private int pr;
    private boolean connected;
    private boolean moreData;
    private X121Address local;
    private X121Address remote;

    static InputStream decorate(final InputStream source) {

        InputStream input = source;

        if (TRACE) {
            input = new TraceInputStream(input, "xot");
        }
        input = new LogInputStream(input);
        input = new XotInputStream(input);
        input = new AnalystInputStream(input);

        return input;
    }

    static OutputStream decorate(final OutputStream source) {

        OutputStream output = source;

        if (TRACE) {
            output = new TraceOutputStream(output, "xot");
        }
        output = new LogOutputStream(output);
        output = new XotOutputStream(output);
        output = new AnalystOutputStream(output);

        return output;
    }

    public X25StateStream(InputStream input, OutputStream output) {

        this.input = decorate(input);
        this.output = decorate(output);

//		lcn = (byte)(lciSource & 0x00ff);
//		lcgn = (byte)((lciSource & 0x0f00) >> 8);
//
//		lciSource = ((lciSource + 1) & 0x0fff);
    }

    private void updateLci(byte[] buffer, int offset, int length) {

        lcgn = (byte) (buffer[offset] & 0x0f);
        lcn = buffer[offset + 1];
        logger.trace("Updated LCI (" + HexUtility.toHex(lcgn) + "," + HexUtility.toHex(lcn) + ") on call circuit (" + local + "," + remote + ")");
    }

    public boolean hasMoreData() {

        return moreData;
    }

    private void clearConnected() {

        logger.trace("clearConnected");

        connected = false;
        local = null;
        remote = null;
    }

    private void setConnected(X121Address local, X121Address remote) {

        logger.trace("setConnected(" + local + "," + remote + ")");

        connected = true;
        this.local = local;
        this.remote = remote;
    }

    public X121Address getLocalAddress() {

        return local;
    }

    public X121Address getRemoteAddress() {

        return remote;
    }

    public boolean isConnected() {

        return connected;
    }

    public void accept() throws CallSetupFailure, IOException {

        final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int length = input.read(buffer);
        final String description = DceToDte.getDescription(buffer[2]);

        if (DceToDte.INCOMING_CALL.equals(description)) {
            final X121Address called;
            final X121Address calling;
            {
                final X121AddressPair addressPair = X121AddressMarshaller.unmarshal(input, /*
                         * assume no a-bit
                         */ false);
                called = addressPair.calledAddress;
                calling = addressPair.callingAddress;
            }
            logger.debug("Accepting call from: " + calling);

            updateLci(buffer, 0, length);
            length = X25MessageUtility.getCallAccept(lcgn, lcn, buffer);
            output.write(buffer, 0, length);
            setConnected(called, calling);

        } else {
            throw new CallSetupFailure("I'm lost! I'm trying to accept. What should I do? [description=" + description + "]");
        }
    }

    public void call(X121Address local, X121Address remote) throws CallSetupFailure, IOException {

        final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int length = X25MessageUtility.getCallRequest(lcgn, lcn, remote, local, buffer);
        output.write(buffer, 0, length);

        length = input.read(buffer);
        final String description = DceToDte.getDescription(buffer[2]);

        switch (description) {
            case DceToDte.CALL_CONNECTED:
                setConnected(local, remote);
                updateLci(buffer, 0, length);
                break;

            case DceToDte.CLEAR_INDICATION:
                length = X25MessageUtility.getClearConfirm(lcgn, lcn, buffer);
                output.write(buffer, 0, length);
                throw new CallSetupFailure("Call failed [description=" + description + "]");

            default:
                throw new CallSetupFailure("I'm lost! I'm trying to connect. What should I do? [description=" + description + "]");
        }
    }

    public void disconnect() throws CallTearDownFailure, IOException {

        final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        {
            int length = X25MessageUtility.getClearRequest(lcgn, lcn, buffer);
            output.write(buffer, 0, length);
        }

        input.read(buffer);
        final String description = DceToDte.getDescription(buffer[2]);
        if (DceToDte.CLEAR_INDICATION.equals(description)) {
            final byte[] temporary = new byte[DEFAULT_BUFFER_SIZE];
            int len = X25MessageUtility.getClearConfirm(lcgn, lcn, temporary);
            output.write(temporary, 0, len);

            clearConnected();

        } else if (!DceToDte.DCE_CLEAR_CONFIRMATION.equals(description)) {
            throw new CallTearDownFailure("I'm lost! I'm trying to disconnect. What should I do? [description=" + description + "]");

        }
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {

        logger.trace("[" + remote + " -> " + local + "]: read([" + buffer.length + "]," + offset + "," + length + ")");

        byte[] temporary = new byte[DEFAULT_BUFFER_SIZE];
        int read = input.read(temporary, 0, (length - offset) + 3) - 3;

        String description = DceToDte.getDescription(temporary[2]);
        if (DceToDte.DATA_PACKET.equals(description)) {
            System.arraycopy(temporary, 3, buffer, offset, read);
            moreData = (temporary[2] & 0x0010) == 0x0010;

            pr = temporary[2] & 0x0e; // get ps
            pr = pr >> 1;
            pr = pr & 0x000f;
            pr = (pr + 1) % 8;
            int p = (pr << 5);
//			p = p | (temporary[2] & 0x0010);
            p = p & 0x00ef; // unset more
            p = p | 0x0001;
            p = (0x00f1 & p);

            X25MessageUtility.prepareHeader(lcgn, lcn, temporary);
            temporary[2] = (byte) p;
            output.write(temporary, 0, 3);

            return read;
        }
        if (DceToDte.CLEAR_INDICATION.equals(description)) {
            int len = X25MessageUtility.getClearConfirm(lcgn, lcn, temporary);
            output.write(temporary, 0, len);
            clearConnected();
        }

        return 0;
    }

    public void write(byte[] buffer, int offset, int length) throws IOException {

        logger.trace("[" + local + " -> " + remote + "]: write([" + buffer.length + "]," + offset + "," + length + ")");

        write(buffer, offset, length, 0);
    }

    private void write(byte[] buffer, int offset, int length, int depth)
            throws IOException {

        final int size = 128;
        for (int off = offset; off < length; off += size) {
            boolean more = (length - off) > size;
            int len = Math.min((length + offset) - off, size);
            write(more, buffer, off, len, depth); // overflow
        }
    }

    private void write(boolean more, byte[] buffer, int offset, int length, int depth)
            throws IOException {

        if (depth > OVERFLOW_LIMIT) {
            throw new IOException("Unable to write data after " + depth + " attempts.");
        }

        final String connection = "[" + local + " -> " + remote + "]";
        logger.trace(connection + ": write(" + more + ",[" + buffer.length + "]," + offset + "," + length + "," + depth + ")");

        byte[] temporary = new byte[DEFAULT_BUFFER_SIZE];
        X25MessageUtility.prepareHeader(lcgn, lcn, temporary);
        int p = (ps << 1) | (pr << 5);
        if (more) {
            p = p | 0x10;
        }
        temporary[2] = (byte) (p & 0x00fe);
        System.arraycopy(buffer, offset, temporary, 3, length);

        output.write(temporary, 0, 3 + length);
        ps = (ps + 1) % 8;

        input.read(temporary);

        String description = DceToDte.getDescription(temporary[2]);
        if (DceToDte.RESET_INDICATION.equals(description)) {
            ps = 0;

            X25MessageUtility.prepareHeader(lcgn, lcn, temporary);
            temporary[2] = 0x1f; // reset confirmation
            output.write(temporary, 0, 3);

            write(buffer, offset, length, depth + 1); // overflow
        }
        if (DceToDte.CLEAR_INDICATION.equals(description)) {
            int len = X25MessageUtility.getClearConfirm(lcgn, lcn, temporary);
            output.write(temporary, 0, len);
            clearConnected();
            throw new IOException("Connection " + connection + " closed by peer.");
        }
    }
}
