//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j;

import xot4j.x25.X25Packet;

public final class XotPacket {

    public static final int VERSION = 0;
    private final int version;
    private final X25Packet packet;

    XotPacket(int version, X25Packet packet) {

        this.version = version;
        this.packet = packet;
    }

    public static XotPacket create(int version, X25Packet packet) {

        return new XotPacket(version, packet);
    }

    public static XotPacket create(X25Packet packet) {

        return create(VERSION, packet);
    }

    public int getVersion() {

        return version;
    }

    public X25Packet getPacket() {

        return packet;
    }
}
