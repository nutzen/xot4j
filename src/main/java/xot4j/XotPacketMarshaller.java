//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import xot4j.x25.X25Packet;
import xot4j.x25.X25PacketMarshaller;
import xot4j.x25.utility.HexUtility;
import xot4j.x25.utility.StreamUtility;

public final class XotPacketMarshaller {

    private static final Logger logger = LoggerFactory.getLogger(XotPacketMarshaller.class);

    private XotPacketMarshaller() {
    }

    public static void marshal(final OutputStream outputStream, final XotPacket source) throws IOException {

        final int version = source.getVersion();
        final X25Packet packet = source.getPacket();

        final byte[] buffer;
        {
            final ByteArrayOutputStream target = new ByteArrayOutputStream();
            X25PacketMarshaller.marshal(target, packet);
            buffer = target.toByteArray();
        }

        StreamUtility.write(outputStream, (short) version);
        StreamUtility.write(outputStream, (short) buffer.length);
        outputStream.write(buffer);
    }

    public static XotPacket unmarshal(final InputStream inputStream) throws IOException {

        final int version = StreamUtility.readShort(inputStream);
        final int length = StreamUtility.readShort(inputStream);
        final byte[] buffer = StreamUtility.readBuffer(inputStream, length);

        try {
            final X25Packet packet = X25PacketMarshaller.unmarshal(new ByteArrayInputStream(buffer));
            return XotPacket.create(version, packet);

        } catch (IOException e) {
            logger.debug("Unable to unmarshal: " + HexUtility.toHex(buffer));
            throw e;

        } catch (RuntimeException e) {
            logger.debug("Unable to unmarshal: " + HexUtility.toHex(buffer));
            throw e;
        }
    }
}
