//
//          Copyright Digitata Limited 2008 - 2013.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
package xot4j;

import java.io.InputStream;
import java.io.OutputStream;
import xot4j.stream.X25ControllerInputStream;
import xot4j.stream.X25ControllerOutputStream;
import xot4j.x121.X121Address;
import xot4j.x25.X25Controller;
import xot4j.x25.context.SessionContext;

final class BasicXotConnection implements XotConnection {

    private final SessionContext sessionContext;
    private final X25Controller controller;
    private final InputStream inputStream;
    private final OutputStream outputStream;

    BasicXotConnection(SessionContext sessionContext,
            X25Controller controller,
            InputStream inputStream,
            OutputStream outputStream) {

        this.sessionContext = sessionContext;
        this.controller = controller;
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    static XotConnection create(SessionContext sessionContext,
            X25Controller controller) {

        final InputStream inputStream = X25ControllerInputStream.create(controller);
        final OutputStream outputStream = X25ControllerOutputStream.create(controller);

        return new BasicXotConnection(sessionContext, controller, inputStream, outputStream);
    }

    @Override
    public X121Address getRemoteAddress() {

        return sessionContext.getRemoteAddress();
    }

    @Override
    public void disconnect() {

        controller.disconnect();
    }

    @Override
    public InputStream getInputStream() {

        return inputStream;
    }

    @Override
    public OutputStream getOutputStream() {

        return outputStream;
    }
}
